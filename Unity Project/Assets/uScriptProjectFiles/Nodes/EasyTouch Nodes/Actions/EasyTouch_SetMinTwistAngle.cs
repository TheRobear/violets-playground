﻿using UnityEngine;
using System.Collections;

[NodePath("EasyTouch/Actions")]

[NodeCopyright("Copyright 2015 Reclusive Ninja")]
[NodeToolTip("Sets the Min Twise Angle.")]
[NodeAuthor("Reclusive Ninja", "http://reclusiveninja.com")]

[FriendlyName("EasyTouch Set Min Twist Angle", "Sets the Min Twist Angle.")]
public class EasyTouch_SetMinTwistAngle : uScriptLogic
{
#region uScript Functions ------------------------------------------------------------------------
	public void In(
		[FriendlyName("Min Twist Angle", "Sets the Min Twist Angle.")]
		float f_MinTwistAngle)
	{
        HedgehogTeam.EasyTouch.EasyTouch.SetMinTwistAngle(f_MinTwistAngle);
	}

//------------------------------------------------------------------------
	public bool Out
	{
		get { return true; }
	}
#endregion
}