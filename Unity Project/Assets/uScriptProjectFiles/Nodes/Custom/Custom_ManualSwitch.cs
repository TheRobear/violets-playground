﻿using UnityEngine;
using System.Collections;

[NodePath("Custom/Actions")]

[FriendlyName("Custom - Manual Switch")]
public class Custom_ManualSwitch : uScriptLogic
{
    private int i_CurrentOutput = 1;
    private bool b_SwitchOpen = true;

#region uScript Functions ------------------------------------------------------------------------
    public delegate void uScriptEventHandler(object sender, System.EventArgs args);

    public void In(
        [FriendlyName("Output To Use", "The output switch to use.")]
        int CurrentOutput)
    {
        i_CurrentOutput = CurrentOutput;

        if (b_SwitchOpen)
        {
            switch (i_CurrentOutput)
            {
                case 1:
                    Case1(this, new System.EventArgs());
                    break;

                case 2:
                    Case2(this, new System.EventArgs());
                    break;

                case 3:
                    Case3(this, new System.EventArgs());
                    break;

                case 4:
                    Case4(this, new System.EventArgs());
                    break;

                case 5:
                    Case5(this, new System.EventArgs());
                    break;

                case 6:
                    Case6(this, new System.EventArgs());
                    break;

                case 7:
                    Case7(this, new System.EventArgs());
                    break;

                case 8:
                    Case8(this, new System.EventArgs());
                    break;

                case 9:
                    Case9(this, new System.EventArgs());
                    break;
                    
                case 10:
                    Case10(this, new System.EventArgs());
                    break;
                    
                case 11:
                    Case11(this, new System.EventArgs());
                    break;

                default:
                    break;
            }
        }
    }
    [FriendlyName("Case 1")]
    public event uScriptEventHandler Case1;

    [FriendlyName("Case 2")]
    public event uScriptEventHandler Case2;

    [FriendlyName("Case 3")]
    public event uScriptEventHandler Case3;

    [FriendlyName("Case 4")]
    public event uScriptEventHandler Case4;

    [FriendlyName("Case 5")]
    public event uScriptEventHandler Case5;

    [FriendlyName("Case 6")]
    public event uScriptEventHandler Case6;

    [FriendlyName("Case 7")]
    public event uScriptEventHandler Case7;

    [FriendlyName("Case 8")]
    public event uScriptEventHandler Case8;

    [FriendlyName("Case 9")]
    public event uScriptEventHandler Case9;
    
    [FriendlyName("Case 10")]
    public event uScriptEventHandler Case10;
    
    [FriendlyName("Case 11")]
    public event uScriptEventHandler Case11;
#endregion
}