﻿using UnityEngine;
using System.Collections;

[NodePath("EasyTouch/Actions")]

[NodeCopyright("Copyright 2015 Reclusive Ninja")]
[NodeToolTip("If Enable auto-select is false, add a camera to the Automatic Selection section.")]
[NodeAuthor("Reclusive Ninja", "http://reclusiveninja.com")]

[FriendlyName("EasyTouch Add Camera", "If Enable auto-select is false, add a camera to the Automatic Selection section.")]
public class EasyTouch_AddCamera : uScriptLogic
{
#region uScript Functions ------------------------------------------------------------------------
    public void In(
        [FriendlyName("Camera", "The camera to add to the Automatic Selection section.")]
        Camera c_Camera,

        [FriendlyName("Is It A GUI Camera?", "If true, tell EasyTouch that the camera is a Unity GUI camera.")]
        bool b_IsGUICamera
        )
    {
        HedgehogTeam.EasyTouch.EasyTouch.AddCamera(c_Camera, b_IsGUICamera);
    }

//------------------------------------------------------------------------
    public bool Out
    {
        get { return true; }
    }
#endregion
}