﻿using UnityEngine;
using System.Collections;

[NodePath("EasyTouch/Actions")]

[NodeCopyright("Copyright 2015 Reclusive Ninja")]
[NodeToolTip("Enables/Disables Second Finger Simulation.")]
[NodeAuthor("Reclusive Ninja", "http://reclusiveninja.com")]

[FriendlyName("EasyTouch Set Second Finger Simulation", "Enables/Disables Second Finger Simulation.")]
public class EasyTouch_SetSecondFingerSimulation : uScriptLogic
{
#region uScript Functions ------------------------------------------------------------------------
	public void In(
		[FriendlyName("Enable Second Finger Simulation", "Enables/Disables Second Finger Simulation.")]
		bool b_EnableSecondFingerSimulation)
	{
        HedgehogTeam.EasyTouch.EasyTouch.SetSecondFingerSimulation(b_EnableSecondFingerSimulation);
	}

//------------------------------------------------------------------------
	public bool Out
	{
		get { return true; }
	}
#endregion
}