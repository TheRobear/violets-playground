﻿using UnityEngine;
using System.Collections;

[NodePath("EasyTouch/Actions")]

[NodeCopyright("Copyright 2015 Reclusive Ninja")]
[NodeToolTip("Sets the Stationary Tolerance for EasyTouch.")]
[NodeAuthor("Reclusive Ninja", "http://reclusiveninja.com")]

[FriendlyName("EasyTouch Set Stationary Tolerance", "Sets the Stationary Tolerance for EasyTouch.")]
public class EasyTouch_SetStationaryTolerance : uScriptLogic
{
#region uScript Functions ------------------------------------------------------------------------
    public void In(
        [FriendlyName("Stationary Tolerance", "Sets the Stationary Tolerance for EasyTouch.")]
        float f_StationaryTolerance
        )
    {
        HedgehogTeam.EasyTouch.EasyTouch.SetStationaryTolerance(f_StationaryTolerance);
    }

//------------------------------------------------------------------------
    public bool Out
    {
        get { return true; }
    }
#endregion
}