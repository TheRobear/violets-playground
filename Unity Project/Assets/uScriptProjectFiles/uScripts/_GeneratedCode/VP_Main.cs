//uScript Generated Code - Build 1.0.3024
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[NodePath("Graphs")]
[System.Serializable]
[FriendlyName("Untitled", "")]
public class VP_Main : uScriptLogic
{

   #pragma warning disable 414
   GameObject parentGameObject = null;
   uScript_GUI thisScriptsOnGuiListener = null; 
   bool m_RegisteredForEvents = false;
   
   //externally exposed events
   
   //external parameters
   
   //local nodes
   public System.Int32 CurrentIndex = (int) 0;
   public System.Boolean EnableTouch = (bool) false;
   public System.Int32 Index = (int) 0;
   UnityEngine.GameObject local_120_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_120_UnityEngine_GameObject_previous = null;
   UnityEngine.Color[] local_121_UnityEngine_ColorArray = new UnityEngine.Color[] {new UnityEngine.Color((float)0, (float)0, (float)1, (float)1),new UnityEngine.Color((float)1, (float)0, (float)0, (float)1),new UnityEngine.Color((float)0, (float)1, (float)0, (float)1),new UnityEngine.Color((float)1, (float)1, (float)0, (float)1),new UnityEngine.Color((float)0, (float)0, (float)0, (float)0)};
   UnityEngine.GameObject local_143_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_143_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_149_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_149_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_156_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_156_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_160_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_160_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_161_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_161_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject[] local_166_UnityEngine_GameObjectArray = new UnityEngine.GameObject[] {null,null,null,null,null,null,null,null};
   UnityEngine.GameObject[] local_342_UnityEngine_GameObjectArray = new UnityEngine.GameObject[] {null,null,null,null,null,null,null,null};
   UnityEngine.GameObject local_348_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_348_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject[] local_353_UnityEngine_GameObjectArray = new UnityEngine.GameObject[] {null,null,null,null,null,null,null,null};
   UnityEngine.GameObject[] local_60_UnityEngine_GameObjectArray = new UnityEngine.GameObject[] {null,null,null,null};
   UnityEngine.GameObject local_86_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_86_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_88_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_88_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_91_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_91_UnityEngine_GameObject_previous = null;
   UnityEngine.Color local_BottomColor_UnityEngine_Color = new UnityEngine.Color( (float)0, (float)0, (float)0, (float)1 );
   UnityEngine.GameObject local_CirclePop_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_CirclePop_UnityEngine_GameObject_previous = null;
   UnityEngine.Color local_CurrentColor_UnityEngine_Color = new UnityEngine.Color( (float)0, (float)0, (float)0, (float)1 );
   UnityEngine.GameObject local_HeartPop_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_HeartPop_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_Number_01_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_Number_01_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_Number_02_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_Number_02_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_Number_03_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_Number_03_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_Number_04_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_Number_04_UnityEngine_GameObject_previous = null;
   UnityEngine.Color local_RandomColor_UnityEngine_Color = new UnityEngine.Color( (float)0, (float)0, (float)0, (float)1 );
   UnityEngine.GameObject local_SquarePop_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_SquarePop_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_StarPop_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_StarPop_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_TrianglePop_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_TrianglePop_UnityEngine_GameObject_previous = null;
   UnityEngine.Vector3 local_WorldPoint_UnityEngine_Vector3 = new Vector3( (float)0, (float)0, (float)0 );
   
   //owner nodes
   
   //logic nodes
   //pointer to script instanced logic node
   uScriptAct_SetGameObjectPosition logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_2 = new uScriptAct_SetGameObjectPosition( );
   UnityEngine.GameObject[] logic_uScriptAct_SetGameObjectPosition_Target_2 = new UnityEngine.GameObject[] {};
   UnityEngine.Vector3 logic_uScriptAct_SetGameObjectPosition_Position_2 = new Vector3( );
   System.Boolean logic_uScriptAct_SetGameObjectPosition_AsOffset_2 = (bool) false;
   System.Boolean logic_uScriptAct_SetGameObjectPosition_AsLocal_2 = (bool) false;
   bool logic_uScriptAct_SetGameObjectPosition_Out_2 = true;
   //pointer to script instanced logic node
   uScriptAct_AnimatorSetParameterTrigger logic_uScriptAct_AnimatorSetParameterTrigger_uScriptAct_AnimatorSetParameterTrigger_5 = new uScriptAct_AnimatorSetParameterTrigger( );
   UnityEngine.GameObject[] logic_uScriptAct_AnimatorSetParameterTrigger_Target_5 = new UnityEngine.GameObject[] {};
   System.String logic_uScriptAct_AnimatorSetParameterTrigger_Name_5 = "Circle_Pop";
   bool logic_uScriptAct_AnimatorSetParameterTrigger_Out_5 = true;
   //pointer to script instanced logic node
   uScriptAct_PlayAudioSource logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_7 = new uScriptAct_PlayAudioSource( );
   UnityEngine.GameObject[] logic_uScriptAct_PlayAudioSource_target_7 = new UnityEngine.GameObject[] {};
   UnityEngine.AudioClip logic_uScriptAct_PlayAudioSource_audioClip_7 = default(UnityEngine.AudioClip);
   bool logic_uScriptAct_PlayAudioSource_Out_7 = true;
   //pointer to script instanced logic node
   uScriptAct_SetRandomColor logic_uScriptAct_SetRandomColor_uScriptAct_SetRandomColor_9 = new uScriptAct_SetRandomColor( );
   System.Single logic_uScriptAct_SetRandomColor_RedMin_9 = (float) 0;
   System.Single logic_uScriptAct_SetRandomColor_RedMax_9 = (float) 1;
   System.Single logic_uScriptAct_SetRandomColor_GreenMin_9 = (float) 0;
   System.Single logic_uScriptAct_SetRandomColor_GreenMax_9 = (float) 1;
   System.Single logic_uScriptAct_SetRandomColor_BlueMin_9 = (float) 0;
   System.Single logic_uScriptAct_SetRandomColor_BlueMax_9 = (float) 1;
   System.Single logic_uScriptAct_SetRandomColor_AlphaMin_9 = (float) 1;
   System.Single logic_uScriptAct_SetRandomColor_AlphaMax_9 = (float) 1;
   UnityEngine.Color logic_uScriptAct_SetRandomColor_TargetColor_9;
   bool logic_uScriptAct_SetRandomColor_Out_9 = true;
   //pointer to script instanced logic node
   uScriptAct_AssignMaterialColor logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_11 = new uScriptAct_AssignMaterialColor( );
   UnityEngine.GameObject[] logic_uScriptAct_AssignMaterialColor_Target_11 = new UnityEngine.GameObject[] {};
   UnityEngine.Color logic_uScriptAct_AssignMaterialColor_MatColor_11 = UnityEngine.Color.black;
   System.Int32 logic_uScriptAct_AssignMaterialColor_MatChannel_11 = (int) 0;
   bool logic_uScriptAct_AssignMaterialColor_Out_11 = true;
   //pointer to script instanced logic node
   uScriptAct_SetGameObjectPosition logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_13 = new uScriptAct_SetGameObjectPosition( );
   UnityEngine.GameObject[] logic_uScriptAct_SetGameObjectPosition_Target_13 = new UnityEngine.GameObject[] {};
   UnityEngine.Vector3 logic_uScriptAct_SetGameObjectPosition_Position_13 = new Vector3( );
   System.Boolean logic_uScriptAct_SetGameObjectPosition_AsOffset_13 = (bool) false;
   System.Boolean logic_uScriptAct_SetGameObjectPosition_AsLocal_13 = (bool) false;
   bool logic_uScriptAct_SetGameObjectPosition_Out_13 = true;
   //pointer to script instanced logic node
   uScriptAct_AnimatorSetParameterTrigger logic_uScriptAct_AnimatorSetParameterTrigger_uScriptAct_AnimatorSetParameterTrigger_16 = new uScriptAct_AnimatorSetParameterTrigger( );
   UnityEngine.GameObject[] logic_uScriptAct_AnimatorSetParameterTrigger_Target_16 = new UnityEngine.GameObject[] {};
   System.String logic_uScriptAct_AnimatorSetParameterTrigger_Name_16 = "Square_Pop";
   bool logic_uScriptAct_AnimatorSetParameterTrigger_Out_16 = true;
   //pointer to script instanced logic node
   uScriptAct_AssignMaterialColor logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_18 = new uScriptAct_AssignMaterialColor( );
   UnityEngine.GameObject[] logic_uScriptAct_AssignMaterialColor_Target_18 = new UnityEngine.GameObject[] {};
   UnityEngine.Color logic_uScriptAct_AssignMaterialColor_MatColor_18 = UnityEngine.Color.black;
   System.Int32 logic_uScriptAct_AssignMaterialColor_MatChannel_18 = (int) 0;
   bool logic_uScriptAct_AssignMaterialColor_Out_18 = true;
   //pointer to script instanced logic node
   uScriptAct_SetGameObjectPosition logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_21 = new uScriptAct_SetGameObjectPosition( );
   UnityEngine.GameObject[] logic_uScriptAct_SetGameObjectPosition_Target_21 = new UnityEngine.GameObject[] {};
   UnityEngine.Vector3 logic_uScriptAct_SetGameObjectPosition_Position_21 = new Vector3( );
   System.Boolean logic_uScriptAct_SetGameObjectPosition_AsOffset_21 = (bool) false;
   System.Boolean logic_uScriptAct_SetGameObjectPosition_AsLocal_21 = (bool) false;
   bool logic_uScriptAct_SetGameObjectPosition_Out_21 = true;
   //pointer to script instanced logic node
   uScriptAct_AnimatorSetParameterTrigger logic_uScriptAct_AnimatorSetParameterTrigger_uScriptAct_AnimatorSetParameterTrigger_24 = new uScriptAct_AnimatorSetParameterTrigger( );
   UnityEngine.GameObject[] logic_uScriptAct_AnimatorSetParameterTrigger_Target_24 = new UnityEngine.GameObject[] {};
   System.String logic_uScriptAct_AnimatorSetParameterTrigger_Name_24 = "Star_Pop";
   bool logic_uScriptAct_AnimatorSetParameterTrigger_Out_24 = true;
   //pointer to script instanced logic node
   uScriptAct_AssignMaterialColor logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_26 = new uScriptAct_AssignMaterialColor( );
   UnityEngine.GameObject[] logic_uScriptAct_AssignMaterialColor_Target_26 = new UnityEngine.GameObject[] {};
   UnityEngine.Color logic_uScriptAct_AssignMaterialColor_MatColor_26 = UnityEngine.Color.black;
   System.Int32 logic_uScriptAct_AssignMaterialColor_MatChannel_26 = (int) 0;
   bool logic_uScriptAct_AssignMaterialColor_Out_26 = true;
   //pointer to script instanced logic node
   uScriptAct_SetGameObjectPosition logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_29 = new uScriptAct_SetGameObjectPosition( );
   UnityEngine.GameObject[] logic_uScriptAct_SetGameObjectPosition_Target_29 = new UnityEngine.GameObject[] {};
   UnityEngine.Vector3 logic_uScriptAct_SetGameObjectPosition_Position_29 = new Vector3( );
   System.Boolean logic_uScriptAct_SetGameObjectPosition_AsOffset_29 = (bool) false;
   System.Boolean logic_uScriptAct_SetGameObjectPosition_AsLocal_29 = (bool) false;
   bool logic_uScriptAct_SetGameObjectPosition_Out_29 = true;
   //pointer to script instanced logic node
   uScriptAct_AnimatorSetParameterTrigger logic_uScriptAct_AnimatorSetParameterTrigger_uScriptAct_AnimatorSetParameterTrigger_32 = new uScriptAct_AnimatorSetParameterTrigger( );
   UnityEngine.GameObject[] logic_uScriptAct_AnimatorSetParameterTrigger_Target_32 = new UnityEngine.GameObject[] {};
   System.String logic_uScriptAct_AnimatorSetParameterTrigger_Name_32 = "Heart_Pop";
   bool logic_uScriptAct_AnimatorSetParameterTrigger_Out_32 = true;
   //pointer to script instanced logic node
   uScriptAct_AssignMaterialColor logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_34 = new uScriptAct_AssignMaterialColor( );
   UnityEngine.GameObject[] logic_uScriptAct_AssignMaterialColor_Target_34 = new UnityEngine.GameObject[] {};
   UnityEngine.Color logic_uScriptAct_AssignMaterialColor_MatColor_34 = UnityEngine.Color.black;
   System.Int32 logic_uScriptAct_AssignMaterialColor_MatChannel_34 = (int) 0;
   bool logic_uScriptAct_AssignMaterialColor_Out_34 = true;
   //pointer to script instanced logic node
   uScriptAct_SetGameObjectPosition logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_38 = new uScriptAct_SetGameObjectPosition( );
   UnityEngine.GameObject[] logic_uScriptAct_SetGameObjectPosition_Target_38 = new UnityEngine.GameObject[] {};
   UnityEngine.Vector3 logic_uScriptAct_SetGameObjectPosition_Position_38 = new Vector3( );
   System.Boolean logic_uScriptAct_SetGameObjectPosition_AsOffset_38 = (bool) false;
   System.Boolean logic_uScriptAct_SetGameObjectPosition_AsLocal_38 = (bool) false;
   bool logic_uScriptAct_SetGameObjectPosition_Out_38 = true;
   //pointer to script instanced logic node
   uScriptAct_AnimatorSetParameterTrigger logic_uScriptAct_AnimatorSetParameterTrigger_uScriptAct_AnimatorSetParameterTrigger_39 = new uScriptAct_AnimatorSetParameterTrigger( );
   UnityEngine.GameObject[] logic_uScriptAct_AnimatorSetParameterTrigger_Target_39 = new UnityEngine.GameObject[] {};
   System.String logic_uScriptAct_AnimatorSetParameterTrigger_Name_39 = "Triangle_Pop";
   bool logic_uScriptAct_AnimatorSetParameterTrigger_Out_39 = true;
   //pointer to script instanced logic node
   uScriptAct_AssignMaterialColor logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_41 = new uScriptAct_AssignMaterialColor( );
   UnityEngine.GameObject[] logic_uScriptAct_AssignMaterialColor_Target_41 = new UnityEngine.GameObject[] {};
   UnityEngine.Color logic_uScriptAct_AssignMaterialColor_MatColor_41 = UnityEngine.Color.black;
   System.Int32 logic_uScriptAct_AssignMaterialColor_MatChannel_41 = (int) 0;
   bool logic_uScriptAct_AssignMaterialColor_Out_41 = true;
   //pointer to script instanced logic node
   uScriptAct_PlayAudioSource logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_46 = new uScriptAct_PlayAudioSource( );
   UnityEngine.GameObject[] logic_uScriptAct_PlayAudioSource_target_46 = new UnityEngine.GameObject[] {};
   UnityEngine.AudioClip logic_uScriptAct_PlayAudioSource_audioClip_46 = default(UnityEngine.AudioClip);
   bool logic_uScriptAct_PlayAudioSource_Out_46 = true;
   //pointer to script instanced logic node
   uScriptAct_PlayAudioSource logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_48 = new uScriptAct_PlayAudioSource( );
   UnityEngine.GameObject[] logic_uScriptAct_PlayAudioSource_target_48 = new UnityEngine.GameObject[] {};
   UnityEngine.AudioClip logic_uScriptAct_PlayAudioSource_audioClip_48 = default(UnityEngine.AudioClip);
   bool logic_uScriptAct_PlayAudioSource_Out_48 = true;
   //pointer to script instanced logic node
   uScriptAct_PlayAudioSource logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_50 = new uScriptAct_PlayAudioSource( );
   UnityEngine.GameObject[] logic_uScriptAct_PlayAudioSource_target_50 = new UnityEngine.GameObject[] {};
   UnityEngine.AudioClip logic_uScriptAct_PlayAudioSource_audioClip_50 = default(UnityEngine.AudioClip);
   bool logic_uScriptAct_PlayAudioSource_Out_50 = true;
   //pointer to script instanced logic node
   uScriptAct_PlayAudioSource logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_52 = new uScriptAct_PlayAudioSource( );
   UnityEngine.GameObject[] logic_uScriptAct_PlayAudioSource_target_52 = new UnityEngine.GameObject[] {};
   UnityEngine.AudioClip logic_uScriptAct_PlayAudioSource_audioClip_52 = default(UnityEngine.AudioClip);
   bool logic_uScriptAct_PlayAudioSource_Out_52 = true;
   //pointer to script instanced logic node
   Custom_SetObjectRendererState logic_Custom_SetObjectRendererState_Custom_SetObjectRendererState_55 = new Custom_SetObjectRendererState( );
   UnityEngine.GameObject[] logic_Custom_SetObjectRendererState_go_Target_55 = new UnityEngine.GameObject[] {};
   Custom_SetObjectRendererState.e_RenderState logic_Custom_SetObjectRendererState_ers_RenderState_55 = Custom_SetObjectRendererState.e_RenderState.No;
   bool logic_Custom_SetObjectRendererState_Out_55 = true;
   //pointer to script instanced logic node
   uScriptAct_SetGameObjectPosition logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_57 = new uScriptAct_SetGameObjectPosition( );
   UnityEngine.GameObject[] logic_uScriptAct_SetGameObjectPosition_Target_57 = new UnityEngine.GameObject[] {};
   UnityEngine.Vector3 logic_uScriptAct_SetGameObjectPosition_Position_57 = new Vector3( );
   System.Boolean logic_uScriptAct_SetGameObjectPosition_AsOffset_57 = (bool) false;
   System.Boolean logic_uScriptAct_SetGameObjectPosition_AsLocal_57 = (bool) false;
   bool logic_uScriptAct_SetGameObjectPosition_Out_57 = true;
   //pointer to script instanced logic node
   Custom_SetObjectRendererState logic_Custom_SetObjectRendererState_Custom_SetObjectRendererState_59 = new Custom_SetObjectRendererState( );
   UnityEngine.GameObject[] logic_Custom_SetObjectRendererState_go_Target_59 = new UnityEngine.GameObject[] {};
   Custom_SetObjectRendererState.e_RenderState logic_Custom_SetObjectRendererState_ers_RenderState_59 = Custom_SetObjectRendererState.e_RenderState.Yes;
   bool logic_Custom_SetObjectRendererState_Out_59 = true;
   //pointer to script instanced logic node
   uScriptAct_AssignMaterialColor logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_61 = new uScriptAct_AssignMaterialColor( );
   UnityEngine.GameObject[] logic_uScriptAct_AssignMaterialColor_Target_61 = new UnityEngine.GameObject[] {};
   UnityEngine.Color logic_uScriptAct_AssignMaterialColor_MatColor_61 = UnityEngine.Color.black;
   System.Int32 logic_uScriptAct_AssignMaterialColor_MatChannel_61 = (int) 0;
   bool logic_uScriptAct_AssignMaterialColor_Out_61 = true;
   //pointer to script instanced logic node
   uScriptAct_Delay logic_uScriptAct_Delay_uScriptAct_Delay_63 = new uScriptAct_Delay( );
   System.Single logic_uScriptAct_Delay_Duration_63 = (float) 0.5;
   System.Boolean logic_uScriptAct_Delay_SingleFrame_63 = (bool) false;
   bool logic_uScriptAct_Delay_Immediate_63 = true;
   bool logic_uScriptAct_Delay_AfterDelay_63 = true;
   bool logic_uScriptAct_Delay_Stopped_63 = true;
   bool logic_uScriptAct_Delay_DrivenDelay_63 = false;
   //pointer to script instanced logic node
   Custom_SetObjectRendererState logic_Custom_SetObjectRendererState_Custom_SetObjectRendererState_64 = new Custom_SetObjectRendererState( );
   UnityEngine.GameObject[] logic_Custom_SetObjectRendererState_go_Target_64 = new UnityEngine.GameObject[] {};
   Custom_SetObjectRendererState.e_RenderState logic_Custom_SetObjectRendererState_ers_RenderState_64 = Custom_SetObjectRendererState.e_RenderState.No;
   bool logic_Custom_SetObjectRendererState_Out_64 = true;
   //pointer to script instanced logic node
   Custom_SetObjectRendererState logic_Custom_SetObjectRendererState_Custom_SetObjectRendererState_65 = new Custom_SetObjectRendererState( );
   UnityEngine.GameObject[] logic_Custom_SetObjectRendererState_go_Target_65 = new UnityEngine.GameObject[] {};
   Custom_SetObjectRendererState.e_RenderState logic_Custom_SetObjectRendererState_ers_RenderState_65 = Custom_SetObjectRendererState.e_RenderState.No;
   bool logic_Custom_SetObjectRendererState_Out_65 = true;
   //pointer to script instanced logic node
   uScriptAct_AssignMaterialColor logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_67 = new uScriptAct_AssignMaterialColor( );
   UnityEngine.GameObject[] logic_uScriptAct_AssignMaterialColor_Target_67 = new UnityEngine.GameObject[] {};
   UnityEngine.Color logic_uScriptAct_AssignMaterialColor_MatColor_67 = UnityEngine.Color.black;
   System.Int32 logic_uScriptAct_AssignMaterialColor_MatChannel_67 = (int) 0;
   bool logic_uScriptAct_AssignMaterialColor_Out_67 = true;
   //pointer to script instanced logic node
   uScriptAct_SetGameObjectPosition logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_68 = new uScriptAct_SetGameObjectPosition( );
   UnityEngine.GameObject[] logic_uScriptAct_SetGameObjectPosition_Target_68 = new UnityEngine.GameObject[] {};
   UnityEngine.Vector3 logic_uScriptAct_SetGameObjectPosition_Position_68 = new Vector3( );
   System.Boolean logic_uScriptAct_SetGameObjectPosition_AsOffset_68 = (bool) false;
   System.Boolean logic_uScriptAct_SetGameObjectPosition_AsLocal_68 = (bool) false;
   bool logic_uScriptAct_SetGameObjectPosition_Out_68 = true;
   //pointer to script instanced logic node
   Custom_SetObjectRendererState logic_Custom_SetObjectRendererState_Custom_SetObjectRendererState_70 = new Custom_SetObjectRendererState( );
   UnityEngine.GameObject[] logic_Custom_SetObjectRendererState_go_Target_70 = new UnityEngine.GameObject[] {};
   Custom_SetObjectRendererState.e_RenderState logic_Custom_SetObjectRendererState_ers_RenderState_70 = Custom_SetObjectRendererState.e_RenderState.Yes;
   bool logic_Custom_SetObjectRendererState_Out_70 = true;
   //pointer to script instanced logic node
   uScriptAct_Delay logic_uScriptAct_Delay_uScriptAct_Delay_71 = new uScriptAct_Delay( );
   System.Single logic_uScriptAct_Delay_Duration_71 = (float) 0.5;
   System.Boolean logic_uScriptAct_Delay_SingleFrame_71 = (bool) false;
   bool logic_uScriptAct_Delay_Immediate_71 = true;
   bool logic_uScriptAct_Delay_AfterDelay_71 = true;
   bool logic_uScriptAct_Delay_Stopped_71 = true;
   bool logic_uScriptAct_Delay_DrivenDelay_71 = false;
   //pointer to script instanced logic node
   uScriptAct_PlayAudioSource logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_73 = new uScriptAct_PlayAudioSource( );
   UnityEngine.GameObject[] logic_uScriptAct_PlayAudioSource_target_73 = new UnityEngine.GameObject[] {};
   UnityEngine.AudioClip logic_uScriptAct_PlayAudioSource_audioClip_73 = default(UnityEngine.AudioClip);
   bool logic_uScriptAct_PlayAudioSource_Out_73 = true;
   //pointer to script instanced logic node
   uScriptAct_PlayAudioSource logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_74 = new uScriptAct_PlayAudioSource( );
   UnityEngine.GameObject[] logic_uScriptAct_PlayAudioSource_target_74 = new UnityEngine.GameObject[] {};
   UnityEngine.AudioClip logic_uScriptAct_PlayAudioSource_audioClip_74 = default(UnityEngine.AudioClip);
   bool logic_uScriptAct_PlayAudioSource_Out_74 = true;
   //pointer to script instanced logic node
   uScriptAct_SetGameObjectPosition logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_85 = new uScriptAct_SetGameObjectPosition( );
   UnityEngine.GameObject[] logic_uScriptAct_SetGameObjectPosition_Target_85 = new UnityEngine.GameObject[] {};
   UnityEngine.Vector3 logic_uScriptAct_SetGameObjectPosition_Position_85 = new Vector3( );
   System.Boolean logic_uScriptAct_SetGameObjectPosition_AsOffset_85 = (bool) false;
   System.Boolean logic_uScriptAct_SetGameObjectPosition_AsLocal_85 = (bool) false;
   bool logic_uScriptAct_SetGameObjectPosition_Out_85 = true;
   //pointer to script instanced logic node
   uScriptAct_SetGameObjectPosition logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_93 = new uScriptAct_SetGameObjectPosition( );
   UnityEngine.GameObject[] logic_uScriptAct_SetGameObjectPosition_Target_93 = new UnityEngine.GameObject[] {};
   UnityEngine.Vector3 logic_uScriptAct_SetGameObjectPosition_Position_93 = new Vector3( );
   System.Boolean logic_uScriptAct_SetGameObjectPosition_AsOffset_93 = (bool) false;
   System.Boolean logic_uScriptAct_SetGameObjectPosition_AsLocal_93 = (bool) false;
   bool logic_uScriptAct_SetGameObjectPosition_Out_93 = true;
   //pointer to script instanced logic node
   uScriptAct_AssignMaterialColor logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_95 = new uScriptAct_AssignMaterialColor( );
   UnityEngine.GameObject[] logic_uScriptAct_AssignMaterialColor_Target_95 = new UnityEngine.GameObject[] {};
   UnityEngine.Color logic_uScriptAct_AssignMaterialColor_MatColor_95 = UnityEngine.Color.black;
   System.Int32 logic_uScriptAct_AssignMaterialColor_MatChannel_95 = (int) 0;
   bool logic_uScriptAct_AssignMaterialColor_Out_95 = true;
   //pointer to script instanced logic node
   Custom_SetObjectRendererState logic_Custom_SetObjectRendererState_Custom_SetObjectRendererState_96 = new Custom_SetObjectRendererState( );
   UnityEngine.GameObject[] logic_Custom_SetObjectRendererState_go_Target_96 = new UnityEngine.GameObject[] {};
   Custom_SetObjectRendererState.e_RenderState logic_Custom_SetObjectRendererState_ers_RenderState_96 = Custom_SetObjectRendererState.e_RenderState.Yes;
   bool logic_Custom_SetObjectRendererState_Out_96 = true;
   //pointer to script instanced logic node
   uScriptAct_PlayAudioSource logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_98 = new uScriptAct_PlayAudioSource( );
   UnityEngine.GameObject[] logic_uScriptAct_PlayAudioSource_target_98 = new UnityEngine.GameObject[] {};
   UnityEngine.AudioClip logic_uScriptAct_PlayAudioSource_audioClip_98 = default(UnityEngine.AudioClip);
   bool logic_uScriptAct_PlayAudioSource_Out_98 = true;
   //pointer to script instanced logic node
   uScriptAct_Delay logic_uScriptAct_Delay_uScriptAct_Delay_99 = new uScriptAct_Delay( );
   System.Single logic_uScriptAct_Delay_Duration_99 = (float) 0.5;
   System.Boolean logic_uScriptAct_Delay_SingleFrame_99 = (bool) false;
   bool logic_uScriptAct_Delay_Immediate_99 = true;
   bool logic_uScriptAct_Delay_AfterDelay_99 = true;
   bool logic_uScriptAct_Delay_Stopped_99 = true;
   bool logic_uScriptAct_Delay_DrivenDelay_99 = false;
   //pointer to script instanced logic node
   Custom_SetObjectRendererState logic_Custom_SetObjectRendererState_Custom_SetObjectRendererState_100 = new Custom_SetObjectRendererState( );
   UnityEngine.GameObject[] logic_Custom_SetObjectRendererState_go_Target_100 = new UnityEngine.GameObject[] {};
   Custom_SetObjectRendererState.e_RenderState logic_Custom_SetObjectRendererState_ers_RenderState_100 = Custom_SetObjectRendererState.e_RenderState.No;
   bool logic_Custom_SetObjectRendererState_Out_100 = true;
   //pointer to script instanced logic node
   Custom_SetObjectRendererState logic_Custom_SetObjectRendererState_Custom_SetObjectRendererState_104 = new Custom_SetObjectRendererState( );
   UnityEngine.GameObject[] logic_Custom_SetObjectRendererState_go_Target_104 = new UnityEngine.GameObject[] {};
   Custom_SetObjectRendererState.e_RenderState logic_Custom_SetObjectRendererState_ers_RenderState_104 = Custom_SetObjectRendererState.e_RenderState.No;
   bool logic_Custom_SetObjectRendererState_Out_104 = true;
   //pointer to script instanced logic node
   uScriptAct_SetGameObjectPosition logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_105 = new uScriptAct_SetGameObjectPosition( );
   UnityEngine.GameObject[] logic_uScriptAct_SetGameObjectPosition_Target_105 = new UnityEngine.GameObject[] {};
   UnityEngine.Vector3 logic_uScriptAct_SetGameObjectPosition_Position_105 = new Vector3( );
   System.Boolean logic_uScriptAct_SetGameObjectPosition_AsOffset_105 = (bool) false;
   System.Boolean logic_uScriptAct_SetGameObjectPosition_AsLocal_105 = (bool) false;
   bool logic_uScriptAct_SetGameObjectPosition_Out_105 = true;
   //pointer to script instanced logic node
   uScriptAct_AssignMaterialColor logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_107 = new uScriptAct_AssignMaterialColor( );
   UnityEngine.GameObject[] logic_uScriptAct_AssignMaterialColor_Target_107 = new UnityEngine.GameObject[] {};
   UnityEngine.Color logic_uScriptAct_AssignMaterialColor_MatColor_107 = UnityEngine.Color.black;
   System.Int32 logic_uScriptAct_AssignMaterialColor_MatChannel_107 = (int) 0;
   bool logic_uScriptAct_AssignMaterialColor_Out_107 = true;
   //pointer to script instanced logic node
   Custom_SetObjectRendererState logic_Custom_SetObjectRendererState_Custom_SetObjectRendererState_109 = new Custom_SetObjectRendererState( );
   UnityEngine.GameObject[] logic_Custom_SetObjectRendererState_go_Target_109 = new UnityEngine.GameObject[] {};
   Custom_SetObjectRendererState.e_RenderState logic_Custom_SetObjectRendererState_ers_RenderState_109 = Custom_SetObjectRendererState.e_RenderState.Yes;
   bool logic_Custom_SetObjectRendererState_Out_109 = true;
   //pointer to script instanced logic node
   uScriptAct_PlayAudioSource logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_110 = new uScriptAct_PlayAudioSource( );
   UnityEngine.GameObject[] logic_uScriptAct_PlayAudioSource_target_110 = new UnityEngine.GameObject[] {};
   UnityEngine.AudioClip logic_uScriptAct_PlayAudioSource_audioClip_110 = default(UnityEngine.AudioClip);
   bool logic_uScriptAct_PlayAudioSource_Out_110 = true;
   //pointer to script instanced logic node
   uScriptAct_Delay logic_uScriptAct_Delay_uScriptAct_Delay_112 = new uScriptAct_Delay( );
   System.Single logic_uScriptAct_Delay_Duration_112 = (float) 0.5;
   System.Boolean logic_uScriptAct_Delay_SingleFrame_112 = (bool) false;
   bool logic_uScriptAct_Delay_Immediate_112 = true;
   bool logic_uScriptAct_Delay_AfterDelay_112 = true;
   bool logic_uScriptAct_Delay_Stopped_112 = true;
   bool logic_uScriptAct_Delay_DrivenDelay_112 = false;
   //pointer to script instanced logic node
   Custom_GradientColorChange logic_Custom_GradientColorChange_Custom_GradientColorChange_119 = new Custom_GradientColorChange( );
   UnityEngine.GameObject logic_Custom_GradientColorChange_go_Target_119 = default(UnityEngine.GameObject);
   Custom_GradientColorChange.e_ColorChannel logic_Custom_GradientColorChange_ecc_ColorChannel_119 = Custom_GradientColorChange.e_ColorChannel.Bottom;
   UnityEngine.Color logic_Custom_GradientColorChange_c_Top_119 = new UnityEngine.Color( (float)0, (float)0, (float)0, (float)1 );
   UnityEngine.Color logic_Custom_GradientColorChange_c_Bottom_119 = UnityEngine.Color.black;
   System.Single logic_Custom_GradientColorChange_f_Time_119 = (float) 5;
   bool logic_Custom_GradientColorChange_Out_119 = true;
   //pointer to script instanced logic node
   uScriptAct_AccessListColor logic_uScriptAct_AccessListColor_uScriptAct_AccessListColor_122 = new uScriptAct_AccessListColor( );
   UnityEngine.Color[] logic_uScriptAct_AccessListColor_List_122 = new UnityEngine.Color[] {};
   System.Int32 logic_uScriptAct_AccessListColor_Index_122 = (int) 0;
   UnityEngine.Color logic_uScriptAct_AccessListColor_Value_122;
   bool logic_uScriptAct_AccessListColor_Out_122 = true;
   //pointer to script instanced logic node
   uScriptAct_Delay logic_uScriptAct_Delay_uScriptAct_Delay_124 = new uScriptAct_Delay( );
   System.Single logic_uScriptAct_Delay_Duration_124 = (float) 10;
   System.Boolean logic_uScriptAct_Delay_SingleFrame_124 = (bool) false;
   bool logic_uScriptAct_Delay_Immediate_124 = true;
   bool logic_uScriptAct_Delay_AfterDelay_124 = true;
   bool logic_uScriptAct_Delay_Stopped_124 = true;
   bool logic_uScriptAct_Delay_DrivenDelay_124 = false;
   //pointer to script instanced logic node
   uScriptAct_SetColor logic_uScriptAct_SetColor_uScriptAct_SetColor_126 = new uScriptAct_SetColor( );
   UnityEngine.Color logic_uScriptAct_SetColor_Value_126 = UnityEngine.Color.black;
   UnityEngine.Color logic_uScriptAct_SetColor_TargetColor_126;
   bool logic_uScriptAct_SetColor_Out_126 = true;
   //pointer to script instanced logic node
   Custom_CompareColor logic_Custom_CompareColor_Custom_CompareColor_131 = new Custom_CompareColor( );
   UnityEngine.Color logic_Custom_CompareColor_c_A_131 = UnityEngine.Color.black;
   UnityEngine.Color logic_Custom_CompareColor_c_B_131 = UnityEngine.Color.black;
   bool logic_Custom_CompareColor_Same_131 = true;
   bool logic_Custom_CompareColor_Different_131 = true;
   //pointer to script instanced logic node
   Custom_ManualSwitch logic_Custom_ManualSwitch_Custom_ManualSwitch_132 = new Custom_ManualSwitch( );
   System.Int32 logic_Custom_ManualSwitch_CurrentOutput_132 = (int) 0;
   //pointer to script instanced logic node
   uScriptAct_SetRandomInt logic_uScriptAct_SetRandomInt_uScriptAct_SetRandomInt_133 = new uScriptAct_SetRandomInt( );
   System.Int32 logic_uScriptAct_SetRandomInt_Min_133 = (int) 1;
   System.Int32 logic_uScriptAct_SetRandomInt_Max_133 = (int) 11;
   System.Int32 logic_uScriptAct_SetRandomInt_TargetInt_133;
   bool logic_uScriptAct_SetRandomInt_Out_133 = true;
   //pointer to script instanced logic node
   uScriptCon_CompareInt logic_uScriptCon_CompareInt_uScriptCon_CompareInt_135 = new uScriptCon_CompareInt( );
   System.Int32 logic_uScriptCon_CompareInt_A_135 = (int) 0;
   System.Int32 logic_uScriptCon_CompareInt_B_135 = (int) 0;
   bool logic_uScriptCon_CompareInt_GreaterThan_135 = true;
   bool logic_uScriptCon_CompareInt_GreaterThanOrEqualTo_135 = true;
   bool logic_uScriptCon_CompareInt_EqualTo_135 = true;
   bool logic_uScriptCon_CompareInt_NotEqualTo_135 = true;
   bool logic_uScriptCon_CompareInt_LessThanOrEqualTo_135 = true;
   bool logic_uScriptCon_CompareInt_LessThan_135 = true;
   //pointer to script instanced logic node
   uScriptAct_SetInt logic_uScriptAct_SetInt_uScriptAct_SetInt_139 = new uScriptAct_SetInt( );
   System.Int32 logic_uScriptAct_SetInt_Value_139 = (int) 0;
   System.Int32 logic_uScriptAct_SetInt_Target_139;
   bool logic_uScriptAct_SetInt_Out_139 = true;
   //pointer to script instanced logic node
   uScriptAct_PlayAudioSource logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_142 = new uScriptAct_PlayAudioSource( );
   UnityEngine.GameObject[] logic_uScriptAct_PlayAudioSource_target_142 = new UnityEngine.GameObject[] {};
   UnityEngine.AudioClip logic_uScriptAct_PlayAudioSource_audioClip_142 = default(UnityEngine.AudioClip);
   bool logic_uScriptAct_PlayAudioSource_Out_142 = true;
   //pointer to script instanced logic node
   uScriptAct_SetBool logic_uScriptAct_SetBool_uScriptAct_SetBool_144 = new uScriptAct_SetBool( );
   System.Boolean logic_uScriptAct_SetBool_Target_144;
   bool logic_uScriptAct_SetBool_Out_144 = true;
   bool logic_uScriptAct_SetBool_SetTrue_144 = true;
   bool logic_uScriptAct_SetBool_SetFalse_144 = true;
   //pointer to script instanced logic node
   uScriptCon_CompareBool logic_uScriptCon_CompareBool_uScriptCon_CompareBool_146 = new uScriptCon_CompareBool( );
   System.Boolean logic_uScriptCon_CompareBool_Bool_146 = (bool) false;
   bool logic_uScriptCon_CompareBool_True_146 = true;
   bool logic_uScriptCon_CompareBool_False_146 = true;
   //pointer to script instanced logic node
   ParticlePlayground_SetEmit logic_ParticlePlayground_SetEmit_ParticlePlayground_SetEmit_148 = new ParticlePlayground_SetEmit( );
   UnityEngine.GameObject logic_ParticlePlayground_SetEmit_go_ParticleObject_148 = default(UnityEngine.GameObject);
   System.Boolean logic_ParticlePlayground_SetEmit_b_Emit_148 = (bool) true;
   bool logic_ParticlePlayground_SetEmit_Out_148 = true;
   //pointer to script instanced logic node
   uScriptAct_SetBool logic_uScriptAct_SetBool_uScriptAct_SetBool_150 = new uScriptAct_SetBool( );
   System.Boolean logic_uScriptAct_SetBool_Target_150;
   bool logic_uScriptAct_SetBool_Out_150 = true;
   bool logic_uScriptAct_SetBool_SetTrue_150 = true;
   bool logic_uScriptAct_SetBool_SetFalse_150 = true;
   //pointer to script instanced logic node
   Custom_LeanTweenMove logic_Custom_LeanTweenMove_Custom_LeanTweenMove_152 = new Custom_LeanTweenMove( );
   UnityEngine.GameObject logic_Custom_LeanTweenMove_go_GameObject_152 = default(UnityEngine.GameObject);
   UnityEngine.Vector3 logic_Custom_LeanTweenMove_v3_Location_152 = new Vector3( (float)0, (float)8, (float)0 );
   System.Single logic_Custom_LeanTweenMove_f_Time_152 = (float) 1;
   bool logic_Custom_LeanTweenMove_Out_152 = true;
   //pointer to script instanced logic node
   uScriptAct_PlayAudioSource logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_153 = new uScriptAct_PlayAudioSource( );
   UnityEngine.GameObject[] logic_uScriptAct_PlayAudioSource_target_153 = new UnityEngine.GameObject[] {};
   UnityEngine.AudioClip logic_uScriptAct_PlayAudioSource_audioClip_153 = default(UnityEngine.AudioClip);
   bool logic_uScriptAct_PlayAudioSource_Out_153 = true;
   //pointer to script instanced logic node
   ParticlePlayground_SetEmit logic_ParticlePlayground_SetEmit_ParticlePlayground_SetEmit_154 = new ParticlePlayground_SetEmit( );
   UnityEngine.GameObject logic_ParticlePlayground_SetEmit_go_ParticleObject_154 = default(UnityEngine.GameObject);
   System.Boolean logic_ParticlePlayground_SetEmit_b_Emit_154 = (bool) true;
   bool logic_ParticlePlayground_SetEmit_Out_154 = true;
   //pointer to script instanced logic node
   uScriptAct_PlayAudioSource logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_155 = new uScriptAct_PlayAudioSource( );
   UnityEngine.GameObject[] logic_uScriptAct_PlayAudioSource_target_155 = new UnityEngine.GameObject[] {};
   UnityEngine.AudioClip logic_uScriptAct_PlayAudioSource_audioClip_155 = default(UnityEngine.AudioClip);
   bool logic_uScriptAct_PlayAudioSource_Out_155 = true;
   //pointer to script instanced logic node
   uScriptAct_SetBool logic_uScriptAct_SetBool_uScriptAct_SetBool_158 = new uScriptAct_SetBool( );
   System.Boolean logic_uScriptAct_SetBool_Target_158;
   bool logic_uScriptAct_SetBool_Out_158 = true;
   bool logic_uScriptAct_SetBool_SetTrue_158 = true;
   bool logic_uScriptAct_SetBool_SetFalse_158 = true;
   //pointer to script instanced logic node
   ParticlePlayground_SetEmit logic_ParticlePlayground_SetEmit_ParticlePlayground_SetEmit_159 = new ParticlePlayground_SetEmit( );
   UnityEngine.GameObject logic_ParticlePlayground_SetEmit_go_ParticleObject_159 = default(UnityEngine.GameObject);
   System.Boolean logic_ParticlePlayground_SetEmit_b_Emit_159 = (bool) false;
   bool logic_ParticlePlayground_SetEmit_Out_159 = true;
   //pointer to script instanced logic node
   ParticlePlayground_SetEmit logic_ParticlePlayground_SetEmit_ParticlePlayground_SetEmit_162 = new ParticlePlayground_SetEmit( );
   UnityEngine.GameObject logic_ParticlePlayground_SetEmit_go_ParticleObject_162 = default(UnityEngine.GameObject);
   System.Boolean logic_ParticlePlayground_SetEmit_b_Emit_162 = (bool) false;
   bool logic_ParticlePlayground_SetEmit_Out_162 = true;
   //pointer to script instanced logic node
   uScriptAct_SetGameObjectPosition logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_163 = new uScriptAct_SetGameObjectPosition( );
   UnityEngine.GameObject[] logic_uScriptAct_SetGameObjectPosition_Target_163 = new UnityEngine.GameObject[] {};
   UnityEngine.Vector3 logic_uScriptAct_SetGameObjectPosition_Position_163 = new Vector3( (float)0, (float)-8, (float)0 );
   System.Boolean logic_uScriptAct_SetGameObjectPosition_AsOffset_163 = (bool) false;
   System.Boolean logic_uScriptAct_SetGameObjectPosition_AsLocal_163 = (bool) false;
   bool logic_uScriptAct_SetGameObjectPosition_Out_163 = true;
   //pointer to script instanced logic node
   uScriptAct_Delay logic_uScriptAct_Delay_uScriptAct_Delay_164 = new uScriptAct_Delay( );
   System.Single logic_uScriptAct_Delay_Duration_164 = (float) 1.5;
   System.Boolean logic_uScriptAct_Delay_SingleFrame_164 = (bool) false;
   bool logic_uScriptAct_Delay_Immediate_164 = true;
   bool logic_uScriptAct_Delay_AfterDelay_164 = true;
   bool logic_uScriptAct_Delay_Stopped_164 = true;
   bool logic_uScriptAct_Delay_DrivenDelay_164 = false;
   //pointer to script instanced logic node
   Custom_SetLineRendererState logic_Custom_SetLineRendererState_Custom_SetLineRendererState_165 = new Custom_SetLineRendererState( );
   UnityEngine.GameObject[] logic_Custom_SetLineRendererState_go_GameObject_165 = new UnityEngine.GameObject[] {};
   System.Boolean logic_Custom_SetLineRendererState_b_Enable_165 = (bool) false;
   bool logic_Custom_SetLineRendererState_Out_165 = true;
   //pointer to script instanced logic node
   uScriptAct_SetBool logic_uScriptAct_SetBool_uScriptAct_SetBool_339 = new uScriptAct_SetBool( );
   System.Boolean logic_uScriptAct_SetBool_Target_339;
   bool logic_uScriptAct_SetBool_Out_339 = true;
   bool logic_uScriptAct_SetBool_SetTrue_339 = true;
   bool logic_uScriptAct_SetBool_SetFalse_339 = true;
   //pointer to script instanced logic node
   Custom_SetLineRendererState logic_Custom_SetLineRendererState_Custom_SetLineRendererState_343 = new Custom_SetLineRendererState( );
   UnityEngine.GameObject[] logic_Custom_SetLineRendererState_go_GameObject_343 = new UnityEngine.GameObject[] {};
   System.Boolean logic_Custom_SetLineRendererState_b_Enable_343 = (bool) true;
   bool logic_Custom_SetLineRendererState_Out_343 = true;
   //pointer to script instanced logic node
   uScriptAct_PlayAudioSource logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_346 = new uScriptAct_PlayAudioSource( );
   UnityEngine.GameObject[] logic_uScriptAct_PlayAudioSource_target_346 = new UnityEngine.GameObject[] {};
   UnityEngine.AudioClip logic_uScriptAct_PlayAudioSource_audioClip_346 = default(UnityEngine.AudioClip);
   bool logic_uScriptAct_PlayAudioSource_Out_346 = true;
   //pointer to script instanced logic node
   uScriptAct_SetBool logic_uScriptAct_SetBool_uScriptAct_SetBool_350 = new uScriptAct_SetBool( );
   System.Boolean logic_uScriptAct_SetBool_Target_350;
   bool logic_uScriptAct_SetBool_Out_350 = true;
   bool logic_uScriptAct_SetBool_SetTrue_350 = true;
   bool logic_uScriptAct_SetBool_SetFalse_350 = true;
   //pointer to script instanced logic node
   Custom_SetLineRendererState logic_Custom_SetLineRendererState_Custom_SetLineRendererState_352 = new Custom_SetLineRendererState( );
   UnityEngine.GameObject[] logic_Custom_SetLineRendererState_go_GameObject_352 = new UnityEngine.GameObject[] {};
   System.Boolean logic_Custom_SetLineRendererState_b_Enable_352 = (bool) false;
   bool logic_Custom_SetLineRendererState_Out_352 = true;
   
   //event nodes
   HedgehogTeam.EasyTouch.Gesture event_UnityEngine_GameObject_g_TargetGesture_0 = default(HedgehogTeam.EasyTouch.Gesture);
   System.Int32 event_UnityEngine_GameObject_i_FingerIndex_0 = (int) 0;
   System.Int32 event_UnityEngine_GameObject_i_TouchCount_0 = (int) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_StartPosition_0 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_StartPosition_0 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_Position_0 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_Position_0 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_DeltaPosition_0 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_DeltaPosition_0 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_ActionTime_0 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_DeltaTime_0 = (float) 0;
   HedgehogTeam.EasyTouch.EasyTouch.SwipeDirection event_UnityEngine_GameObject_etsw_SwipeDirection_0 = HedgehogTeam.EasyTouch.EasyTouch.SwipeDirection.None;
   System.String event_UnityEngine_GameObject_s_SwipeDirection_0 = "";
   System.Single event_UnityEngine_GameObject_f_SwipeLength_0 = (float) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_SwipeVector_0 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_SwipeVector_0 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_DeltaPinch_0 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_TwistAngle_0 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_TwoFingerDistance_0 = (float) 0;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_PickedObject_0 = default(UnityEngine.GameObject);
   UnityEngine.Camera event_UnityEngine_GameObject_c_PickedCamera_0 = default(UnityEngine.Camera);
   System.Boolean event_UnityEngine_GameObject_b_GUICamera_0 = (bool) false;
   System.Boolean event_UnityEngine_GameObject_b_IsOverGUI_0 = (bool) false;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_PickedUIElement_0 = default(UnityEngine.GameObject);
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_TouchToWorldPoint_0 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_TouchToWorldPoint_0 = new Vector3( );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_0 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_0 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_AltitudeAngle_0 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_AzimuthAngle_0 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_MaximumPossiblePressure_0 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_pressure_0 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_Radius_0 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_RadiusVariance_0 = (float) 0;
   HedgehogTeam.EasyTouch.EasyTouch.EvtType event_UnityEngine_GameObject_et_Type_0 = HedgehogTeam.EasyTouch.EasyTouch.EvtType.None;
   System.Single event_UnityEngine_GameObject_f_SwipeDragAngle_0 = (float) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_NormalizedPosition_0 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_NormalizedPosition_0 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.GameObject event_UnityEngine_GameObject_go_CurrentPickedObject_0 = default(UnityEngine.GameObject);
   UnityEngine.GameObject event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_0 = default(UnityEngine.GameObject);
   System.Boolean event_UnityEngine_GameObject_b_OverUIElement_0 = (bool) false;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_FirstUI_0 = default(UnityEngine.GameObject);
   UnityEngine.GameObject event_UnityEngine_GameObject_go_FirstUITwoFingers_0 = default(UnityEngine.GameObject);
   UnityEngine.GameObject event_UnityEngine_GameObject_Instance_0 = default(UnityEngine.GameObject);
   UnityEngine.GameObject event_UnityEngine_GameObject_Instance_54 = default(UnityEngine.GameObject);
   HedgehogTeam.EasyTouch.Gesture event_UnityEngine_GameObject_g_TargetGesture_83 = default(HedgehogTeam.EasyTouch.Gesture);
   System.Int32 event_UnityEngine_GameObject_i_FingerIndex_83 = (int) 0;
   System.Int32 event_UnityEngine_GameObject_i_TouchCount_83 = (int) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_StartPosition_83 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_StartPosition_83 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_Position_83 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_Position_83 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_DeltaPosition_83 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_DeltaPosition_83 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_ActionTime_83 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_DeltaTime_83 = (float) 0;
   HedgehogTeam.EasyTouch.EasyTouch.SwipeDirection event_UnityEngine_GameObject_etsw_SwipeDirection_83 = HedgehogTeam.EasyTouch.EasyTouch.SwipeDirection.None;
   System.String event_UnityEngine_GameObject_s_SwipeDirection_83 = "";
   System.Single event_UnityEngine_GameObject_f_SwipeLength_83 = (float) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_SwipeVector_83 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_SwipeVector_83 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_DeltaPinch_83 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_TwistAngle_83 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_TwoFingerDistance_83 = (float) 0;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_PickedObject_83 = default(UnityEngine.GameObject);
   UnityEngine.Camera event_UnityEngine_GameObject_c_PickedCamera_83 = default(UnityEngine.Camera);
   System.Boolean event_UnityEngine_GameObject_b_GUICamera_83 = (bool) false;
   System.Boolean event_UnityEngine_GameObject_b_IsOverGUI_83 = (bool) false;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_PickedUIElement_83 = default(UnityEngine.GameObject);
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_TouchToWorldPoint_83 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_TouchToWorldPoint_83 = new Vector3( );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_83 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_83 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_AltitudeAngle_83 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_AzimuthAngle_83 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_MaximumPossiblePressure_83 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_pressure_83 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_Radius_83 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_RadiusVariance_83 = (float) 0;
   HedgehogTeam.EasyTouch.EasyTouch.EvtType event_UnityEngine_GameObject_et_Type_83 = HedgehogTeam.EasyTouch.EasyTouch.EvtType.None;
   System.Single event_UnityEngine_GameObject_f_SwipeDragAngle_83 = (float) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_NormalizedPosition_83 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_NormalizedPosition_83 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.GameObject event_UnityEngine_GameObject_go_CurrentPickedObject_83 = default(UnityEngine.GameObject);
   UnityEngine.GameObject event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_83 = default(UnityEngine.GameObject);
   System.Boolean event_UnityEngine_GameObject_b_OverUIElement_83 = (bool) false;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_FirstUI_83 = default(UnityEngine.GameObject);
   UnityEngine.GameObject event_UnityEngine_GameObject_go_FirstUITwoFingers_83 = default(UnityEngine.GameObject);
   UnityEngine.GameObject event_UnityEngine_GameObject_Instance_83 = default(UnityEngine.GameObject);
   UnityEngine.GameObject event_UnityEngine_GameObject_Instance_118 = default(UnityEngine.GameObject);
   
   //property nodes
   
   //method nodes
   #pragma warning restore 414
   
   //functions to refresh properties from entities
   
   void SyncUnityHooks( )
   {
      SyncEventListeners( );
      if ( null == local_CirclePop_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_CirclePop_UnityEngine_GameObject = GameObject.Find( "Circle_Pop" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_CirclePop_UnityEngine_GameObject_previous != local_CirclePop_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_CirclePop_UnityEngine_GameObject_previous = local_CirclePop_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_SquarePop_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_SquarePop_UnityEngine_GameObject = GameObject.Find( "Square_Pop" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_SquarePop_UnityEngine_GameObject_previous != local_SquarePop_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_SquarePop_UnityEngine_GameObject_previous = local_SquarePop_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_StarPop_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_StarPop_UnityEngine_GameObject = GameObject.Find( "Star_Pop" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_StarPop_UnityEngine_GameObject_previous != local_StarPop_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_StarPop_UnityEngine_GameObject_previous = local_StarPop_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_HeartPop_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_HeartPop_UnityEngine_GameObject = GameObject.Find( "Heart_Pop" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_HeartPop_UnityEngine_GameObject_previous != local_HeartPop_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_HeartPop_UnityEngine_GameObject_previous = local_HeartPop_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_TrianglePop_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_TrianglePop_UnityEngine_GameObject = GameObject.Find( "Triangle_Pop" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_TrianglePop_UnityEngine_GameObject_previous != local_TrianglePop_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_TrianglePop_UnityEngine_GameObject_previous = local_TrianglePop_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_60_UnityEngine_GameObjectArray[0] || false == m_RegisteredForEvents )
      {
         local_60_UnityEngine_GameObjectArray[0] = GameObject.Find( "/_3D_Objects/Number_01" ) as UnityEngine.GameObject;
      }
      if ( null == local_60_UnityEngine_GameObjectArray[1] || false == m_RegisteredForEvents )
      {
         local_60_UnityEngine_GameObjectArray[1] = GameObject.Find( "/_3D_Objects/Number_02" ) as UnityEngine.GameObject;
      }
      if ( null == local_60_UnityEngine_GameObjectArray[2] || false == m_RegisteredForEvents )
      {
         local_60_UnityEngine_GameObjectArray[2] = GameObject.Find( "/_3D_Objects/Number_03" ) as UnityEngine.GameObject;
      }
      if ( null == local_60_UnityEngine_GameObjectArray[3] || false == m_RegisteredForEvents )
      {
         local_60_UnityEngine_GameObjectArray[3] = GameObject.Find( "/_3D_Objects/Number_04" ) as UnityEngine.GameObject;
      }
      if ( null == local_Number_01_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_Number_01_UnityEngine_GameObject = GameObject.Find( "/_3D_Objects/Number_01" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_Number_01_UnityEngine_GameObject_previous != local_Number_01_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_Number_01_UnityEngine_GameObject_previous = local_Number_01_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_Number_02_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_Number_02_UnityEngine_GameObject = GameObject.Find( "/_3D_Objects/Number_02" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_Number_02_UnityEngine_GameObject_previous != local_Number_02_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_Number_02_UnityEngine_GameObject_previous = local_Number_02_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_86_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_86_UnityEngine_GameObject = GameObject.Find( "Drag_PS" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_86_UnityEngine_GameObject_previous != local_86_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_86_UnityEngine_GameObject_previous = local_86_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_88_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_88_UnityEngine_GameObject = GameObject.Find( "Drag_PS" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_88_UnityEngine_GameObject_previous != local_88_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_88_UnityEngine_GameObject_previous = local_88_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_91_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_91_UnityEngine_GameObject = GameObject.Find( "Drag_PS" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_91_UnityEngine_GameObject_previous != local_91_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_91_UnityEngine_GameObject_previous = local_91_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_Number_03_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_Number_03_UnityEngine_GameObject = GameObject.Find( "/_3D_Objects/Number_03" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_Number_03_UnityEngine_GameObject_previous != local_Number_03_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_Number_03_UnityEngine_GameObject_previous = local_Number_03_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_Number_04_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_Number_04_UnityEngine_GameObject = GameObject.Find( "/_3D_Objects/Number_04" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_Number_04_UnityEngine_GameObject_previous != local_Number_04_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_Number_04_UnityEngine_GameObject_previous = local_Number_04_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_120_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_120_UnityEngine_GameObject = GameObject.Find( "Img_Background" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_120_UnityEngine_GameObject_previous != local_120_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_120_UnityEngine_GameObject_previous = local_120_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_143_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_143_UnityEngine_GameObject = GameObject.Find( "Drag_PS" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_143_UnityEngine_GameObject_previous != local_143_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_143_UnityEngine_GameObject_previous = local_143_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_149_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_149_UnityEngine_GameObject = GameObject.Find( "Playground Ice Fountain" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_149_UnityEngine_GameObject_previous != local_149_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_149_UnityEngine_GameObject_previous = local_149_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_156_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_156_UnityEngine_GameObject = GameObject.Find( "Fireworks - Fountain" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_156_UnityEngine_GameObject_previous != local_156_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_156_UnityEngine_GameObject_previous = local_156_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_160_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_160_UnityEngine_GameObject = GameObject.Find( "Playground Ice Fountain" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_160_UnityEngine_GameObject_previous != local_160_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_160_UnityEngine_GameObject_previous = local_160_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_161_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_161_UnityEngine_GameObject = GameObject.Find( "Fireworks - Fountain" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_161_UnityEngine_GameObject_previous != local_161_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_161_UnityEngine_GameObject_previous = local_161_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_166_UnityEngine_GameObjectArray[0] || false == m_RegisteredForEvents )
      {
         local_166_UnityEngine_GameObjectArray[0] = GameObject.Find( "/Lightning/LineWaveform_Orange" ) as UnityEngine.GameObject;
      }
      if ( null == local_166_UnityEngine_GameObjectArray[1] || false == m_RegisteredForEvents )
      {
         local_166_UnityEngine_GameObjectArray[1] = GameObject.Find( "/Lightning/LineWaveform_Yellow" ) as UnityEngine.GameObject;
      }
      if ( null == local_166_UnityEngine_GameObjectArray[2] || false == m_RegisteredForEvents )
      {
         local_166_UnityEngine_GameObjectArray[2] = GameObject.Find( "/Lightning/LineWaveform_GreenUp" ) as UnityEngine.GameObject;
      }
      if ( null == local_166_UnityEngine_GameObjectArray[3] || false == m_RegisteredForEvents )
      {
         local_166_UnityEngine_GameObjectArray[3] = GameObject.Find( "/Lightning/LineWaveform_Red" ) as UnityEngine.GameObject;
      }
      if ( null == local_166_UnityEngine_GameObjectArray[4] || false == m_RegisteredForEvents )
      {
         local_166_UnityEngine_GameObjectArray[4] = GameObject.Find( "/Lightning/LineWaveform_Purple" ) as UnityEngine.GameObject;
      }
      if ( null == local_166_UnityEngine_GameObjectArray[5] || false == m_RegisteredForEvents )
      {
         local_166_UnityEngine_GameObjectArray[5] = GameObject.Find( "/Lightning/LineWaveform_GreenDown" ) as UnityEngine.GameObject;
      }
      if ( null == local_166_UnityEngine_GameObjectArray[6] || false == m_RegisteredForEvents )
      {
         local_166_UnityEngine_GameObjectArray[6] = GameObject.Find( "/Lightning/LineWaveform_DarkBlue" ) as UnityEngine.GameObject;
      }
      if ( null == local_166_UnityEngine_GameObjectArray[7] || false == m_RegisteredForEvents )
      {
         local_166_UnityEngine_GameObjectArray[7] = GameObject.Find( "/Lightning/LineWaveform_LightBlue" ) as UnityEngine.GameObject;
      }
      if ( null == local_342_UnityEngine_GameObjectArray[0] || false == m_RegisteredForEvents )
      {
         local_342_UnityEngine_GameObjectArray[0] = GameObject.Find( "/Lightning/LineWaveform_Orange" ) as UnityEngine.GameObject;
      }
      if ( null == local_342_UnityEngine_GameObjectArray[1] || false == m_RegisteredForEvents )
      {
         local_342_UnityEngine_GameObjectArray[1] = GameObject.Find( "/Lightning/LineWaveform_Yellow" ) as UnityEngine.GameObject;
      }
      if ( null == local_342_UnityEngine_GameObjectArray[2] || false == m_RegisteredForEvents )
      {
         local_342_UnityEngine_GameObjectArray[2] = GameObject.Find( "/Lightning/LineWaveform_GreenUp" ) as UnityEngine.GameObject;
      }
      if ( null == local_342_UnityEngine_GameObjectArray[3] || false == m_RegisteredForEvents )
      {
         local_342_UnityEngine_GameObjectArray[3] = GameObject.Find( "/Lightning/LineWaveform_Red" ) as UnityEngine.GameObject;
      }
      if ( null == local_342_UnityEngine_GameObjectArray[4] || false == m_RegisteredForEvents )
      {
         local_342_UnityEngine_GameObjectArray[4] = GameObject.Find( "/Lightning/LineWaveform_Purple" ) as UnityEngine.GameObject;
      }
      if ( null == local_342_UnityEngine_GameObjectArray[5] || false == m_RegisteredForEvents )
      {
         local_342_UnityEngine_GameObjectArray[5] = GameObject.Find( "/Lightning/LineWaveform_GreenDown" ) as UnityEngine.GameObject;
      }
      if ( null == local_342_UnityEngine_GameObjectArray[6] || false == m_RegisteredForEvents )
      {
         local_342_UnityEngine_GameObjectArray[6] = GameObject.Find( "/Lightning/LineWaveform_DarkBlue" ) as UnityEngine.GameObject;
      }
      if ( null == local_342_UnityEngine_GameObjectArray[7] || false == m_RegisteredForEvents )
      {
         local_342_UnityEngine_GameObjectArray[7] = GameObject.Find( "/Lightning/LineWaveform_LightBlue" ) as UnityEngine.GameObject;
      }
      if ( null == local_348_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_348_UnityEngine_GameObject = GameObject.Find( "Chopsticks" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_348_UnityEngine_GameObject_previous != local_348_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_348_UnityEngine_GameObject_previous = local_348_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_353_UnityEngine_GameObjectArray[0] || false == m_RegisteredForEvents )
      {
         local_353_UnityEngine_GameObjectArray[0] = GameObject.Find( "/Lightning/LineWaveform_Orange" ) as UnityEngine.GameObject;
      }
      if ( null == local_353_UnityEngine_GameObjectArray[1] || false == m_RegisteredForEvents )
      {
         local_353_UnityEngine_GameObjectArray[1] = GameObject.Find( "/Lightning/LineWaveform_Yellow" ) as UnityEngine.GameObject;
      }
      if ( null == local_353_UnityEngine_GameObjectArray[2] || false == m_RegisteredForEvents )
      {
         local_353_UnityEngine_GameObjectArray[2] = GameObject.Find( "/Lightning/LineWaveform_GreenUp" ) as UnityEngine.GameObject;
      }
      if ( null == local_353_UnityEngine_GameObjectArray[3] || false == m_RegisteredForEvents )
      {
         local_353_UnityEngine_GameObjectArray[3] = GameObject.Find( "/Lightning/LineWaveform_Red" ) as UnityEngine.GameObject;
      }
      if ( null == local_353_UnityEngine_GameObjectArray[4] || false == m_RegisteredForEvents )
      {
         local_353_UnityEngine_GameObjectArray[4] = GameObject.Find( "/Lightning/LineWaveform_Purple" ) as UnityEngine.GameObject;
      }
      if ( null == local_353_UnityEngine_GameObjectArray[5] || false == m_RegisteredForEvents )
      {
         local_353_UnityEngine_GameObjectArray[5] = GameObject.Find( "/Lightning/LineWaveform_GreenDown" ) as UnityEngine.GameObject;
      }
      if ( null == local_353_UnityEngine_GameObjectArray[6] || false == m_RegisteredForEvents )
      {
         local_353_UnityEngine_GameObjectArray[6] = GameObject.Find( "/Lightning/LineWaveform_DarkBlue" ) as UnityEngine.GameObject;
      }
      if ( null == local_353_UnityEngine_GameObjectArray[7] || false == m_RegisteredForEvents )
      {
         local_353_UnityEngine_GameObjectArray[7] = GameObject.Find( "/Lightning/LineWaveform_LightBlue" ) as UnityEngine.GameObject;
      }
   }
   
   void RegisterForUnityHooks( )
   {
      SyncEventListeners( );
      //if our game object reference was changed then we need to reset event listeners
      if ( local_CirclePop_UnityEngine_GameObject_previous != local_CirclePop_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_CirclePop_UnityEngine_GameObject_previous = local_CirclePop_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_SquarePop_UnityEngine_GameObject_previous != local_SquarePop_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_SquarePop_UnityEngine_GameObject_previous = local_SquarePop_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_StarPop_UnityEngine_GameObject_previous != local_StarPop_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_StarPop_UnityEngine_GameObject_previous = local_StarPop_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_HeartPop_UnityEngine_GameObject_previous != local_HeartPop_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_HeartPop_UnityEngine_GameObject_previous = local_HeartPop_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_TrianglePop_UnityEngine_GameObject_previous != local_TrianglePop_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_TrianglePop_UnityEngine_GameObject_previous = local_TrianglePop_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_Number_01_UnityEngine_GameObject_previous != local_Number_01_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_Number_01_UnityEngine_GameObject_previous = local_Number_01_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_Number_02_UnityEngine_GameObject_previous != local_Number_02_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_Number_02_UnityEngine_GameObject_previous = local_Number_02_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_86_UnityEngine_GameObject_previous != local_86_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_86_UnityEngine_GameObject_previous = local_86_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_88_UnityEngine_GameObject_previous != local_88_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_88_UnityEngine_GameObject_previous = local_88_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_91_UnityEngine_GameObject_previous != local_91_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_91_UnityEngine_GameObject_previous = local_91_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_Number_03_UnityEngine_GameObject_previous != local_Number_03_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_Number_03_UnityEngine_GameObject_previous = local_Number_03_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_Number_04_UnityEngine_GameObject_previous != local_Number_04_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_Number_04_UnityEngine_GameObject_previous = local_Number_04_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_120_UnityEngine_GameObject_previous != local_120_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_120_UnityEngine_GameObject_previous = local_120_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_143_UnityEngine_GameObject_previous != local_143_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_143_UnityEngine_GameObject_previous = local_143_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_149_UnityEngine_GameObject_previous != local_149_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_149_UnityEngine_GameObject_previous = local_149_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_156_UnityEngine_GameObject_previous != local_156_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_156_UnityEngine_GameObject_previous = local_156_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_160_UnityEngine_GameObject_previous != local_160_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_160_UnityEngine_GameObject_previous = local_160_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_161_UnityEngine_GameObject_previous != local_161_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_161_UnityEngine_GameObject_previous = local_161_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_348_UnityEngine_GameObject_previous != local_348_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_348_UnityEngine_GameObject_previous = local_348_UnityEngine_GameObject;
         
         //setup new listeners
      }
   }
   
   void SyncEventListeners( )
   {
      if ( null == event_UnityEngine_GameObject_Instance_0 || false == m_RegisteredForEvents )
      {
         event_UnityEngine_GameObject_Instance_0 = uScript_MasterComponent.LatestMaster;
         if ( null != event_UnityEngine_GameObject_Instance_0 )
         {
            {
               EasyTouch_On_Touch component = event_UnityEngine_GameObject_Instance_0.GetComponent<EasyTouch_On_Touch>();
               if ( null == component )
               {
                  component = event_UnityEngine_GameObject_Instance_0.AddComponent<EasyTouch_On_Touch>();
               }
               if ( null != component )
               {
                  component.OnTouchStart += Instance_OnTouchStart_0;
                  component.OnTouchDown += Instance_OnTouchDown_0;
                  component.OnTouchUp += Instance_OnTouchUp_0;
               }
            }
         }
      }
      if ( null == event_UnityEngine_GameObject_Instance_54 || false == m_RegisteredForEvents )
      {
         event_UnityEngine_GameObject_Instance_54 = uScript_MasterComponent.LatestMaster;
         if ( null != event_UnityEngine_GameObject_Instance_54 )
         {
            {
               uScript_Global component = event_UnityEngine_GameObject_Instance_54.GetComponent<uScript_Global>();
               if ( null == component )
               {
                  component = event_UnityEngine_GameObject_Instance_54.AddComponent<uScript_Global>();
               }
               if ( null != component )
               {
                  component.uScriptStart += Instance_uScriptStart_54;
                  component.uScriptLateStart += Instance_uScriptLateStart_54;
               }
            }
         }
      }
      if ( null == event_UnityEngine_GameObject_Instance_83 || false == m_RegisteredForEvents )
      {
         event_UnityEngine_GameObject_Instance_83 = uScript_MasterComponent.LatestMaster;
         if ( null != event_UnityEngine_GameObject_Instance_83 )
         {
            {
               EasyTouch_On_Swipe component = event_UnityEngine_GameObject_Instance_83.GetComponent<EasyTouch_On_Swipe>();
               if ( null == component )
               {
                  component = event_UnityEngine_GameObject_Instance_83.AddComponent<EasyTouch_On_Swipe>();
               }
               if ( null != component )
               {
                  component.OnSwipeStart += Instance_OnSwipeStart_83;
                  component.OnSwipe += Instance_OnSwipe_83;
                  component.OnSwipeEnd += Instance_OnSwipeEnd_83;
               }
            }
         }
      }
      if ( null == event_UnityEngine_GameObject_Instance_118 || false == m_RegisteredForEvents )
      {
         event_UnityEngine_GameObject_Instance_118 = uScript_MasterComponent.LatestMaster;
         if ( null != event_UnityEngine_GameObject_Instance_118 )
         {
            {
               uScript_Global component = event_UnityEngine_GameObject_Instance_118.GetComponent<uScript_Global>();
               if ( null == component )
               {
                  component = event_UnityEngine_GameObject_Instance_118.AddComponent<uScript_Global>();
               }
               if ( null != component )
               {
                  component.uScriptStart += Instance_uScriptStart_118;
                  component.uScriptLateStart += Instance_uScriptLateStart_118;
               }
            }
         }
      }
   }
   
   void UnregisterEventListeners( )
   {
      if ( null != event_UnityEngine_GameObject_Instance_0 )
      {
         {
            EasyTouch_On_Touch component = event_UnityEngine_GameObject_Instance_0.GetComponent<EasyTouch_On_Touch>();
            if ( null != component )
            {
               component.OnTouchStart -= Instance_OnTouchStart_0;
               component.OnTouchDown -= Instance_OnTouchDown_0;
               component.OnTouchUp -= Instance_OnTouchUp_0;
            }
         }
      }
      if ( null != event_UnityEngine_GameObject_Instance_54 )
      {
         {
            uScript_Global component = event_UnityEngine_GameObject_Instance_54.GetComponent<uScript_Global>();
            if ( null != component )
            {
               component.uScriptStart -= Instance_uScriptStart_54;
               component.uScriptLateStart -= Instance_uScriptLateStart_54;
            }
         }
      }
      if ( null != event_UnityEngine_GameObject_Instance_83 )
      {
         {
            EasyTouch_On_Swipe component = event_UnityEngine_GameObject_Instance_83.GetComponent<EasyTouch_On_Swipe>();
            if ( null != component )
            {
               component.OnSwipeStart -= Instance_OnSwipeStart_83;
               component.OnSwipe -= Instance_OnSwipe_83;
               component.OnSwipeEnd -= Instance_OnSwipeEnd_83;
            }
         }
      }
      if ( null != event_UnityEngine_GameObject_Instance_118 )
      {
         {
            uScript_Global component = event_UnityEngine_GameObject_Instance_118.GetComponent<uScript_Global>();
            if ( null != component )
            {
               component.uScriptStart -= Instance_uScriptStart_118;
               component.uScriptLateStart -= Instance_uScriptLateStart_118;
            }
         }
      }
   }
   
   public override void SetParent(GameObject g)
   {
      parentGameObject = g;
      
      logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_2.SetParent(g);
      logic_uScriptAct_AnimatorSetParameterTrigger_uScriptAct_AnimatorSetParameterTrigger_5.SetParent(g);
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_7.SetParent(g);
      logic_uScriptAct_SetRandomColor_uScriptAct_SetRandomColor_9.SetParent(g);
      logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_11.SetParent(g);
      logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_13.SetParent(g);
      logic_uScriptAct_AnimatorSetParameterTrigger_uScriptAct_AnimatorSetParameterTrigger_16.SetParent(g);
      logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_18.SetParent(g);
      logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_21.SetParent(g);
      logic_uScriptAct_AnimatorSetParameterTrigger_uScriptAct_AnimatorSetParameterTrigger_24.SetParent(g);
      logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_26.SetParent(g);
      logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_29.SetParent(g);
      logic_uScriptAct_AnimatorSetParameterTrigger_uScriptAct_AnimatorSetParameterTrigger_32.SetParent(g);
      logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_34.SetParent(g);
      logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_38.SetParent(g);
      logic_uScriptAct_AnimatorSetParameterTrigger_uScriptAct_AnimatorSetParameterTrigger_39.SetParent(g);
      logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_41.SetParent(g);
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_46.SetParent(g);
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_48.SetParent(g);
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_50.SetParent(g);
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_52.SetParent(g);
      logic_Custom_SetObjectRendererState_Custom_SetObjectRendererState_55.SetParent(g);
      logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_57.SetParent(g);
      logic_Custom_SetObjectRendererState_Custom_SetObjectRendererState_59.SetParent(g);
      logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_61.SetParent(g);
      logic_uScriptAct_Delay_uScriptAct_Delay_63.SetParent(g);
      logic_Custom_SetObjectRendererState_Custom_SetObjectRendererState_64.SetParent(g);
      logic_Custom_SetObjectRendererState_Custom_SetObjectRendererState_65.SetParent(g);
      logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_67.SetParent(g);
      logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_68.SetParent(g);
      logic_Custom_SetObjectRendererState_Custom_SetObjectRendererState_70.SetParent(g);
      logic_uScriptAct_Delay_uScriptAct_Delay_71.SetParent(g);
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_73.SetParent(g);
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_74.SetParent(g);
      logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_85.SetParent(g);
      logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_93.SetParent(g);
      logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_95.SetParent(g);
      logic_Custom_SetObjectRendererState_Custom_SetObjectRendererState_96.SetParent(g);
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_98.SetParent(g);
      logic_uScriptAct_Delay_uScriptAct_Delay_99.SetParent(g);
      logic_Custom_SetObjectRendererState_Custom_SetObjectRendererState_100.SetParent(g);
      logic_Custom_SetObjectRendererState_Custom_SetObjectRendererState_104.SetParent(g);
      logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_105.SetParent(g);
      logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_107.SetParent(g);
      logic_Custom_SetObjectRendererState_Custom_SetObjectRendererState_109.SetParent(g);
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_110.SetParent(g);
      logic_uScriptAct_Delay_uScriptAct_Delay_112.SetParent(g);
      logic_Custom_GradientColorChange_Custom_GradientColorChange_119.SetParent(g);
      logic_uScriptAct_AccessListColor_uScriptAct_AccessListColor_122.SetParent(g);
      logic_uScriptAct_Delay_uScriptAct_Delay_124.SetParent(g);
      logic_uScriptAct_SetColor_uScriptAct_SetColor_126.SetParent(g);
      logic_Custom_CompareColor_Custom_CompareColor_131.SetParent(g);
      logic_Custom_ManualSwitch_Custom_ManualSwitch_132.SetParent(g);
      logic_uScriptAct_SetRandomInt_uScriptAct_SetRandomInt_133.SetParent(g);
      logic_uScriptCon_CompareInt_uScriptCon_CompareInt_135.SetParent(g);
      logic_uScriptAct_SetInt_uScriptAct_SetInt_139.SetParent(g);
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_142.SetParent(g);
      logic_uScriptAct_SetBool_uScriptAct_SetBool_144.SetParent(g);
      logic_uScriptCon_CompareBool_uScriptCon_CompareBool_146.SetParent(g);
      logic_ParticlePlayground_SetEmit_ParticlePlayground_SetEmit_148.SetParent(g);
      logic_uScriptAct_SetBool_uScriptAct_SetBool_150.SetParent(g);
      logic_Custom_LeanTweenMove_Custom_LeanTweenMove_152.SetParent(g);
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_153.SetParent(g);
      logic_ParticlePlayground_SetEmit_ParticlePlayground_SetEmit_154.SetParent(g);
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_155.SetParent(g);
      logic_uScriptAct_SetBool_uScriptAct_SetBool_158.SetParent(g);
      logic_ParticlePlayground_SetEmit_ParticlePlayground_SetEmit_159.SetParent(g);
      logic_ParticlePlayground_SetEmit_ParticlePlayground_SetEmit_162.SetParent(g);
      logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_163.SetParent(g);
      logic_uScriptAct_Delay_uScriptAct_Delay_164.SetParent(g);
      logic_Custom_SetLineRendererState_Custom_SetLineRendererState_165.SetParent(g);
      logic_uScriptAct_SetBool_uScriptAct_SetBool_339.SetParent(g);
      logic_Custom_SetLineRendererState_Custom_SetLineRendererState_343.SetParent(g);
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_346.SetParent(g);
      logic_uScriptAct_SetBool_uScriptAct_SetBool_350.SetParent(g);
      logic_Custom_SetLineRendererState_Custom_SetLineRendererState_352.SetParent(g);
   }
   public void Awake()
   {
      
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_7.Finished += uScriptAct_PlayAudioSource_Finished_7;
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_46.Finished += uScriptAct_PlayAudioSource_Finished_46;
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_48.Finished += uScriptAct_PlayAudioSource_Finished_48;
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_50.Finished += uScriptAct_PlayAudioSource_Finished_50;
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_52.Finished += uScriptAct_PlayAudioSource_Finished_52;
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_73.Finished += uScriptAct_PlayAudioSource_Finished_73;
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_74.Finished += uScriptAct_PlayAudioSource_Finished_74;
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_98.Finished += uScriptAct_PlayAudioSource_Finished_98;
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_110.Finished += uScriptAct_PlayAudioSource_Finished_110;
      logic_Custom_ManualSwitch_Custom_ManualSwitch_132.Case1 += Custom_ManualSwitch_Case1_132;
      logic_Custom_ManualSwitch_Custom_ManualSwitch_132.Case2 += Custom_ManualSwitch_Case2_132;
      logic_Custom_ManualSwitch_Custom_ManualSwitch_132.Case3 += Custom_ManualSwitch_Case3_132;
      logic_Custom_ManualSwitch_Custom_ManualSwitch_132.Case4 += Custom_ManualSwitch_Case4_132;
      logic_Custom_ManualSwitch_Custom_ManualSwitch_132.Case5 += Custom_ManualSwitch_Case5_132;
      logic_Custom_ManualSwitch_Custom_ManualSwitch_132.Case6 += Custom_ManualSwitch_Case6_132;
      logic_Custom_ManualSwitch_Custom_ManualSwitch_132.Case7 += Custom_ManualSwitch_Case7_132;
      logic_Custom_ManualSwitch_Custom_ManualSwitch_132.Case8 += Custom_ManualSwitch_Case8_132;
      logic_Custom_ManualSwitch_Custom_ManualSwitch_132.Case9 += Custom_ManualSwitch_Case9_132;
      logic_Custom_ManualSwitch_Custom_ManualSwitch_132.Case10 += Custom_ManualSwitch_Case10_132;
      logic_Custom_ManualSwitch_Custom_ManualSwitch_132.Case11 += Custom_ManualSwitch_Case11_132;
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_142.Finished += uScriptAct_PlayAudioSource_Finished_142;
      logic_Custom_LeanTweenMove_Custom_LeanTweenMove_152.OnComplete += Custom_LeanTweenMove_OnComplete_152;
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_153.Finished += uScriptAct_PlayAudioSource_Finished_153;
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_155.Finished += uScriptAct_PlayAudioSource_Finished_155;
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_346.Finished += uScriptAct_PlayAudioSource_Finished_346;
   }
   
   public void Start()
   {
      SyncUnityHooks( );
      m_RegisteredForEvents = true;
      
   }
   
   public void OnEnable()
   {
      RegisterForUnityHooks( );
      m_RegisteredForEvents = true;
   }
   
   public void OnDisable()
   {
      UnregisterEventListeners( );
      m_RegisteredForEvents = false;
   }
   
   public void Update()
   {
      
      //other scripts might have added GameObjects with event scripts
      //so we need to verify all our event listeners are registered
      SyncEventListeners( );
      
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_7.Update( );
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_46.Update( );
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_48.Update( );
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_50.Update( );
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_52.Update( );
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_73.Update( );
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_74.Update( );
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_98.Update( );
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_110.Update( );
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_142.Update( );
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_153.Update( );
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_155.Update( );
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_346.Update( );
      if (true == logic_uScriptAct_Delay_DrivenDelay_63)
      {
         Relay_DrivenDelay_63();
      }
      if (true == logic_uScriptAct_Delay_DrivenDelay_71)
      {
         Relay_DrivenDelay_71();
      }
      if (true == logic_uScriptAct_Delay_DrivenDelay_99)
      {
         Relay_DrivenDelay_99();
      }
      if (true == logic_uScriptAct_Delay_DrivenDelay_112)
      {
         Relay_DrivenDelay_112();
      }
      if (true == logic_uScriptAct_Delay_DrivenDelay_124)
      {
         Relay_DrivenDelay_124();
      }
      if (true == logic_uScriptAct_Delay_DrivenDelay_164)
      {
         Relay_DrivenDelay_164();
      }
   }
   
   public void OnDestroy()
   {
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_7.Finished -= uScriptAct_PlayAudioSource_Finished_7;
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_46.Finished -= uScriptAct_PlayAudioSource_Finished_46;
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_48.Finished -= uScriptAct_PlayAudioSource_Finished_48;
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_50.Finished -= uScriptAct_PlayAudioSource_Finished_50;
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_52.Finished -= uScriptAct_PlayAudioSource_Finished_52;
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_73.Finished -= uScriptAct_PlayAudioSource_Finished_73;
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_74.Finished -= uScriptAct_PlayAudioSource_Finished_74;
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_98.Finished -= uScriptAct_PlayAudioSource_Finished_98;
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_110.Finished -= uScriptAct_PlayAudioSource_Finished_110;
      logic_Custom_ManualSwitch_Custom_ManualSwitch_132.Case1 -= Custom_ManualSwitch_Case1_132;
      logic_Custom_ManualSwitch_Custom_ManualSwitch_132.Case2 -= Custom_ManualSwitch_Case2_132;
      logic_Custom_ManualSwitch_Custom_ManualSwitch_132.Case3 -= Custom_ManualSwitch_Case3_132;
      logic_Custom_ManualSwitch_Custom_ManualSwitch_132.Case4 -= Custom_ManualSwitch_Case4_132;
      logic_Custom_ManualSwitch_Custom_ManualSwitch_132.Case5 -= Custom_ManualSwitch_Case5_132;
      logic_Custom_ManualSwitch_Custom_ManualSwitch_132.Case6 -= Custom_ManualSwitch_Case6_132;
      logic_Custom_ManualSwitch_Custom_ManualSwitch_132.Case7 -= Custom_ManualSwitch_Case7_132;
      logic_Custom_ManualSwitch_Custom_ManualSwitch_132.Case8 -= Custom_ManualSwitch_Case8_132;
      logic_Custom_ManualSwitch_Custom_ManualSwitch_132.Case9 -= Custom_ManualSwitch_Case9_132;
      logic_Custom_ManualSwitch_Custom_ManualSwitch_132.Case10 -= Custom_ManualSwitch_Case10_132;
      logic_Custom_ManualSwitch_Custom_ManualSwitch_132.Case11 -= Custom_ManualSwitch_Case11_132;
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_142.Finished -= uScriptAct_PlayAudioSource_Finished_142;
      logic_Custom_LeanTweenMove_Custom_LeanTweenMove_152.OnComplete -= Custom_LeanTweenMove_OnComplete_152;
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_153.Finished -= uScriptAct_PlayAudioSource_Finished_153;
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_155.Finished -= uScriptAct_PlayAudioSource_Finished_155;
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_346.Finished -= uScriptAct_PlayAudioSource_Finished_346;
   }
   
   void Instance_OnTouchStart_0(object o, EasyTouch_On_Touch.OnTouchEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_0 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_0 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_0 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_0 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_0 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_0 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_0 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_0 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_0 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_0 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_0 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_0 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_0 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_0 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_0 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_0 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_0 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_0 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_0 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_0 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_0 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_0 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_0 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_0 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_0 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_0 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_0 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_0 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_0 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_0 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_0 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_0 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_0 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_0 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_0 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_0 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_0 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_0 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_0 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_0 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_0 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_0 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_0 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnTouchStart_0( );
   }
   
   void Instance_OnTouchDown_0(object o, EasyTouch_On_Touch.OnTouchEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_0 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_0 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_0 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_0 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_0 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_0 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_0 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_0 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_0 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_0 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_0 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_0 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_0 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_0 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_0 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_0 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_0 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_0 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_0 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_0 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_0 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_0 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_0 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_0 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_0 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_0 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_0 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_0 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_0 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_0 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_0 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_0 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_0 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_0 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_0 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_0 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_0 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_0 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_0 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_0 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_0 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_0 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_0 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnTouchDown_0( );
   }
   
   void Instance_OnTouchUp_0(object o, EasyTouch_On_Touch.OnTouchEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_0 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_0 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_0 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_0 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_0 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_0 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_0 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_0 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_0 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_0 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_0 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_0 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_0 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_0 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_0 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_0 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_0 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_0 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_0 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_0 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_0 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_0 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_0 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_0 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_0 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_0 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_0 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_0 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_0 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_0 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_0 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_0 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_0 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_0 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_0 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_0 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_0 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_0 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_0 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_0 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_0 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_0 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_0 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnTouchUp_0( );
   }
   
   void Instance_uScriptStart_54(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_uScriptStart_54( );
   }
   
   void Instance_uScriptLateStart_54(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_uScriptLateStart_54( );
   }
   
   void Instance_OnSwipeStart_83(object o, EasyTouch_On_Swipe.OnSwipeEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_83 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_83 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_83 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_83 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_83 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_83 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_83 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_83 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_83 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_83 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_83 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_83 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_83 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_83 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_83 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_83 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_83 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_83 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_83 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_83 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_83 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_83 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_83 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_83 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_83 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_83 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_83 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_83 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_83 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_83 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_83 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_83 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_83 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_83 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_83 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_83 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_83 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_83 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_83 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_83 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_83 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_83 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_83 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnSwipeStart_83( );
   }
   
   void Instance_OnSwipe_83(object o, EasyTouch_On_Swipe.OnSwipeEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_83 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_83 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_83 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_83 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_83 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_83 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_83 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_83 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_83 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_83 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_83 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_83 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_83 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_83 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_83 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_83 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_83 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_83 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_83 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_83 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_83 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_83 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_83 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_83 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_83 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_83 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_83 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_83 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_83 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_83 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_83 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_83 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_83 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_83 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_83 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_83 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_83 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_83 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_83 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_83 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_83 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_83 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_83 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnSwipe_83( );
   }
   
   void Instance_OnSwipeEnd_83(object o, EasyTouch_On_Swipe.OnSwipeEventArgs e)
   {
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_83 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_83 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_83 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_83 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_83 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_83 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_83 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_83 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_83 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_83 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_83 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_83 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_83 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_83 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_83 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_83 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_83 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_83 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_83 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_83 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_83 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_83 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_83 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_83 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_83 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_83 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_83 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_83 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_83 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_83 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_83 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_83 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_83 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_83 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_83 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_83 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_83 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_83 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_83 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_83 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_83 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_83 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_83 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnSwipeEnd_83( );
   }
   
   void Instance_uScriptStart_118(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_uScriptStart_118( );
   }
   
   void Instance_uScriptLateStart_118(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_uScriptLateStart_118( );
   }
   
   void uScriptAct_PlayAudioSource_Finished_7(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_Finished_7( );
   }
   
   void uScriptAct_PlayAudioSource_Finished_46(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_Finished_46( );
   }
   
   void uScriptAct_PlayAudioSource_Finished_48(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_Finished_48( );
   }
   
   void uScriptAct_PlayAudioSource_Finished_50(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_Finished_50( );
   }
   
   void uScriptAct_PlayAudioSource_Finished_52(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_Finished_52( );
   }
   
   void uScriptAct_PlayAudioSource_Finished_73(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_Finished_73( );
   }
   
   void uScriptAct_PlayAudioSource_Finished_74(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_Finished_74( );
   }
   
   void uScriptAct_PlayAudioSource_Finished_98(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_Finished_98( );
   }
   
   void uScriptAct_PlayAudioSource_Finished_110(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_Finished_110( );
   }
   
   void Custom_ManualSwitch_Case1_132(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_Case1_132( );
   }
   
   void Custom_ManualSwitch_Case2_132(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_Case2_132( );
   }
   
   void Custom_ManualSwitch_Case3_132(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_Case3_132( );
   }
   
   void Custom_ManualSwitch_Case4_132(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_Case4_132( );
   }
   
   void Custom_ManualSwitch_Case5_132(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_Case5_132( );
   }
   
   void Custom_ManualSwitch_Case6_132(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_Case6_132( );
   }
   
   void Custom_ManualSwitch_Case7_132(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_Case7_132( );
   }
   
   void Custom_ManualSwitch_Case8_132(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_Case8_132( );
   }
   
   void Custom_ManualSwitch_Case9_132(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_Case9_132( );
   }
   
   void Custom_ManualSwitch_Case10_132(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_Case10_132( );
   }
   
   void Custom_ManualSwitch_Case11_132(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_Case11_132( );
   }
   
   void uScriptAct_PlayAudioSource_Finished_142(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_Finished_142( );
   }
   
   void Custom_LeanTweenMove_OnComplete_152(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_OnComplete_152( );
   }
   
   void uScriptAct_PlayAudioSource_Finished_153(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_Finished_153( );
   }
   
   void uScriptAct_PlayAudioSource_Finished_155(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_Finished_155( );
   }
   
   void uScriptAct_PlayAudioSource_Finished_346(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_Finished_346( );
   }
   
   void Relay_OnTouchStart_0()
   {
      local_WorldPoint_UnityEngine_Vector3 = event_UnityEngine_GameObject_v3_TouchToWorldPoint_0;
   }
   
   void Relay_OnTouchDown_0()
   {
      local_WorldPoint_UnityEngine_Vector3 = event_UnityEngine_GameObject_v3_TouchToWorldPoint_0;
   }
   
   void Relay_OnTouchUp_0()
   {
      local_WorldPoint_UnityEngine_Vector3 = event_UnityEngine_GameObject_v3_TouchToWorldPoint_0;
      Relay_In_146();
   }
   
   void Relay_In_2()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_CirclePop_UnityEngine_GameObject_previous != local_CirclePop_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_CirclePop_UnityEngine_GameObject_previous = local_CirclePop_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptAct_SetGameObjectPosition_Target_2.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_SetGameObjectPosition_Target_2, index + 1);
            }
            logic_uScriptAct_SetGameObjectPosition_Target_2[ index++ ] = local_CirclePop_UnityEngine_GameObject;
            
         }
         {
            logic_uScriptAct_SetGameObjectPosition_Position_2 = local_WorldPoint_UnityEngine_Vector3;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_2.In(logic_uScriptAct_SetGameObjectPosition_Target_2, logic_uScriptAct_SetGameObjectPosition_Position_2, logic_uScriptAct_SetGameObjectPosition_AsOffset_2, logic_uScriptAct_SetGameObjectPosition_AsLocal_2);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_2.Out;
      
      if ( test_0 == true )
      {
         Relay_In_5();
      }
   }
   
   void Relay_In_5()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_CirclePop_UnityEngine_GameObject_previous != local_CirclePop_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_CirclePop_UnityEngine_GameObject_previous = local_CirclePop_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptAct_AnimatorSetParameterTrigger_Target_5.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_AnimatorSetParameterTrigger_Target_5, index + 1);
            }
            logic_uScriptAct_AnimatorSetParameterTrigger_Target_5[ index++ ] = local_CirclePop_UnityEngine_GameObject;
            
         }
         {
         }
      }
      logic_uScriptAct_AnimatorSetParameterTrigger_uScriptAct_AnimatorSetParameterTrigger_5.In(logic_uScriptAct_AnimatorSetParameterTrigger_Target_5, logic_uScriptAct_AnimatorSetParameterTrigger_Name_5);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_AnimatorSetParameterTrigger_uScriptAct_AnimatorSetParameterTrigger_5.Out;
      
      if ( test_0 == true )
      {
         Relay_In_11();
      }
   }
   
   void Relay_Finished_7()
   {
   }
   
   void Relay_Play_7()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_CirclePop_UnityEngine_GameObject_previous != local_CirclePop_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_CirclePop_UnityEngine_GameObject_previous = local_CirclePop_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptAct_PlayAudioSource_target_7.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_PlayAudioSource_target_7, index + 1);
            }
            logic_uScriptAct_PlayAudioSource_target_7[ index++ ] = local_CirclePop_UnityEngine_GameObject;
            
         }
         {
         }
      }
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_7.Play(logic_uScriptAct_PlayAudioSource_target_7, logic_uScriptAct_PlayAudioSource_audioClip_7);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_Stop_7()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_CirclePop_UnityEngine_GameObject_previous != local_CirclePop_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_CirclePop_UnityEngine_GameObject_previous = local_CirclePop_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptAct_PlayAudioSource_target_7.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_PlayAudioSource_target_7, index + 1);
            }
            logic_uScriptAct_PlayAudioSource_target_7[ index++ ] = local_CirclePop_UnityEngine_GameObject;
            
         }
         {
         }
      }
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_7.Stop(logic_uScriptAct_PlayAudioSource_target_7, logic_uScriptAct_PlayAudioSource_audioClip_7);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_9()
   {
      {
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_SetRandomColor_uScriptAct_SetRandomColor_9.In(logic_uScriptAct_SetRandomColor_RedMin_9, logic_uScriptAct_SetRandomColor_RedMax_9, logic_uScriptAct_SetRandomColor_GreenMin_9, logic_uScriptAct_SetRandomColor_GreenMax_9, logic_uScriptAct_SetRandomColor_BlueMin_9, logic_uScriptAct_SetRandomColor_BlueMax_9, logic_uScriptAct_SetRandomColor_AlphaMin_9, logic_uScriptAct_SetRandomColor_AlphaMax_9, out logic_uScriptAct_SetRandomColor_TargetColor_9);
      local_RandomColor_UnityEngine_Color = logic_uScriptAct_SetRandomColor_TargetColor_9;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_SetRandomColor_uScriptAct_SetRandomColor_9.Out;
      
      if ( test_0 == true )
      {
         Relay_In_133();
      }
   }
   
   void Relay_In_11()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_CirclePop_UnityEngine_GameObject_previous != local_CirclePop_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_CirclePop_UnityEngine_GameObject_previous = local_CirclePop_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptAct_AssignMaterialColor_Target_11.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_AssignMaterialColor_Target_11, index + 1);
            }
            logic_uScriptAct_AssignMaterialColor_Target_11[ index++ ] = local_CirclePop_UnityEngine_GameObject;
            
         }
         {
            logic_uScriptAct_AssignMaterialColor_MatColor_11 = local_RandomColor_UnityEngine_Color;
            
         }
         {
         }
      }
      logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_11.In(logic_uScriptAct_AssignMaterialColor_Target_11, logic_uScriptAct_AssignMaterialColor_MatColor_11, logic_uScriptAct_AssignMaterialColor_MatChannel_11);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_11.Out;
      
      if ( test_0 == true )
      {
         Relay_Play_7();
      }
   }
   
   void Relay_In_13()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_SquarePop_UnityEngine_GameObject_previous != local_SquarePop_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_SquarePop_UnityEngine_GameObject_previous = local_SquarePop_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptAct_SetGameObjectPosition_Target_13.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_SetGameObjectPosition_Target_13, index + 1);
            }
            logic_uScriptAct_SetGameObjectPosition_Target_13[ index++ ] = local_SquarePop_UnityEngine_GameObject;
            
         }
         {
            logic_uScriptAct_SetGameObjectPosition_Position_13 = local_WorldPoint_UnityEngine_Vector3;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_13.In(logic_uScriptAct_SetGameObjectPosition_Target_13, logic_uScriptAct_SetGameObjectPosition_Position_13, logic_uScriptAct_SetGameObjectPosition_AsOffset_13, logic_uScriptAct_SetGameObjectPosition_AsLocal_13);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_13.Out;
      
      if ( test_0 == true )
      {
         Relay_In_16();
      }
   }
   
   void Relay_In_16()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_SquarePop_UnityEngine_GameObject_previous != local_SquarePop_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_SquarePop_UnityEngine_GameObject_previous = local_SquarePop_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptAct_AnimatorSetParameterTrigger_Target_16.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_AnimatorSetParameterTrigger_Target_16, index + 1);
            }
            logic_uScriptAct_AnimatorSetParameterTrigger_Target_16[ index++ ] = local_SquarePop_UnityEngine_GameObject;
            
         }
         {
         }
      }
      logic_uScriptAct_AnimatorSetParameterTrigger_uScriptAct_AnimatorSetParameterTrigger_16.In(logic_uScriptAct_AnimatorSetParameterTrigger_Target_16, logic_uScriptAct_AnimatorSetParameterTrigger_Name_16);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_AnimatorSetParameterTrigger_uScriptAct_AnimatorSetParameterTrigger_16.Out;
      
      if ( test_0 == true )
      {
         Relay_In_18();
      }
   }
   
   void Relay_In_18()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_SquarePop_UnityEngine_GameObject_previous != local_SquarePop_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_SquarePop_UnityEngine_GameObject_previous = local_SquarePop_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptAct_AssignMaterialColor_Target_18.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_AssignMaterialColor_Target_18, index + 1);
            }
            logic_uScriptAct_AssignMaterialColor_Target_18[ index++ ] = local_SquarePop_UnityEngine_GameObject;
            
         }
         {
            logic_uScriptAct_AssignMaterialColor_MatColor_18 = local_RandomColor_UnityEngine_Color;
            
         }
         {
         }
      }
      logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_18.In(logic_uScriptAct_AssignMaterialColor_Target_18, logic_uScriptAct_AssignMaterialColor_MatColor_18, logic_uScriptAct_AssignMaterialColor_MatChannel_18);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_18.Out;
      
      if ( test_0 == true )
      {
         Relay_Play_46();
      }
   }
   
   void Relay_In_21()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_StarPop_UnityEngine_GameObject_previous != local_StarPop_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_StarPop_UnityEngine_GameObject_previous = local_StarPop_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptAct_SetGameObjectPosition_Target_21.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_SetGameObjectPosition_Target_21, index + 1);
            }
            logic_uScriptAct_SetGameObjectPosition_Target_21[ index++ ] = local_StarPop_UnityEngine_GameObject;
            
         }
         {
            logic_uScriptAct_SetGameObjectPosition_Position_21 = local_WorldPoint_UnityEngine_Vector3;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_21.In(logic_uScriptAct_SetGameObjectPosition_Target_21, logic_uScriptAct_SetGameObjectPosition_Position_21, logic_uScriptAct_SetGameObjectPosition_AsOffset_21, logic_uScriptAct_SetGameObjectPosition_AsLocal_21);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_21.Out;
      
      if ( test_0 == true )
      {
         Relay_In_24();
      }
   }
   
   void Relay_In_24()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_StarPop_UnityEngine_GameObject_previous != local_StarPop_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_StarPop_UnityEngine_GameObject_previous = local_StarPop_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptAct_AnimatorSetParameterTrigger_Target_24.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_AnimatorSetParameterTrigger_Target_24, index + 1);
            }
            logic_uScriptAct_AnimatorSetParameterTrigger_Target_24[ index++ ] = local_StarPop_UnityEngine_GameObject;
            
         }
         {
         }
      }
      logic_uScriptAct_AnimatorSetParameterTrigger_uScriptAct_AnimatorSetParameterTrigger_24.In(logic_uScriptAct_AnimatorSetParameterTrigger_Target_24, logic_uScriptAct_AnimatorSetParameterTrigger_Name_24);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_AnimatorSetParameterTrigger_uScriptAct_AnimatorSetParameterTrigger_24.Out;
      
      if ( test_0 == true )
      {
         Relay_In_26();
      }
   }
   
   void Relay_In_26()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_StarPop_UnityEngine_GameObject_previous != local_StarPop_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_StarPop_UnityEngine_GameObject_previous = local_StarPop_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptAct_AssignMaterialColor_Target_26.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_AssignMaterialColor_Target_26, index + 1);
            }
            logic_uScriptAct_AssignMaterialColor_Target_26[ index++ ] = local_StarPop_UnityEngine_GameObject;
            
         }
         {
            logic_uScriptAct_AssignMaterialColor_MatColor_26 = local_RandomColor_UnityEngine_Color;
            
         }
         {
         }
      }
      logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_26.In(logic_uScriptAct_AssignMaterialColor_Target_26, logic_uScriptAct_AssignMaterialColor_MatColor_26, logic_uScriptAct_AssignMaterialColor_MatChannel_26);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_26.Out;
      
      if ( test_0 == true )
      {
         Relay_Play_48();
      }
   }
   
   void Relay_In_29()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_HeartPop_UnityEngine_GameObject_previous != local_HeartPop_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_HeartPop_UnityEngine_GameObject_previous = local_HeartPop_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptAct_SetGameObjectPosition_Target_29.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_SetGameObjectPosition_Target_29, index + 1);
            }
            logic_uScriptAct_SetGameObjectPosition_Target_29[ index++ ] = local_HeartPop_UnityEngine_GameObject;
            
         }
         {
            logic_uScriptAct_SetGameObjectPosition_Position_29 = local_WorldPoint_UnityEngine_Vector3;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_29.In(logic_uScriptAct_SetGameObjectPosition_Target_29, logic_uScriptAct_SetGameObjectPosition_Position_29, logic_uScriptAct_SetGameObjectPosition_AsOffset_29, logic_uScriptAct_SetGameObjectPosition_AsLocal_29);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_29.Out;
      
      if ( test_0 == true )
      {
         Relay_In_32();
      }
   }
   
   void Relay_In_32()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_HeartPop_UnityEngine_GameObject_previous != local_HeartPop_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_HeartPop_UnityEngine_GameObject_previous = local_HeartPop_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptAct_AnimatorSetParameterTrigger_Target_32.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_AnimatorSetParameterTrigger_Target_32, index + 1);
            }
            logic_uScriptAct_AnimatorSetParameterTrigger_Target_32[ index++ ] = local_HeartPop_UnityEngine_GameObject;
            
         }
         {
         }
      }
      logic_uScriptAct_AnimatorSetParameterTrigger_uScriptAct_AnimatorSetParameterTrigger_32.In(logic_uScriptAct_AnimatorSetParameterTrigger_Target_32, logic_uScriptAct_AnimatorSetParameterTrigger_Name_32);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_AnimatorSetParameterTrigger_uScriptAct_AnimatorSetParameterTrigger_32.Out;
      
      if ( test_0 == true )
      {
         Relay_In_34();
      }
   }
   
   void Relay_In_34()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_HeartPop_UnityEngine_GameObject_previous != local_HeartPop_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_HeartPop_UnityEngine_GameObject_previous = local_HeartPop_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptAct_AssignMaterialColor_Target_34.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_AssignMaterialColor_Target_34, index + 1);
            }
            logic_uScriptAct_AssignMaterialColor_Target_34[ index++ ] = local_HeartPop_UnityEngine_GameObject;
            
         }
         {
            logic_uScriptAct_AssignMaterialColor_MatColor_34 = local_RandomColor_UnityEngine_Color;
            
         }
         {
         }
      }
      logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_34.In(logic_uScriptAct_AssignMaterialColor_Target_34, logic_uScriptAct_AssignMaterialColor_MatColor_34, logic_uScriptAct_AssignMaterialColor_MatChannel_34);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_34.Out;
      
      if ( test_0 == true )
      {
         Relay_Play_50();
      }
   }
   
   void Relay_In_38()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_TrianglePop_UnityEngine_GameObject_previous != local_TrianglePop_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_TrianglePop_UnityEngine_GameObject_previous = local_TrianglePop_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptAct_SetGameObjectPosition_Target_38.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_SetGameObjectPosition_Target_38, index + 1);
            }
            logic_uScriptAct_SetGameObjectPosition_Target_38[ index++ ] = local_TrianglePop_UnityEngine_GameObject;
            
         }
         {
            logic_uScriptAct_SetGameObjectPosition_Position_38 = local_WorldPoint_UnityEngine_Vector3;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_38.In(logic_uScriptAct_SetGameObjectPosition_Target_38, logic_uScriptAct_SetGameObjectPosition_Position_38, logic_uScriptAct_SetGameObjectPosition_AsOffset_38, logic_uScriptAct_SetGameObjectPosition_AsLocal_38);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_38.Out;
      
      if ( test_0 == true )
      {
         Relay_In_39();
      }
   }
   
   void Relay_In_39()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_TrianglePop_UnityEngine_GameObject_previous != local_TrianglePop_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_TrianglePop_UnityEngine_GameObject_previous = local_TrianglePop_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptAct_AnimatorSetParameterTrigger_Target_39.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_AnimatorSetParameterTrigger_Target_39, index + 1);
            }
            logic_uScriptAct_AnimatorSetParameterTrigger_Target_39[ index++ ] = local_TrianglePop_UnityEngine_GameObject;
            
         }
         {
         }
      }
      logic_uScriptAct_AnimatorSetParameterTrigger_uScriptAct_AnimatorSetParameterTrigger_39.In(logic_uScriptAct_AnimatorSetParameterTrigger_Target_39, logic_uScriptAct_AnimatorSetParameterTrigger_Name_39);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_AnimatorSetParameterTrigger_uScriptAct_AnimatorSetParameterTrigger_39.Out;
      
      if ( test_0 == true )
      {
         Relay_In_41();
      }
   }
   
   void Relay_In_41()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_TrianglePop_UnityEngine_GameObject_previous != local_TrianglePop_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_TrianglePop_UnityEngine_GameObject_previous = local_TrianglePop_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptAct_AssignMaterialColor_Target_41.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_AssignMaterialColor_Target_41, index + 1);
            }
            logic_uScriptAct_AssignMaterialColor_Target_41[ index++ ] = local_TrianglePop_UnityEngine_GameObject;
            
         }
         {
            logic_uScriptAct_AssignMaterialColor_MatColor_41 = local_RandomColor_UnityEngine_Color;
            
         }
         {
         }
      }
      logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_41.In(logic_uScriptAct_AssignMaterialColor_Target_41, logic_uScriptAct_AssignMaterialColor_MatColor_41, logic_uScriptAct_AssignMaterialColor_MatChannel_41);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_41.Out;
      
      if ( test_0 == true )
      {
         Relay_Play_52();
      }
   }
   
   void Relay_Finished_46()
   {
   }
   
   void Relay_Play_46()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_SquarePop_UnityEngine_GameObject_previous != local_SquarePop_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_SquarePop_UnityEngine_GameObject_previous = local_SquarePop_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptAct_PlayAudioSource_target_46.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_PlayAudioSource_target_46, index + 1);
            }
            logic_uScriptAct_PlayAudioSource_target_46[ index++ ] = local_SquarePop_UnityEngine_GameObject;
            
         }
         {
         }
      }
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_46.Play(logic_uScriptAct_PlayAudioSource_target_46, logic_uScriptAct_PlayAudioSource_audioClip_46);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_Stop_46()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_SquarePop_UnityEngine_GameObject_previous != local_SquarePop_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_SquarePop_UnityEngine_GameObject_previous = local_SquarePop_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptAct_PlayAudioSource_target_46.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_PlayAudioSource_target_46, index + 1);
            }
            logic_uScriptAct_PlayAudioSource_target_46[ index++ ] = local_SquarePop_UnityEngine_GameObject;
            
         }
         {
         }
      }
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_46.Stop(logic_uScriptAct_PlayAudioSource_target_46, logic_uScriptAct_PlayAudioSource_audioClip_46);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_Finished_48()
   {
   }
   
   void Relay_Play_48()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_StarPop_UnityEngine_GameObject_previous != local_StarPop_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_StarPop_UnityEngine_GameObject_previous = local_StarPop_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptAct_PlayAudioSource_target_48.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_PlayAudioSource_target_48, index + 1);
            }
            logic_uScriptAct_PlayAudioSource_target_48[ index++ ] = local_StarPop_UnityEngine_GameObject;
            
         }
         {
         }
      }
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_48.Play(logic_uScriptAct_PlayAudioSource_target_48, logic_uScriptAct_PlayAudioSource_audioClip_48);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_Stop_48()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_StarPop_UnityEngine_GameObject_previous != local_StarPop_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_StarPop_UnityEngine_GameObject_previous = local_StarPop_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptAct_PlayAudioSource_target_48.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_PlayAudioSource_target_48, index + 1);
            }
            logic_uScriptAct_PlayAudioSource_target_48[ index++ ] = local_StarPop_UnityEngine_GameObject;
            
         }
         {
         }
      }
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_48.Stop(logic_uScriptAct_PlayAudioSource_target_48, logic_uScriptAct_PlayAudioSource_audioClip_48);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_Finished_50()
   {
   }
   
   void Relay_Play_50()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_HeartPop_UnityEngine_GameObject_previous != local_HeartPop_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_HeartPop_UnityEngine_GameObject_previous = local_HeartPop_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptAct_PlayAudioSource_target_50.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_PlayAudioSource_target_50, index + 1);
            }
            logic_uScriptAct_PlayAudioSource_target_50[ index++ ] = local_HeartPop_UnityEngine_GameObject;
            
         }
         {
         }
      }
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_50.Play(logic_uScriptAct_PlayAudioSource_target_50, logic_uScriptAct_PlayAudioSource_audioClip_50);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_Stop_50()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_HeartPop_UnityEngine_GameObject_previous != local_HeartPop_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_HeartPop_UnityEngine_GameObject_previous = local_HeartPop_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptAct_PlayAudioSource_target_50.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_PlayAudioSource_target_50, index + 1);
            }
            logic_uScriptAct_PlayAudioSource_target_50[ index++ ] = local_HeartPop_UnityEngine_GameObject;
            
         }
         {
         }
      }
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_50.Stop(logic_uScriptAct_PlayAudioSource_target_50, logic_uScriptAct_PlayAudioSource_audioClip_50);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_Finished_52()
   {
   }
   
   void Relay_Play_52()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_TrianglePop_UnityEngine_GameObject_previous != local_TrianglePop_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_TrianglePop_UnityEngine_GameObject_previous = local_TrianglePop_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptAct_PlayAudioSource_target_52.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_PlayAudioSource_target_52, index + 1);
            }
            logic_uScriptAct_PlayAudioSource_target_52[ index++ ] = local_TrianglePop_UnityEngine_GameObject;
            
         }
         {
         }
      }
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_52.Play(logic_uScriptAct_PlayAudioSource_target_52, logic_uScriptAct_PlayAudioSource_audioClip_52);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_Stop_52()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_TrianglePop_UnityEngine_GameObject_previous != local_TrianglePop_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_TrianglePop_UnityEngine_GameObject_previous = local_TrianglePop_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptAct_PlayAudioSource_target_52.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_PlayAudioSource_target_52, index + 1);
            }
            logic_uScriptAct_PlayAudioSource_target_52[ index++ ] = local_TrianglePop_UnityEngine_GameObject;
            
         }
         {
         }
      }
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_52.Stop(logic_uScriptAct_PlayAudioSource_target_52, logic_uScriptAct_PlayAudioSource_audioClip_52);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_uScriptStart_54()
   {
      Relay_In_55();
   }
   
   void Relay_uScriptLateStart_54()
   {
   }
   
   void Relay_In_55()
   {
      {
         {
            System.Array properties;
            int index = 0;
            properties = local_60_UnityEngine_GameObjectArray;
            if ( logic_Custom_SetObjectRendererState_go_Target_55.Length != index + properties.Length)
            {
               System.Array.Resize(ref logic_Custom_SetObjectRendererState_go_Target_55, index + properties.Length);
            }
            System.Array.Copy(properties, 0, logic_Custom_SetObjectRendererState_go_Target_55, index, properties.Length);
            index += properties.Length;
            
         }
         {
         }
      }
      logic_Custom_SetObjectRendererState_Custom_SetObjectRendererState_55.In(logic_Custom_SetObjectRendererState_go_Target_55, logic_Custom_SetObjectRendererState_ers_RenderState_55);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_57()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_Number_01_UnityEngine_GameObject_previous != local_Number_01_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_Number_01_UnityEngine_GameObject_previous = local_Number_01_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptAct_SetGameObjectPosition_Target_57.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_SetGameObjectPosition_Target_57, index + 1);
            }
            logic_uScriptAct_SetGameObjectPosition_Target_57[ index++ ] = local_Number_01_UnityEngine_GameObject;
            
         }
         {
            logic_uScriptAct_SetGameObjectPosition_Position_57 = local_WorldPoint_UnityEngine_Vector3;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_57.In(logic_uScriptAct_SetGameObjectPosition_Target_57, logic_uScriptAct_SetGameObjectPosition_Position_57, logic_uScriptAct_SetGameObjectPosition_AsOffset_57, logic_uScriptAct_SetGameObjectPosition_AsLocal_57);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_57.Out;
      
      if ( test_0 == true )
      {
         Relay_In_59();
      }
   }
   
   void Relay_In_59()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_Number_01_UnityEngine_GameObject_previous != local_Number_01_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_Number_01_UnityEngine_GameObject_previous = local_Number_01_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_Custom_SetObjectRendererState_go_Target_59.Length <= index)
            {
               System.Array.Resize(ref logic_Custom_SetObjectRendererState_go_Target_59, index + 1);
            }
            logic_Custom_SetObjectRendererState_go_Target_59[ index++ ] = local_Number_01_UnityEngine_GameObject;
            
         }
         {
         }
      }
      logic_Custom_SetObjectRendererState_Custom_SetObjectRendererState_59.In(logic_Custom_SetObjectRendererState_go_Target_59, logic_Custom_SetObjectRendererState_ers_RenderState_59);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_Custom_SetObjectRendererState_Custom_SetObjectRendererState_59.Out;
      
      if ( test_0 == true )
      {
         Relay_In_61();
      }
   }
   
   void Relay_In_61()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_Number_01_UnityEngine_GameObject_previous != local_Number_01_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_Number_01_UnityEngine_GameObject_previous = local_Number_01_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptAct_AssignMaterialColor_Target_61.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_AssignMaterialColor_Target_61, index + 1);
            }
            logic_uScriptAct_AssignMaterialColor_Target_61[ index++ ] = local_Number_01_UnityEngine_GameObject;
            
         }
         {
            logic_uScriptAct_AssignMaterialColor_MatColor_61 = local_RandomColor_UnityEngine_Color;
            
         }
         {
         }
      }
      logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_61.In(logic_uScriptAct_AssignMaterialColor_Target_61, logic_uScriptAct_AssignMaterialColor_MatColor_61, logic_uScriptAct_AssignMaterialColor_MatChannel_61);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_61.Out;
      
      if ( test_0 == true )
      {
         Relay_Play_73();
      }
   }
   
   void Relay_In_63()
   {
      {
         {
         }
         {
         }
      }
      logic_uScriptAct_Delay_uScriptAct_Delay_63.In(logic_uScriptAct_Delay_Duration_63, logic_uScriptAct_Delay_SingleFrame_63);
      logic_uScriptAct_Delay_DrivenDelay_63 = true;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Delay_uScriptAct_Delay_63.AfterDelay;
      
      if ( test_0 == true )
      {
         Relay_In_64();
      }
   }
   
   void Relay_Stop_63()
   {
      {
         {
         }
         {
         }
      }
      logic_uScriptAct_Delay_uScriptAct_Delay_63.Stop(logic_uScriptAct_Delay_Duration_63, logic_uScriptAct_Delay_SingleFrame_63);
      logic_uScriptAct_Delay_DrivenDelay_63 = true;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Delay_uScriptAct_Delay_63.AfterDelay;
      
      if ( test_0 == true )
      {
         Relay_In_64();
      }
   }
   
   void Relay_DrivenDelay_63( )
   {
      {
         {
         }
         {
         }
      }
      logic_uScriptAct_Delay_DrivenDelay_63 = logic_uScriptAct_Delay_uScriptAct_Delay_63.DrivenDelay();
      if ( true == logic_uScriptAct_Delay_DrivenDelay_63 )
      {
         if ( logic_uScriptAct_Delay_uScriptAct_Delay_63.AfterDelay == true )
         {
            Relay_In_64();
         }
      }
   }
   void Relay_In_64()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_Number_01_UnityEngine_GameObject_previous != local_Number_01_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_Number_01_UnityEngine_GameObject_previous = local_Number_01_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_Custom_SetObjectRendererState_go_Target_64.Length <= index)
            {
               System.Array.Resize(ref logic_Custom_SetObjectRendererState_go_Target_64, index + 1);
            }
            logic_Custom_SetObjectRendererState_go_Target_64[ index++ ] = local_Number_01_UnityEngine_GameObject;
            
         }
         {
         }
      }
      logic_Custom_SetObjectRendererState_Custom_SetObjectRendererState_64.In(logic_Custom_SetObjectRendererState_go_Target_64, logic_Custom_SetObjectRendererState_ers_RenderState_64);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_65()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_Number_02_UnityEngine_GameObject_previous != local_Number_02_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_Number_02_UnityEngine_GameObject_previous = local_Number_02_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_Custom_SetObjectRendererState_go_Target_65.Length <= index)
            {
               System.Array.Resize(ref logic_Custom_SetObjectRendererState_go_Target_65, index + 1);
            }
            logic_Custom_SetObjectRendererState_go_Target_65[ index++ ] = local_Number_02_UnityEngine_GameObject;
            
         }
         {
         }
      }
      logic_Custom_SetObjectRendererState_Custom_SetObjectRendererState_65.In(logic_Custom_SetObjectRendererState_go_Target_65, logic_Custom_SetObjectRendererState_ers_RenderState_65);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_67()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_Number_02_UnityEngine_GameObject_previous != local_Number_02_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_Number_02_UnityEngine_GameObject_previous = local_Number_02_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptAct_AssignMaterialColor_Target_67.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_AssignMaterialColor_Target_67, index + 1);
            }
            logic_uScriptAct_AssignMaterialColor_Target_67[ index++ ] = local_Number_02_UnityEngine_GameObject;
            
         }
         {
            logic_uScriptAct_AssignMaterialColor_MatColor_67 = local_RandomColor_UnityEngine_Color;
            
         }
         {
         }
      }
      logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_67.In(logic_uScriptAct_AssignMaterialColor_Target_67, logic_uScriptAct_AssignMaterialColor_MatColor_67, logic_uScriptAct_AssignMaterialColor_MatChannel_67);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_67.Out;
      
      if ( test_0 == true )
      {
         Relay_Play_74();
      }
   }
   
   void Relay_In_68()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_Number_02_UnityEngine_GameObject_previous != local_Number_02_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_Number_02_UnityEngine_GameObject_previous = local_Number_02_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptAct_SetGameObjectPosition_Target_68.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_SetGameObjectPosition_Target_68, index + 1);
            }
            logic_uScriptAct_SetGameObjectPosition_Target_68[ index++ ] = local_Number_02_UnityEngine_GameObject;
            
         }
         {
            logic_uScriptAct_SetGameObjectPosition_Position_68 = local_WorldPoint_UnityEngine_Vector3;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_68.In(logic_uScriptAct_SetGameObjectPosition_Target_68, logic_uScriptAct_SetGameObjectPosition_Position_68, logic_uScriptAct_SetGameObjectPosition_AsOffset_68, logic_uScriptAct_SetGameObjectPosition_AsLocal_68);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_68.Out;
      
      if ( test_0 == true )
      {
         Relay_In_70();
      }
   }
   
   void Relay_In_70()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_Number_02_UnityEngine_GameObject_previous != local_Number_02_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_Number_02_UnityEngine_GameObject_previous = local_Number_02_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_Custom_SetObjectRendererState_go_Target_70.Length <= index)
            {
               System.Array.Resize(ref logic_Custom_SetObjectRendererState_go_Target_70, index + 1);
            }
            logic_Custom_SetObjectRendererState_go_Target_70[ index++ ] = local_Number_02_UnityEngine_GameObject;
            
         }
         {
         }
      }
      logic_Custom_SetObjectRendererState_Custom_SetObjectRendererState_70.In(logic_Custom_SetObjectRendererState_go_Target_70, logic_Custom_SetObjectRendererState_ers_RenderState_70);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_Custom_SetObjectRendererState_Custom_SetObjectRendererState_70.Out;
      
      if ( test_0 == true )
      {
         Relay_In_67();
      }
   }
   
   void Relay_In_71()
   {
      {
         {
         }
         {
         }
      }
      logic_uScriptAct_Delay_uScriptAct_Delay_71.In(logic_uScriptAct_Delay_Duration_71, logic_uScriptAct_Delay_SingleFrame_71);
      logic_uScriptAct_Delay_DrivenDelay_71 = true;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Delay_uScriptAct_Delay_71.AfterDelay;
      
      if ( test_0 == true )
      {
         Relay_In_65();
      }
   }
   
   void Relay_Stop_71()
   {
      {
         {
         }
         {
         }
      }
      logic_uScriptAct_Delay_uScriptAct_Delay_71.Stop(logic_uScriptAct_Delay_Duration_71, logic_uScriptAct_Delay_SingleFrame_71);
      logic_uScriptAct_Delay_DrivenDelay_71 = true;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Delay_uScriptAct_Delay_71.AfterDelay;
      
      if ( test_0 == true )
      {
         Relay_In_65();
      }
   }
   
   void Relay_DrivenDelay_71( )
   {
      {
         {
         }
         {
         }
      }
      logic_uScriptAct_Delay_DrivenDelay_71 = logic_uScriptAct_Delay_uScriptAct_Delay_71.DrivenDelay();
      if ( true == logic_uScriptAct_Delay_DrivenDelay_71 )
      {
         if ( logic_uScriptAct_Delay_uScriptAct_Delay_71.AfterDelay == true )
         {
            Relay_In_65();
         }
      }
   }
   void Relay_Finished_73()
   {
   }
   
   void Relay_Play_73()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_Number_01_UnityEngine_GameObject_previous != local_Number_01_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_Number_01_UnityEngine_GameObject_previous = local_Number_01_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptAct_PlayAudioSource_target_73.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_PlayAudioSource_target_73, index + 1);
            }
            logic_uScriptAct_PlayAudioSource_target_73[ index++ ] = local_Number_01_UnityEngine_GameObject;
            
         }
         {
         }
      }
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_73.Play(logic_uScriptAct_PlayAudioSource_target_73, logic_uScriptAct_PlayAudioSource_audioClip_73);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_73.Out;
      
      if ( test_0 == true )
      {
         Relay_In_63();
      }
   }
   
   void Relay_Stop_73()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_Number_01_UnityEngine_GameObject_previous != local_Number_01_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_Number_01_UnityEngine_GameObject_previous = local_Number_01_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptAct_PlayAudioSource_target_73.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_PlayAudioSource_target_73, index + 1);
            }
            logic_uScriptAct_PlayAudioSource_target_73[ index++ ] = local_Number_01_UnityEngine_GameObject;
            
         }
         {
         }
      }
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_73.Stop(logic_uScriptAct_PlayAudioSource_target_73, logic_uScriptAct_PlayAudioSource_audioClip_73);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_73.Out;
      
      if ( test_0 == true )
      {
         Relay_In_63();
      }
   }
   
   void Relay_Finished_74()
   {
   }
   
   void Relay_Play_74()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_Number_02_UnityEngine_GameObject_previous != local_Number_02_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_Number_02_UnityEngine_GameObject_previous = local_Number_02_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptAct_PlayAudioSource_target_74.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_PlayAudioSource_target_74, index + 1);
            }
            logic_uScriptAct_PlayAudioSource_target_74[ index++ ] = local_Number_02_UnityEngine_GameObject;
            
         }
         {
         }
      }
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_74.Play(logic_uScriptAct_PlayAudioSource_target_74, logic_uScriptAct_PlayAudioSource_audioClip_74);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_74.Out;
      
      if ( test_0 == true )
      {
         Relay_In_71();
      }
   }
   
   void Relay_Stop_74()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_Number_02_UnityEngine_GameObject_previous != local_Number_02_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_Number_02_UnityEngine_GameObject_previous = local_Number_02_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptAct_PlayAudioSource_target_74.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_PlayAudioSource_target_74, index + 1);
            }
            logic_uScriptAct_PlayAudioSource_target_74[ index++ ] = local_Number_02_UnityEngine_GameObject;
            
         }
         {
         }
      }
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_74.Stop(logic_uScriptAct_PlayAudioSource_target_74, logic_uScriptAct_PlayAudioSource_audioClip_74);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_74.Out;
      
      if ( test_0 == true )
      {
         Relay_In_71();
      }
   }
   
   void Relay_OnSwipeStart_83()
   {
      local_WorldPoint_UnityEngine_Vector3 = event_UnityEngine_GameObject_v3_TouchToWorldPoint_83;
      Relay_Play_89();
   }
   
   void Relay_OnSwipe_83()
   {
      local_WorldPoint_UnityEngine_Vector3 = event_UnityEngine_GameObject_v3_TouchToWorldPoint_83;
      Relay_In_85();
   }
   
   void Relay_OnSwipeEnd_83()
   {
      local_WorldPoint_UnityEngine_Vector3 = event_UnityEngine_GameObject_v3_TouchToWorldPoint_83;
      Relay_Stop_90();
   }
   
   void Relay_In_85()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_86_UnityEngine_GameObject_previous != local_86_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_86_UnityEngine_GameObject_previous = local_86_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptAct_SetGameObjectPosition_Target_85.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_SetGameObjectPosition_Target_85, index + 1);
            }
            logic_uScriptAct_SetGameObjectPosition_Target_85[ index++ ] = local_86_UnityEngine_GameObject;
            
         }
         {
            logic_uScriptAct_SetGameObjectPosition_Position_85 = local_WorldPoint_UnityEngine_Vector3;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_85.In(logic_uScriptAct_SetGameObjectPosition_Target_85, logic_uScriptAct_SetGameObjectPosition_Position_85, logic_uScriptAct_SetGameObjectPosition_AsOffset_85, logic_uScriptAct_SetGameObjectPosition_AsLocal_85);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_Play_89()
   {
      {
      }
      {
         UnityEngine.ParticleSystem component;
         component = local_88_UnityEngine_GameObject.GetComponent<UnityEngine.ParticleSystem>();
         if ( null != component )
         {
            component.Play();
         }
      }
      Relay_Play_142();
   }
   
   void Relay_Stop_90()
   {
      {
      }
      {
         UnityEngine.ParticleSystem component;
         component = local_91_UnityEngine_GameObject.GetComponent<UnityEngine.ParticleSystem>();
         if ( null != component )
         {
            component.Stop();
         }
      }
      Relay_Stop_142();
   }
   
   void Relay_In_93()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_Number_03_UnityEngine_GameObject_previous != local_Number_03_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_Number_03_UnityEngine_GameObject_previous = local_Number_03_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptAct_SetGameObjectPosition_Target_93.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_SetGameObjectPosition_Target_93, index + 1);
            }
            logic_uScriptAct_SetGameObjectPosition_Target_93[ index++ ] = local_Number_03_UnityEngine_GameObject;
            
         }
         {
            logic_uScriptAct_SetGameObjectPosition_Position_93 = local_WorldPoint_UnityEngine_Vector3;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_93.In(logic_uScriptAct_SetGameObjectPosition_Target_93, logic_uScriptAct_SetGameObjectPosition_Position_93, logic_uScriptAct_SetGameObjectPosition_AsOffset_93, logic_uScriptAct_SetGameObjectPosition_AsLocal_93);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_93.Out;
      
      if ( test_0 == true )
      {
         Relay_In_96();
      }
   }
   
   void Relay_In_95()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_Number_03_UnityEngine_GameObject_previous != local_Number_03_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_Number_03_UnityEngine_GameObject_previous = local_Number_03_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptAct_AssignMaterialColor_Target_95.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_AssignMaterialColor_Target_95, index + 1);
            }
            logic_uScriptAct_AssignMaterialColor_Target_95[ index++ ] = local_Number_03_UnityEngine_GameObject;
            
         }
         {
            logic_uScriptAct_AssignMaterialColor_MatColor_95 = local_RandomColor_UnityEngine_Color;
            
         }
         {
         }
      }
      logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_95.In(logic_uScriptAct_AssignMaterialColor_Target_95, logic_uScriptAct_AssignMaterialColor_MatColor_95, logic_uScriptAct_AssignMaterialColor_MatChannel_95);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_95.Out;
      
      if ( test_0 == true )
      {
         Relay_Play_98();
      }
   }
   
   void Relay_In_96()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_Number_03_UnityEngine_GameObject_previous != local_Number_03_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_Number_03_UnityEngine_GameObject_previous = local_Number_03_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_Custom_SetObjectRendererState_go_Target_96.Length <= index)
            {
               System.Array.Resize(ref logic_Custom_SetObjectRendererState_go_Target_96, index + 1);
            }
            logic_Custom_SetObjectRendererState_go_Target_96[ index++ ] = local_Number_03_UnityEngine_GameObject;
            
         }
         {
         }
      }
      logic_Custom_SetObjectRendererState_Custom_SetObjectRendererState_96.In(logic_Custom_SetObjectRendererState_go_Target_96, logic_Custom_SetObjectRendererState_ers_RenderState_96);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_Custom_SetObjectRendererState_Custom_SetObjectRendererState_96.Out;
      
      if ( test_0 == true )
      {
         Relay_In_95();
      }
   }
   
   void Relay_Finished_98()
   {
   }
   
   void Relay_Play_98()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_Number_03_UnityEngine_GameObject_previous != local_Number_03_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_Number_03_UnityEngine_GameObject_previous = local_Number_03_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptAct_PlayAudioSource_target_98.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_PlayAudioSource_target_98, index + 1);
            }
            logic_uScriptAct_PlayAudioSource_target_98[ index++ ] = local_Number_03_UnityEngine_GameObject;
            
         }
         {
         }
      }
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_98.Play(logic_uScriptAct_PlayAudioSource_target_98, logic_uScriptAct_PlayAudioSource_audioClip_98);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_98.Out;
      
      if ( test_0 == true )
      {
         Relay_In_99();
      }
   }
   
   void Relay_Stop_98()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_Number_03_UnityEngine_GameObject_previous != local_Number_03_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_Number_03_UnityEngine_GameObject_previous = local_Number_03_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptAct_PlayAudioSource_target_98.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_PlayAudioSource_target_98, index + 1);
            }
            logic_uScriptAct_PlayAudioSource_target_98[ index++ ] = local_Number_03_UnityEngine_GameObject;
            
         }
         {
         }
      }
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_98.Stop(logic_uScriptAct_PlayAudioSource_target_98, logic_uScriptAct_PlayAudioSource_audioClip_98);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_98.Out;
      
      if ( test_0 == true )
      {
         Relay_In_99();
      }
   }
   
   void Relay_In_99()
   {
      {
         {
         }
         {
         }
      }
      logic_uScriptAct_Delay_uScriptAct_Delay_99.In(logic_uScriptAct_Delay_Duration_99, logic_uScriptAct_Delay_SingleFrame_99);
      logic_uScriptAct_Delay_DrivenDelay_99 = true;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Delay_uScriptAct_Delay_99.AfterDelay;
      
      if ( test_0 == true )
      {
         Relay_In_100();
      }
   }
   
   void Relay_Stop_99()
   {
      {
         {
         }
         {
         }
      }
      logic_uScriptAct_Delay_uScriptAct_Delay_99.Stop(logic_uScriptAct_Delay_Duration_99, logic_uScriptAct_Delay_SingleFrame_99);
      logic_uScriptAct_Delay_DrivenDelay_99 = true;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Delay_uScriptAct_Delay_99.AfterDelay;
      
      if ( test_0 == true )
      {
         Relay_In_100();
      }
   }
   
   void Relay_DrivenDelay_99( )
   {
      {
         {
         }
         {
         }
      }
      logic_uScriptAct_Delay_DrivenDelay_99 = logic_uScriptAct_Delay_uScriptAct_Delay_99.DrivenDelay();
      if ( true == logic_uScriptAct_Delay_DrivenDelay_99 )
      {
         if ( logic_uScriptAct_Delay_uScriptAct_Delay_99.AfterDelay == true )
         {
            Relay_In_100();
         }
      }
   }
   void Relay_In_100()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_Number_03_UnityEngine_GameObject_previous != local_Number_03_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_Number_03_UnityEngine_GameObject_previous = local_Number_03_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_Custom_SetObjectRendererState_go_Target_100.Length <= index)
            {
               System.Array.Resize(ref logic_Custom_SetObjectRendererState_go_Target_100, index + 1);
            }
            logic_Custom_SetObjectRendererState_go_Target_100[ index++ ] = local_Number_03_UnityEngine_GameObject;
            
         }
         {
         }
      }
      logic_Custom_SetObjectRendererState_Custom_SetObjectRendererState_100.In(logic_Custom_SetObjectRendererState_go_Target_100, logic_Custom_SetObjectRendererState_ers_RenderState_100);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_104()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_Number_04_UnityEngine_GameObject_previous != local_Number_04_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_Number_04_UnityEngine_GameObject_previous = local_Number_04_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_Custom_SetObjectRendererState_go_Target_104.Length <= index)
            {
               System.Array.Resize(ref logic_Custom_SetObjectRendererState_go_Target_104, index + 1);
            }
            logic_Custom_SetObjectRendererState_go_Target_104[ index++ ] = local_Number_04_UnityEngine_GameObject;
            
         }
         {
         }
      }
      logic_Custom_SetObjectRendererState_Custom_SetObjectRendererState_104.In(logic_Custom_SetObjectRendererState_go_Target_104, logic_Custom_SetObjectRendererState_ers_RenderState_104);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_105()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_Number_04_UnityEngine_GameObject_previous != local_Number_04_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_Number_04_UnityEngine_GameObject_previous = local_Number_04_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptAct_SetGameObjectPosition_Target_105.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_SetGameObjectPosition_Target_105, index + 1);
            }
            logic_uScriptAct_SetGameObjectPosition_Target_105[ index++ ] = local_Number_04_UnityEngine_GameObject;
            
         }
         {
            logic_uScriptAct_SetGameObjectPosition_Position_105 = local_WorldPoint_UnityEngine_Vector3;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_105.In(logic_uScriptAct_SetGameObjectPosition_Target_105, logic_uScriptAct_SetGameObjectPosition_Position_105, logic_uScriptAct_SetGameObjectPosition_AsOffset_105, logic_uScriptAct_SetGameObjectPosition_AsLocal_105);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_105.Out;
      
      if ( test_0 == true )
      {
         Relay_In_109();
      }
   }
   
   void Relay_In_107()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_Number_04_UnityEngine_GameObject_previous != local_Number_04_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_Number_04_UnityEngine_GameObject_previous = local_Number_04_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptAct_AssignMaterialColor_Target_107.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_AssignMaterialColor_Target_107, index + 1);
            }
            logic_uScriptAct_AssignMaterialColor_Target_107[ index++ ] = local_Number_04_UnityEngine_GameObject;
            
         }
         {
            logic_uScriptAct_AssignMaterialColor_MatColor_107 = local_RandomColor_UnityEngine_Color;
            
         }
         {
         }
      }
      logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_107.In(logic_uScriptAct_AssignMaterialColor_Target_107, logic_uScriptAct_AssignMaterialColor_MatColor_107, logic_uScriptAct_AssignMaterialColor_MatChannel_107);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_107.Out;
      
      if ( test_0 == true )
      {
         Relay_Play_110();
      }
   }
   
   void Relay_In_109()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_Number_04_UnityEngine_GameObject_previous != local_Number_04_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_Number_04_UnityEngine_GameObject_previous = local_Number_04_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_Custom_SetObjectRendererState_go_Target_109.Length <= index)
            {
               System.Array.Resize(ref logic_Custom_SetObjectRendererState_go_Target_109, index + 1);
            }
            logic_Custom_SetObjectRendererState_go_Target_109[ index++ ] = local_Number_04_UnityEngine_GameObject;
            
         }
         {
         }
      }
      logic_Custom_SetObjectRendererState_Custom_SetObjectRendererState_109.In(logic_Custom_SetObjectRendererState_go_Target_109, logic_Custom_SetObjectRendererState_ers_RenderState_109);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_Custom_SetObjectRendererState_Custom_SetObjectRendererState_109.Out;
      
      if ( test_0 == true )
      {
         Relay_In_107();
      }
   }
   
   void Relay_Finished_110()
   {
   }
   
   void Relay_Play_110()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_Number_04_UnityEngine_GameObject_previous != local_Number_04_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_Number_04_UnityEngine_GameObject_previous = local_Number_04_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptAct_PlayAudioSource_target_110.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_PlayAudioSource_target_110, index + 1);
            }
            logic_uScriptAct_PlayAudioSource_target_110[ index++ ] = local_Number_04_UnityEngine_GameObject;
            
         }
         {
         }
      }
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_110.Play(logic_uScriptAct_PlayAudioSource_target_110, logic_uScriptAct_PlayAudioSource_audioClip_110);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_110.Out;
      
      if ( test_0 == true )
      {
         Relay_In_112();
      }
   }
   
   void Relay_Stop_110()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_Number_04_UnityEngine_GameObject_previous != local_Number_04_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_Number_04_UnityEngine_GameObject_previous = local_Number_04_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptAct_PlayAudioSource_target_110.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_PlayAudioSource_target_110, index + 1);
            }
            logic_uScriptAct_PlayAudioSource_target_110[ index++ ] = local_Number_04_UnityEngine_GameObject;
            
         }
         {
         }
      }
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_110.Stop(logic_uScriptAct_PlayAudioSource_target_110, logic_uScriptAct_PlayAudioSource_audioClip_110);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_110.Out;
      
      if ( test_0 == true )
      {
         Relay_In_112();
      }
   }
   
   void Relay_In_112()
   {
      {
         {
         }
         {
         }
      }
      logic_uScriptAct_Delay_uScriptAct_Delay_112.In(logic_uScriptAct_Delay_Duration_112, logic_uScriptAct_Delay_SingleFrame_112);
      logic_uScriptAct_Delay_DrivenDelay_112 = true;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Delay_uScriptAct_Delay_112.AfterDelay;
      
      if ( test_0 == true )
      {
         Relay_In_104();
      }
   }
   
   void Relay_Stop_112()
   {
      {
         {
         }
         {
         }
      }
      logic_uScriptAct_Delay_uScriptAct_Delay_112.Stop(logic_uScriptAct_Delay_Duration_112, logic_uScriptAct_Delay_SingleFrame_112);
      logic_uScriptAct_Delay_DrivenDelay_112 = true;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Delay_uScriptAct_Delay_112.AfterDelay;
      
      if ( test_0 == true )
      {
         Relay_In_104();
      }
   }
   
   void Relay_DrivenDelay_112( )
   {
      {
         {
         }
         {
         }
      }
      logic_uScriptAct_Delay_DrivenDelay_112 = logic_uScriptAct_Delay_uScriptAct_Delay_112.DrivenDelay();
      if ( true == logic_uScriptAct_Delay_DrivenDelay_112 )
      {
         if ( logic_uScriptAct_Delay_uScriptAct_Delay_112.AfterDelay == true )
         {
            Relay_In_104();
         }
      }
   }
   void Relay_uScriptStart_118()
   {
      Relay_Random_122();
      Relay_True_144();
   }
   
   void Relay_uScriptLateStart_118()
   {
   }
   
   void Relay_In_119()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_120_UnityEngine_GameObject_previous != local_120_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_120_UnityEngine_GameObject_previous = local_120_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_Custom_GradientColorChange_go_Target_119 = local_120_UnityEngine_GameObject;
            
         }
         {
         }
         {
         }
         {
            logic_Custom_GradientColorChange_c_Bottom_119 = local_BottomColor_UnityEngine_Color;
            
         }
         {
         }
      }
      logic_Custom_GradientColorChange_Custom_GradientColorChange_119.In(logic_Custom_GradientColorChange_go_Target_119, logic_Custom_GradientColorChange_ecc_ColorChannel_119, logic_Custom_GradientColorChange_c_Top_119, logic_Custom_GradientColorChange_c_Bottom_119, logic_Custom_GradientColorChange_f_Time_119);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_Custom_GradientColorChange_Custom_GradientColorChange_119.Out;
      
      if ( test_0 == true )
      {
         Relay_In_124();
      }
   }
   
   void Relay_First_122()
   {
      {
         {
            System.Array properties;
            int index = 0;
            properties = local_121_UnityEngine_ColorArray;
            if ( logic_uScriptAct_AccessListColor_List_122.Length != index + properties.Length)
            {
               System.Array.Resize(ref logic_uScriptAct_AccessListColor_List_122, index + properties.Length);
            }
            System.Array.Copy(properties, 0, logic_uScriptAct_AccessListColor_List_122, index, properties.Length);
            index += properties.Length;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_AccessListColor_uScriptAct_AccessListColor_122.First(logic_uScriptAct_AccessListColor_List_122, logic_uScriptAct_AccessListColor_Index_122, out logic_uScriptAct_AccessListColor_Value_122);
      local_BottomColor_UnityEngine_Color = logic_uScriptAct_AccessListColor_Value_122;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_AccessListColor_uScriptAct_AccessListColor_122.Out;
      
      if ( test_0 == true )
      {
         Relay_In_131();
      }
   }
   
   void Relay_Last_122()
   {
      {
         {
            System.Array properties;
            int index = 0;
            properties = local_121_UnityEngine_ColorArray;
            if ( logic_uScriptAct_AccessListColor_List_122.Length != index + properties.Length)
            {
               System.Array.Resize(ref logic_uScriptAct_AccessListColor_List_122, index + properties.Length);
            }
            System.Array.Copy(properties, 0, logic_uScriptAct_AccessListColor_List_122, index, properties.Length);
            index += properties.Length;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_AccessListColor_uScriptAct_AccessListColor_122.Last(logic_uScriptAct_AccessListColor_List_122, logic_uScriptAct_AccessListColor_Index_122, out logic_uScriptAct_AccessListColor_Value_122);
      local_BottomColor_UnityEngine_Color = logic_uScriptAct_AccessListColor_Value_122;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_AccessListColor_uScriptAct_AccessListColor_122.Out;
      
      if ( test_0 == true )
      {
         Relay_In_131();
      }
   }
   
   void Relay_Random_122()
   {
      {
         {
            System.Array properties;
            int index = 0;
            properties = local_121_UnityEngine_ColorArray;
            if ( logic_uScriptAct_AccessListColor_List_122.Length != index + properties.Length)
            {
               System.Array.Resize(ref logic_uScriptAct_AccessListColor_List_122, index + properties.Length);
            }
            System.Array.Copy(properties, 0, logic_uScriptAct_AccessListColor_List_122, index, properties.Length);
            index += properties.Length;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_AccessListColor_uScriptAct_AccessListColor_122.Random(logic_uScriptAct_AccessListColor_List_122, logic_uScriptAct_AccessListColor_Index_122, out logic_uScriptAct_AccessListColor_Value_122);
      local_BottomColor_UnityEngine_Color = logic_uScriptAct_AccessListColor_Value_122;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_AccessListColor_uScriptAct_AccessListColor_122.Out;
      
      if ( test_0 == true )
      {
         Relay_In_131();
      }
   }
   
   void Relay_AtIndex_122()
   {
      {
         {
            System.Array properties;
            int index = 0;
            properties = local_121_UnityEngine_ColorArray;
            if ( logic_uScriptAct_AccessListColor_List_122.Length != index + properties.Length)
            {
               System.Array.Resize(ref logic_uScriptAct_AccessListColor_List_122, index + properties.Length);
            }
            System.Array.Copy(properties, 0, logic_uScriptAct_AccessListColor_List_122, index, properties.Length);
            index += properties.Length;
            
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_AccessListColor_uScriptAct_AccessListColor_122.AtIndex(logic_uScriptAct_AccessListColor_List_122, logic_uScriptAct_AccessListColor_Index_122, out logic_uScriptAct_AccessListColor_Value_122);
      local_BottomColor_UnityEngine_Color = logic_uScriptAct_AccessListColor_Value_122;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_AccessListColor_uScriptAct_AccessListColor_122.Out;
      
      if ( test_0 == true )
      {
         Relay_In_131();
      }
   }
   
   void Relay_In_124()
   {
      {
         {
         }
         {
         }
      }
      logic_uScriptAct_Delay_uScriptAct_Delay_124.In(logic_uScriptAct_Delay_Duration_124, logic_uScriptAct_Delay_SingleFrame_124);
      logic_uScriptAct_Delay_DrivenDelay_124 = true;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Delay_uScriptAct_Delay_124.AfterDelay;
      
      if ( test_0 == true )
      {
         Relay_Random_122();
      }
   }
   
   void Relay_Stop_124()
   {
      {
         {
         }
         {
         }
      }
      logic_uScriptAct_Delay_uScriptAct_Delay_124.Stop(logic_uScriptAct_Delay_Duration_124, logic_uScriptAct_Delay_SingleFrame_124);
      logic_uScriptAct_Delay_DrivenDelay_124 = true;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Delay_uScriptAct_Delay_124.AfterDelay;
      
      if ( test_0 == true )
      {
         Relay_Random_122();
      }
   }
   
   void Relay_DrivenDelay_124( )
   {
      {
         {
         }
         {
         }
      }
      logic_uScriptAct_Delay_DrivenDelay_124 = logic_uScriptAct_Delay_uScriptAct_Delay_124.DrivenDelay();
      if ( true == logic_uScriptAct_Delay_DrivenDelay_124 )
      {
         if ( logic_uScriptAct_Delay_uScriptAct_Delay_124.AfterDelay == true )
         {
            Relay_Random_122();
         }
      }
   }
   void Relay_In_126()
   {
      {
         {
            logic_uScriptAct_SetColor_Value_126 = local_BottomColor_UnityEngine_Color;
            
         }
         {
         }
      }
      logic_uScriptAct_SetColor_uScriptAct_SetColor_126.In(logic_uScriptAct_SetColor_Value_126, out logic_uScriptAct_SetColor_TargetColor_126);
      local_CurrentColor_UnityEngine_Color = logic_uScriptAct_SetColor_TargetColor_126;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_SetColor_uScriptAct_SetColor_126.Out;
      
      if ( test_0 == true )
      {
         Relay_In_119();
      }
   }
   
   void Relay_In_131()
   {
      {
         {
            logic_Custom_CompareColor_c_A_131 = local_BottomColor_UnityEngine_Color;
            
         }
         {
            logic_Custom_CompareColor_c_B_131 = local_CurrentColor_UnityEngine_Color;
            
         }
      }
      logic_Custom_CompareColor_Custom_CompareColor_131.In(logic_Custom_CompareColor_c_A_131, logic_Custom_CompareColor_c_B_131);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_Custom_CompareColor_Custom_CompareColor_131.Same;
      bool test_1 = logic_Custom_CompareColor_Custom_CompareColor_131.Different;
      
      if ( test_0 == true )
      {
         Relay_Random_122();
      }
      if ( test_1 == true )
      {
         Relay_In_126();
      }
   }
   
   void Relay_Case1_132()
   {
      Relay_In_2();
   }
   
   void Relay_Case2_132()
   {
      Relay_In_13();
   }
   
   void Relay_Case3_132()
   {
      Relay_In_21();
   }
   
   void Relay_Case4_132()
   {
      Relay_In_29();
   }
   
   void Relay_Case5_132()
   {
      Relay_In_38();
   }
   
   void Relay_Case6_132()
   {
      Relay_In_57();
   }
   
   void Relay_Case7_132()
   {
      Relay_In_68();
   }
   
   void Relay_Case8_132()
   {
      Relay_In_93();
   }
   
   void Relay_Case9_132()
   {
      Relay_In_105();
   }
   
   void Relay_Case10_132()
   {
      Relay_False_150();
   }
   
   void Relay_Case11_132()
   {
      Relay_False_339();
   }
   
   void Relay_In_132()
   {
      {
         {
            logic_Custom_ManualSwitch_CurrentOutput_132 = Index;
            
         }
      }
      logic_Custom_ManualSwitch_Custom_ManualSwitch_132.In(logic_Custom_ManualSwitch_CurrentOutput_132);
      
   }
   
   void Relay_In_133()
   {
      {
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_SetRandomInt_uScriptAct_SetRandomInt_133.In(logic_uScriptAct_SetRandomInt_Min_133, logic_uScriptAct_SetRandomInt_Max_133, out logic_uScriptAct_SetRandomInt_TargetInt_133);
      Index = logic_uScriptAct_SetRandomInt_TargetInt_133;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_SetRandomInt_uScriptAct_SetRandomInt_133.Out;
      
      if ( test_0 == true )
      {
         Relay_In_135();
      }
   }
   
   void Relay_In_135()
   {
      {
         {
            logic_uScriptCon_CompareInt_A_135 = Index;
            
         }
         {
            logic_uScriptCon_CompareInt_B_135 = CurrentIndex;
            
         }
      }
      logic_uScriptCon_CompareInt_uScriptCon_CompareInt_135.In(logic_uScriptCon_CompareInt_A_135, logic_uScriptCon_CompareInt_B_135);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_CompareInt_uScriptCon_CompareInt_135.EqualTo;
      bool test_1 = logic_uScriptCon_CompareInt_uScriptCon_CompareInt_135.NotEqualTo;
      
      if ( test_0 == true )
      {
         Relay_In_133();
      }
      if ( test_1 == true )
      {
         Relay_In_139();
      }
   }
   
   void Relay_In_139()
   {
      {
         {
            logic_uScriptAct_SetInt_Value_139 = Index;
            
         }
         {
         }
      }
      logic_uScriptAct_SetInt_uScriptAct_SetInt_139.In(logic_uScriptAct_SetInt_Value_139, out logic_uScriptAct_SetInt_Target_139);
      CurrentIndex = logic_uScriptAct_SetInt_Target_139;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_SetInt_uScriptAct_SetInt_139.Out;
      
      if ( test_0 == true )
      {
         Relay_In_132();
      }
   }
   
   void Relay_Finished_142()
   {
   }
   
   void Relay_Play_142()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_143_UnityEngine_GameObject_previous != local_143_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_143_UnityEngine_GameObject_previous = local_143_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptAct_PlayAudioSource_target_142.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_PlayAudioSource_target_142, index + 1);
            }
            logic_uScriptAct_PlayAudioSource_target_142[ index++ ] = local_143_UnityEngine_GameObject;
            
         }
         {
         }
      }
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_142.Play(logic_uScriptAct_PlayAudioSource_target_142, logic_uScriptAct_PlayAudioSource_audioClip_142);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_Stop_142()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_143_UnityEngine_GameObject_previous != local_143_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_143_UnityEngine_GameObject_previous = local_143_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptAct_PlayAudioSource_target_142.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_PlayAudioSource_target_142, index + 1);
            }
            logic_uScriptAct_PlayAudioSource_target_142[ index++ ] = local_143_UnityEngine_GameObject;
            
         }
         {
         }
      }
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_142.Stop(logic_uScriptAct_PlayAudioSource_target_142, logic_uScriptAct_PlayAudioSource_audioClip_142);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_True_144()
   {
      {
         {
         }
      }
      logic_uScriptAct_SetBool_uScriptAct_SetBool_144.True(out logic_uScriptAct_SetBool_Target_144);
      EnableTouch = logic_uScriptAct_SetBool_Target_144;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_SetBool_uScriptAct_SetBool_144.Out;
      
      if ( test_0 == true )
      {
         Relay_In_165();
      }
   }
   
   void Relay_False_144()
   {
      {
         {
         }
      }
      logic_uScriptAct_SetBool_uScriptAct_SetBool_144.False(out logic_uScriptAct_SetBool_Target_144);
      EnableTouch = logic_uScriptAct_SetBool_Target_144;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_SetBool_uScriptAct_SetBool_144.Out;
      
      if ( test_0 == true )
      {
         Relay_In_165();
      }
   }
   
   void Relay_In_146()
   {
      {
         {
            logic_uScriptCon_CompareBool_Bool_146 = EnableTouch;
            
         }
      }
      logic_uScriptCon_CompareBool_uScriptCon_CompareBool_146.In(logic_uScriptCon_CompareBool_Bool_146);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptCon_CompareBool_uScriptCon_CompareBool_146.True;
      
      if ( test_0 == true )
      {
         Relay_In_9();
      }
   }
   
   void Relay_In_148()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_149_UnityEngine_GameObject_previous != local_149_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_149_UnityEngine_GameObject_previous = local_149_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_ParticlePlayground_SetEmit_go_ParticleObject_148 = local_149_UnityEngine_GameObject;
            
         }
         {
         }
      }
      logic_ParticlePlayground_SetEmit_ParticlePlayground_SetEmit_148.In(logic_ParticlePlayground_SetEmit_go_ParticleObject_148, logic_ParticlePlayground_SetEmit_b_Emit_148);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_ParticlePlayground_SetEmit_ParticlePlayground_SetEmit_148.Out;
      
      if ( test_0 == true )
      {
         Relay_Play_153();
      }
   }
   
   void Relay_True_150()
   {
      {
         {
         }
      }
      logic_uScriptAct_SetBool_uScriptAct_SetBool_150.True(out logic_uScriptAct_SetBool_Target_150);
      EnableTouch = logic_uScriptAct_SetBool_Target_150;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_SetBool_uScriptAct_SetBool_150.Out;
      
      if ( test_0 == true )
      {
         Relay_In_148();
      }
   }
   
   void Relay_False_150()
   {
      {
         {
         }
      }
      logic_uScriptAct_SetBool_uScriptAct_SetBool_150.False(out logic_uScriptAct_SetBool_Target_150);
      EnableTouch = logic_uScriptAct_SetBool_Target_150;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_SetBool_uScriptAct_SetBool_150.Out;
      
      if ( test_0 == true )
      {
         Relay_In_148();
      }
   }
   
   void Relay_OnComplete_152()
   {
      Relay_In_159();
   }
   
   void Relay_In_152()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_149_UnityEngine_GameObject_previous != local_149_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_149_UnityEngine_GameObject_previous = local_149_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_Custom_LeanTweenMove_go_GameObject_152 = local_149_UnityEngine_GameObject;
            
         }
         {
         }
         {
         }
      }
      logic_Custom_LeanTweenMove_Custom_LeanTweenMove_152.In(logic_Custom_LeanTweenMove_go_GameObject_152, logic_Custom_LeanTweenMove_v3_Location_152, logic_Custom_LeanTweenMove_f_Time_152);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_Finished_153()
   {
   }
   
   void Relay_Play_153()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_149_UnityEngine_GameObject_previous != local_149_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_149_UnityEngine_GameObject_previous = local_149_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptAct_PlayAudioSource_target_153.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_PlayAudioSource_target_153, index + 1);
            }
            logic_uScriptAct_PlayAudioSource_target_153[ index++ ] = local_149_UnityEngine_GameObject;
            
         }
         {
         }
      }
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_153.Play(logic_uScriptAct_PlayAudioSource_target_153, logic_uScriptAct_PlayAudioSource_audioClip_153);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_153.Out;
      
      if ( test_0 == true )
      {
         Relay_In_152();
      }
   }
   
   void Relay_Stop_153()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_149_UnityEngine_GameObject_previous != local_149_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_149_UnityEngine_GameObject_previous = local_149_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptAct_PlayAudioSource_target_153.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_PlayAudioSource_target_153, index + 1);
            }
            logic_uScriptAct_PlayAudioSource_target_153[ index++ ] = local_149_UnityEngine_GameObject;
            
         }
         {
         }
      }
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_153.Stop(logic_uScriptAct_PlayAudioSource_target_153, logic_uScriptAct_PlayAudioSource_audioClip_153);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_153.Out;
      
      if ( test_0 == true )
      {
         Relay_In_152();
      }
   }
   
   void Relay_In_154()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_156_UnityEngine_GameObject_previous != local_156_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_156_UnityEngine_GameObject_previous = local_156_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_ParticlePlayground_SetEmit_go_ParticleObject_154 = local_156_UnityEngine_GameObject;
            
         }
         {
         }
      }
      logic_ParticlePlayground_SetEmit_ParticlePlayground_SetEmit_154.In(logic_ParticlePlayground_SetEmit_go_ParticleObject_154, logic_ParticlePlayground_SetEmit_b_Emit_154);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_ParticlePlayground_SetEmit_ParticlePlayground_SetEmit_154.Out;
      
      if ( test_0 == true )
      {
         Relay_Play_155();
      }
   }
   
   void Relay_Finished_155()
   {
      Relay_In_162();
   }
   
   void Relay_Play_155()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_156_UnityEngine_GameObject_previous != local_156_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_156_UnityEngine_GameObject_previous = local_156_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptAct_PlayAudioSource_target_155.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_PlayAudioSource_target_155, index + 1);
            }
            logic_uScriptAct_PlayAudioSource_target_155[ index++ ] = local_156_UnityEngine_GameObject;
            
         }
         {
         }
      }
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_155.Play(logic_uScriptAct_PlayAudioSource_target_155, logic_uScriptAct_PlayAudioSource_audioClip_155);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_155.Out;
      
      if ( test_0 == true )
      {
         Relay_True_158();
      }
   }
   
   void Relay_Stop_155()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_156_UnityEngine_GameObject_previous != local_156_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_156_UnityEngine_GameObject_previous = local_156_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptAct_PlayAudioSource_target_155.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_PlayAudioSource_target_155, index + 1);
            }
            logic_uScriptAct_PlayAudioSource_target_155[ index++ ] = local_156_UnityEngine_GameObject;
            
         }
         {
         }
      }
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_155.Stop(logic_uScriptAct_PlayAudioSource_target_155, logic_uScriptAct_PlayAudioSource_audioClip_155);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_155.Out;
      
      if ( test_0 == true )
      {
         Relay_True_158();
      }
   }
   
   void Relay_True_158()
   {
      {
         {
         }
      }
      logic_uScriptAct_SetBool_uScriptAct_SetBool_158.True(out logic_uScriptAct_SetBool_Target_158);
      EnableTouch = logic_uScriptAct_SetBool_Target_158;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_False_158()
   {
      {
         {
         }
      }
      logic_uScriptAct_SetBool_uScriptAct_SetBool_158.False(out logic_uScriptAct_SetBool_Target_158);
      EnableTouch = logic_uScriptAct_SetBool_Target_158;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_159()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_160_UnityEngine_GameObject_previous != local_160_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_160_UnityEngine_GameObject_previous = local_160_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_ParticlePlayground_SetEmit_go_ParticleObject_159 = local_160_UnityEngine_GameObject;
            
         }
         {
         }
      }
      logic_ParticlePlayground_SetEmit_ParticlePlayground_SetEmit_159.In(logic_ParticlePlayground_SetEmit_go_ParticleObject_159, logic_ParticlePlayground_SetEmit_b_Emit_159);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_ParticlePlayground_SetEmit_ParticlePlayground_SetEmit_159.Out;
      
      if ( test_0 == true )
      {
         Relay_In_163();
      }
   }
   
   void Relay_In_162()
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_161_UnityEngine_GameObject_previous != local_161_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_161_UnityEngine_GameObject_previous = local_161_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            logic_ParticlePlayground_SetEmit_go_ParticleObject_162 = local_161_UnityEngine_GameObject;
            
         }
         {
         }
      }
      logic_ParticlePlayground_SetEmit_ParticlePlayground_SetEmit_162.In(logic_ParticlePlayground_SetEmit_go_ParticleObject_162, logic_ParticlePlayground_SetEmit_b_Emit_162);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_163()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_160_UnityEngine_GameObject_previous != local_160_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_160_UnityEngine_GameObject_previous = local_160_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptAct_SetGameObjectPosition_Target_163.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_SetGameObjectPosition_Target_163, index + 1);
            }
            logic_uScriptAct_SetGameObjectPosition_Target_163[ index++ ] = local_160_UnityEngine_GameObject;
            
         }
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_163.In(logic_uScriptAct_SetGameObjectPosition_Target_163, logic_uScriptAct_SetGameObjectPosition_Position_163, logic_uScriptAct_SetGameObjectPosition_AsOffset_163, logic_uScriptAct_SetGameObjectPosition_AsLocal_163);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_163.Out;
      
      if ( test_0 == true )
      {
         Relay_In_164();
      }
   }
   
   void Relay_In_164()
   {
      {
         {
         }
         {
         }
      }
      logic_uScriptAct_Delay_uScriptAct_Delay_164.In(logic_uScriptAct_Delay_Duration_164, logic_uScriptAct_Delay_SingleFrame_164);
      logic_uScriptAct_Delay_DrivenDelay_164 = true;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Delay_uScriptAct_Delay_164.AfterDelay;
      
      if ( test_0 == true )
      {
         Relay_In_154();
      }
   }
   
   void Relay_Stop_164()
   {
      {
         {
         }
         {
         }
      }
      logic_uScriptAct_Delay_uScriptAct_Delay_164.Stop(logic_uScriptAct_Delay_Duration_164, logic_uScriptAct_Delay_SingleFrame_164);
      logic_uScriptAct_Delay_DrivenDelay_164 = true;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Delay_uScriptAct_Delay_164.AfterDelay;
      
      if ( test_0 == true )
      {
         Relay_In_154();
      }
   }
   
   void Relay_DrivenDelay_164( )
   {
      {
         {
         }
         {
         }
      }
      logic_uScriptAct_Delay_DrivenDelay_164 = logic_uScriptAct_Delay_uScriptAct_Delay_164.DrivenDelay();
      if ( true == logic_uScriptAct_Delay_DrivenDelay_164 )
      {
         if ( logic_uScriptAct_Delay_uScriptAct_Delay_164.AfterDelay == true )
         {
            Relay_In_154();
         }
      }
   }
   void Relay_In_165()
   {
      {
         {
            System.Array properties;
            int index = 0;
            properties = local_166_UnityEngine_GameObjectArray;
            if ( logic_Custom_SetLineRendererState_go_GameObject_165.Length != index + properties.Length)
            {
               System.Array.Resize(ref logic_Custom_SetLineRendererState_go_GameObject_165, index + properties.Length);
            }
            System.Array.Copy(properties, 0, logic_Custom_SetLineRendererState_go_GameObject_165, index, properties.Length);
            index += properties.Length;
            
         }
         {
         }
      }
      logic_Custom_SetLineRendererState_Custom_SetLineRendererState_165.In(logic_Custom_SetLineRendererState_go_GameObject_165, logic_Custom_SetLineRendererState_b_Enable_165);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_True_339()
   {
      {
         {
         }
      }
      logic_uScriptAct_SetBool_uScriptAct_SetBool_339.True(out logic_uScriptAct_SetBool_Target_339);
      EnableTouch = logic_uScriptAct_SetBool_Target_339;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_SetBool_uScriptAct_SetBool_339.Out;
      
      if ( test_0 == true )
      {
         Relay_In_343();
      }
   }
   
   void Relay_False_339()
   {
      {
         {
         }
      }
      logic_uScriptAct_SetBool_uScriptAct_SetBool_339.False(out logic_uScriptAct_SetBool_Target_339);
      EnableTouch = logic_uScriptAct_SetBool_Target_339;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_SetBool_uScriptAct_SetBool_339.Out;
      
      if ( test_0 == true )
      {
         Relay_In_343();
      }
   }
   
   void Relay_In_343()
   {
      {
         {
            System.Array properties;
            int index = 0;
            properties = local_342_UnityEngine_GameObjectArray;
            if ( logic_Custom_SetLineRendererState_go_GameObject_343.Length != index + properties.Length)
            {
               System.Array.Resize(ref logic_Custom_SetLineRendererState_go_GameObject_343, index + properties.Length);
            }
            System.Array.Copy(properties, 0, logic_Custom_SetLineRendererState_go_GameObject_343, index, properties.Length);
            index += properties.Length;
            
         }
         {
         }
      }
      logic_Custom_SetLineRendererState_Custom_SetLineRendererState_343.In(logic_Custom_SetLineRendererState_go_GameObject_343, logic_Custom_SetLineRendererState_b_Enable_343);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_Custom_SetLineRendererState_Custom_SetLineRendererState_343.Out;
      
      if ( test_0 == true )
      {
         Relay_Play_346();
      }
   }
   
   void Relay_Finished_346()
   {
      Relay_True_350();
   }
   
   void Relay_Play_346()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_348_UnityEngine_GameObject_previous != local_348_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_348_UnityEngine_GameObject_previous = local_348_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptAct_PlayAudioSource_target_346.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_PlayAudioSource_target_346, index + 1);
            }
            logic_uScriptAct_PlayAudioSource_target_346[ index++ ] = local_348_UnityEngine_GameObject;
            
         }
         {
         }
      }
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_346.Play(logic_uScriptAct_PlayAudioSource_target_346, logic_uScriptAct_PlayAudioSource_audioClip_346);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_Stop_346()
   {
      {
         {
            int index = 0;
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_348_UnityEngine_GameObject_previous != local_348_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_348_UnityEngine_GameObject_previous = local_348_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            if ( logic_uScriptAct_PlayAudioSource_target_346.Length <= index)
            {
               System.Array.Resize(ref logic_uScriptAct_PlayAudioSource_target_346, index + 1);
            }
            logic_uScriptAct_PlayAudioSource_target_346[ index++ ] = local_348_UnityEngine_GameObject;
            
         }
         {
         }
      }
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_346.Stop(logic_uScriptAct_PlayAudioSource_target_346, logic_uScriptAct_PlayAudioSource_audioClip_346);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_True_350()
   {
      {
         {
         }
      }
      logic_uScriptAct_SetBool_uScriptAct_SetBool_350.True(out logic_uScriptAct_SetBool_Target_350);
      EnableTouch = logic_uScriptAct_SetBool_Target_350;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_SetBool_uScriptAct_SetBool_350.Out;
      
      if ( test_0 == true )
      {
         Relay_In_352();
      }
   }
   
   void Relay_False_350()
   {
      {
         {
         }
      }
      logic_uScriptAct_SetBool_uScriptAct_SetBool_350.False(out logic_uScriptAct_SetBool_Target_350);
      EnableTouch = logic_uScriptAct_SetBool_Target_350;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_SetBool_uScriptAct_SetBool_350.Out;
      
      if ( test_0 == true )
      {
         Relay_In_352();
      }
   }
   
   void Relay_In_352()
   {
      {
         {
            System.Array properties;
            int index = 0;
            properties = local_353_UnityEngine_GameObjectArray;
            if ( logic_Custom_SetLineRendererState_go_GameObject_352.Length != index + properties.Length)
            {
               System.Array.Resize(ref logic_Custom_SetLineRendererState_go_GameObject_352, index + properties.Length);
            }
            System.Array.Copy(properties, 0, logic_Custom_SetLineRendererState_go_GameObject_352, index, properties.Length);
            index += properties.Length;
            
         }
         {
         }
      }
      logic_Custom_SetLineRendererState_Custom_SetLineRendererState_352.In(logic_Custom_SetLineRendererState_go_GameObject_352, logic_Custom_SetLineRendererState_b_Enable_352);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
}
