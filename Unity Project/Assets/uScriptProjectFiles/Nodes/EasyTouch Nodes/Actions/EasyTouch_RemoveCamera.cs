﻿using UnityEngine;
using System.Collections;

[NodePath("EasyTouch/Actions")]

[NodeCopyright("Copyright 2015 Reclusive Ninja")]
[NodeToolTip("Removes a camera from the Automatic Selection section.")]
[NodeAuthor("Reclusive Ninja", "http://reclusiveninja.com")]

[FriendlyName("EasyTouch Remove Camera", "Removes a camera from the Automatic Selection section.")]
public class EasyTouch_RemoveCamera : uScriptLogic
{
#region uScript Functions ------------------------------------------------------------------------
    public void In(
        [FriendlyName("Camera", "The camera to remove from the Automatic Selection section.")]
        Camera c_Camera
        )
    {
        HedgehogTeam.EasyTouch.EasyTouch.RemoveCamera(c_Camera);
    }
//------------------------------------------------------------------------
    public bool Out
    {
        get { return true; }
    }
#endregion
}