﻿using UnityEngine;
using System.Collections;

[NodePath("Reclusive Ninja/Actions")]

[NodeCopyright("Copyright 2015 Reclusive Ninja")]
[NodeToolTip("Move an object with a character controller component in a specific direction.")]
[NodeAuthor("Reclusive Ninja", "http://reclusiveninja.com")]

[FriendlyName("Reclusive Ninja - Move Character Controller", "Move an object with a character controller component in a specific direction.")]
public class RN_MoveCharacterController : uScriptLogic
{
#region uScript Functions ------------------------------------------------------------------------
    public void In(
        [FriendlyName("Target", "The target object with the character controller component.")]
        GameObject go_GameObject,
        
        [FriendlyName("Return Value", "Returns collision flags.")]
        [SocketState(false, false)]
        out CollisionFlags cf_CollisionFlags,
        
        [FriendlyName("Motion", "The direction to move the target object.")]
        Vector3 v3_Motion)
    {
        go_GameObject.GetComponent<CharacterController>().Move(v3_Motion);

        cf_CollisionFlags = go_GameObject.GetComponent<CharacterController>().collisionFlags;
    }

//------------------------------------------------------------------------
    public bool Out
    {
        get { return true; }
    }
#endregion
}