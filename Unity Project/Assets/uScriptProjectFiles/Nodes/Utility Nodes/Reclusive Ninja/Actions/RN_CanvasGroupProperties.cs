﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[NodePath("Reclusive Ninja/Actions")]

[NodeCopyright("Copyright 2015 Reclusive Ninja")]
[NodeToolTip("Sets the canvas group's properties.")]
[NodeAuthor("Reclusive Ninja", "http://reclusiveninja.com")]

[FriendlyName("Reclusive Ninja - Canvas Group Properties", "Sets the canvas group's properties.")]
public class RN_CanvasGroupProperties : uScriptLogic
{
#region uScript Functions ------------------------------------------------------------------------
    public void In(
        [FriendlyName("Canvas Group", "The target canvas group.")]
        CanvasGroup cg_CanvasGroup,
        
        [FriendlyName("Alpha", "The value to set the canvas group alpha to.")]
        float f_Alpha,
        
        [FriendlyName("Interactable", "Sets the panel to be interactable")]
        bool b_Interactable,
        
        [FriendlyName("Blocks Raycasts", "Sets the panel to block raycasts.")]
        bool b_BlocksRaycasts,
        
        [FriendlyName("Ignore Parent Groups", "Sets the panel to ignore parent groups.")]
        bool b_IgnoreParentGroups)
    {
        cg_CanvasGroup.alpha = f_Alpha;
        cg_CanvasGroup.interactable = b_Interactable;
        cg_CanvasGroup.blocksRaycasts = b_BlocksRaycasts;
        cg_CanvasGroup.ignoreParentGroups = b_IgnoreParentGroups;
    }

//------------------------------------------------------------------------
    public bool Out
    {
        get { return true; }
    }
#endregion
}
