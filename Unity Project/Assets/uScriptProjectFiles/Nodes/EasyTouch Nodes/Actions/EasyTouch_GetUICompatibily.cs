﻿using UnityEngine;
using System.Collections;

[NodePath("EasyTouch/Actions")]

[NodeCopyright("Copyright 2015 Reclusive Ninja")]
[NodeToolTip("Returns if UICompatibily is enabled/disabled.")]
[NodeAuthor("Reclusive Ninja", "http://reclusiveninja.com")]

[FriendlyName("EasyTouch Get UI Compatibily", "Returns if UICompatibily is enabled/disabled.")]
public class EasyTouch_GetUICompatibily : uScriptLogic
{
#region uScript Functions ------------------------------------------------------------------------
    public void In(
        [FriendlyName("Is UI Compatibily Enabled?", "Returns if UICompatibily is enabled/disabled.")]
        out bool b_UICompatibilyEnabled)
    {
        b_UICompatibilyEnabled = HedgehogTeam.EasyTouch.EasyTouch.GetUIComptability();
    }

//------------------------------------------------------------------------
    public bool Out
    {
        get { return true; }
    }
#endregion
}