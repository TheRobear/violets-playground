﻿using UnityEngine;
using System.Collections;

[NodePath("Reclusive Ninja/Actions")]

[NodeCopyright("Copyright 2015 Reclusive Ninja")]
[NodeToolTip("Sets the field of view of a camera.")]
[NodeAuthor("Reclusive Ninja", "http://reclusiveninja.com")]

[FriendlyName("Reclusive Ninja - Set Camera FOV", "Sets the field of view of a camera.")]
public class RN_SetCameraFOV : uScriptLogic
{
#region uScript Functions ------------------------------------------------------------------------
    public void In(
        [FriendlyName("Camera", "The target camera.")]
        Camera c_Camera,
        
        [FriendlyName("Field of View", "The value to set the target camera's field of view.")]
        float f_FieldOfView)
    {
        c_Camera.fieldOfView = f_FieldOfView;
    }

//------------------------------------------------------------------------
    public bool Out
    {
        get { return true; }
    }
#endregion
}