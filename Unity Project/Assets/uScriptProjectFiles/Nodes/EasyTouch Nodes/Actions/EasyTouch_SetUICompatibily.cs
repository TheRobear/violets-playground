﻿using UnityEngine;
using System.Collections;

[NodePath("EasyTouch/Actions")]

[NodeCopyright("Copyright 2015 Reclusive Ninja")]
[NodeToolTip("Enables/Disables UI Compatibility.")]
[NodeAuthor("Reclusive Ninja", "http://reclusiveninja.com")]

[FriendlyName("EasyTouch Set UI Compatibility", "Enables/Disables UI Compatibility.")]
public class EasyTouch_SetUICompatibily : uScriptLogic
{
#region uScript Functions ------------------------------------------------------------------------
    public void In(
        [FriendlyName("Enable UI Compatibility", "Enables/Disables UI Compatibility.")]
        bool b_EnableUIComp)
    {
        HedgehogTeam.EasyTouch.EasyTouch.SetUICompatibily(b_EnableUIComp);
    }

//------------------------------------------------------------------------
    public bool Out
    {
        get { return true; }
    }
#endregion
}