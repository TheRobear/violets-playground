﻿using UnityEngine;
using System.Collections;

[NodePath("EasyTouch/Actions")]

[NodeCopyright("Copyright 2015 Reclusive Ninja")]
[NodeToolTip("Returns the Double Tap Time of EasyTouch.")]
[NodeAuthor("Reclusive Ninja", "http://reclusiveninja.com")]

[FriendlyName("EasyTouch Get Double Tap Time", "Returns the Double Tap Time of EasyTouch.")]
public class EasyTouch_GetDoubleTapTime : uScriptLogic
{
#region uScript Functions ------------------------------------------------------------------------
    public void In(
        [FriendlyName("Double Tap Time", "Returns the Double Tap Time of EasyTouch.")]
        out float f_DoubleTapTime)
    {
        f_DoubleTapTime = HedgehogTeam.EasyTouch.EasyTouch.GetDoubleTapTime();
    }

//------------------------------------------------------------------------
    public bool Out
    {
        get { return true; }
    }
#endregion
}