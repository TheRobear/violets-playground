*********************************************************************
					EasyTouch Nodes for uScript

Reclusive Ninja
http://reclusiveninja.com/forums

General Inquiries: contact@reclusiveninja.com
Support: support@reclusiveninja.com
*********************************************************************

V1.1
----------------------------------------------
* Created example packages for each version of uscript.
* Updated documentation.

V1.0
----------------------------------------------
* Added support for EasyTouch 5.