﻿using UnityEngine;
using System.Collections;

[NodePath("EasyTouch/Actions")]

[NodeCopyright("Copyright 2015 Reclusive Ninja")]
[NodeToolTip("Returns a camera from the Automatic Selection section.")]
[NodeAuthor("Reclusive Ninja", "http://reclusiveninja.com")]

[FriendlyName("EasyTouch Get Camera", "Returns a camera from the Automatic Selection section.")]
public class EasyTouch_GetCamera : uScriptLogic
{
#region uScript Functions ------------------------------------------------------------------------
    public void In(
        [FriendlyName("Camera Index", "The index of the camera you want to get.")]
        int i_CameraIndex,

        [FriendlyName("Camera", "The camera that is located at the specified camera index.")]
        out Camera c_Camera
        )
    {
        c_Camera = HedgehogTeam.EasyTouch.EasyTouch.GetCamera(i_CameraIndex);
    }
//------------------------------------------------------------------------
    public bool Out
    {
        get { return true; }
    }
#endregion
}