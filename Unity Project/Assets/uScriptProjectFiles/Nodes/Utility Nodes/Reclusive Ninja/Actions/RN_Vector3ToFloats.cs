﻿using UnityEngine;
using System.Collections;

[NodePath("Reclusive Ninja/Actions")]

[NodeCopyright("Copyright 2015 Reclusive Ninja")]
[NodeToolTip("Allows a Vector3 to be split into three float values for each respective axis.")]
[NodeAuthor("Reclusive Ninja", "http://reclusiveninja.com")]

[FriendlyName("Reclusive Ninja - Vector3 To Floats", "Allows a Vector3 to be split into three float values for each respective axis.")]
public class RN_Vector3ToFloats : uScriptLogic
{
#region uScript Functions ------------------------------------------------------------------------
    public void In(
        [FriendlyName("Vector3", "The Vector3 value to split.")]
        Vector3 v3_Vector3,
        
        [FriendlyName("X Value", "The float value for X.")]
        out float f_XValue,
        
        [FriendlyName("Y Value", "The float value for Y.")]
        out float f_YValue,
        
        [FriendlyName("Z Value", "The float value for Z.")]
        out float f_ZValue)
    {
        f_XValue = v3_Vector3.x;

        f_YValue = v3_Vector3.y;

        f_ZValue = v3_Vector3.z;
    }

//------------------------------------------------------------------------
    public bool Out
    {
        get { return true; }
    }
#endregion
}