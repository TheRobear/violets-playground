﻿using UnityEngine;
using System.Collections;

[NodePath("EasyTouch/Actions")]

[NodeCopyright("Copyright 2015 Reclusive Ninja")]
[NodeToolTip("Returns the Gesture Priority of EasyTouch.")]
[NodeAuthor("Reclusive Ninja", "http://reclusiveninja.com")]

[FriendlyName("EasyTouch Get Gesture Priority", "Returns the Gesture Priority of EasyTouch.")]
public class EasyTouch_GetGesturePriority : uScriptLogic
{
#region uScript Functions ------------------------------------------------------------------------
    public void In(
        [FriendlyName("Gesture Priority", "Returns the Gesture Priority of EasyTouch.")]
        out HedgehogTeam.EasyTouch.EasyTouch.GesturePriority etgp_GesturePriority)
    {
        etgp_GesturePriority = HedgehogTeam.EasyTouch.EasyTouch.GetGesturePriority();
    }

//------------------------------------------------------------------------
    public bool Out
    {
        get { return true; }
    }
#endregion
}