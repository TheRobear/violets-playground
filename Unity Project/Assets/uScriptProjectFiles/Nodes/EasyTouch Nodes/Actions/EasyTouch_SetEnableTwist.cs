﻿using UnityEngine;
using System.Collections;

[NodePath("EasyTouch/Actions")]

[NodeCopyright("Copyright 2015 Reclusive Ninja")]
[NodeToolTip("Enables/Disables the EasyTouch twist gesture.")]
[NodeAuthor("Reclusive Ninja", "http://reclusiveninja.com")]

[FriendlyName("EasyTouch Set Enable Twist", "Enables/Disables the EasyTouch twist gesture.")]
public class EasyTouch_SetEnableTwist : uScriptLogic
{
#region uScript Functions ------------------------------------------------------------------------
    public void In(
        [FriendlyName("Enable Twist", "Set this to true to enable the twist gesute. Set this to false to disable the twist gesture.")]
        bool b_EnableTwist)
    {
        HedgehogTeam.EasyTouch.EasyTouch.SetEnableTwist(b_EnableTwist);
    }

//------------------------------------------------------------------------
    public bool Out
    {
        get { return true; }
    }
#endregion
}