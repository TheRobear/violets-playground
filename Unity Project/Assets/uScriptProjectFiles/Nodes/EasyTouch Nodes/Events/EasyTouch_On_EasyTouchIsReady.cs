﻿using UnityEngine;
using System.Collections;

[NodePath("EasyTouch/Events")]

[NodeCopyright("Copyright 2015 Reclusive Ninja")]
[NodeToolTip("Uses EasyTouch On_EasyTouchIsReady to handle EasyTouch On_EasyTouchIsReady events.")]
[NodeAuthor("Reclusive Ninja", "http://reclusiveninja.com")]

[FriendlyName("EasyTouch - On Easy Touch Is Ready", "Uses EasyTouch On_EasyTouchIsReady to handle EasyTouch On_EasyTouchIsReady events.")]
public class EasyTouch_On_EasyTouchIsReady : uScriptEvent
{
#region uScript Event Handlers ------------------------------------------------------------------------
    //uScript event delcaration.
    public delegate void uScriptEventHandler(object sender, System.EventArgs args);

    //EasyTouch event delcaration.
    [FriendlyName("On_EasyTouchIsReady")]
    public event uScriptEventHandler OnEasyTouchIsReady;
#endregion

#region Unity Related Functions ------------------------------------------------------------------------
    //Subscribe to the EasyTouch On_EasyTouchIsReady delegate call when the object is enabled.
    void OnEnable()
    {
        HedgehogTeam.EasyTouch.EasyTouch.On_EasyTouchIsReady += On_EasyTouchIsReady;
    }

//------------------------------------------------------------------------
    //Unsubscribe to the EasyTouch On_EasyTouchIsReady delegate call when the object is disabled.
    void OnDisable()
    {
        HedgehogTeam.EasyTouch.EasyTouch.On_EasyTouchIsReady -= On_EasyTouchIsReady;
    }

//------------------------------------------------------------------------
    //Unsubscribe to the EasyTouch On_EasyTouchIsReady delegate call when the object is destroyed.
    void OnDestroy()
    {
        HedgehogTeam.EasyTouch.EasyTouch.On_EasyTouchIsReady -= On_EasyTouchIsReady;
    }
#endregion

#region EasyTouch Functions ------------------------------------------------------------------------
    //Calls the EasyTouch On_EasyTouchIsReady delecare call.
    public void On_EasyTouchIsReady()
    {
        TouchGesture();
    }

//------------------------------------------------------------------------
    //Sends the event to the node chain connected to this event.
    void TouchGesture()
    {
        if (OnEasyTouchIsReady != null)
        {
            OnEasyTouchIsReady(this, new System.EventArgs());
        }
    }

#endregion
}