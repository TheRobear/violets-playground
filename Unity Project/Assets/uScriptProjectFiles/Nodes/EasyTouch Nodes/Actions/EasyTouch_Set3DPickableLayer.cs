﻿using UnityEngine;
using System.Collections;

[NodePath("EasyTouch/Actions")]

[NodeCopyright("Copyright 2015 Reclusive Ninja")]
[NodeToolTip("Sets the layer for the EasyUpdate Set3DPickableLayer function.")]
[NodeAuthor("Reclusive Ninja", "http://reclusiveninja.com")]

[FriendlyName("EasyTouch Set 3D Pickable Layer", "Sets the layer for the EasyUpdate Set3DPickableLayer function.")]
public class EasyTouch_Set3DPickableLayer : uScriptLogic
{
#region uScript Functions ------------------------------------------------------------------------
    public void In(
        [FriendlyName("Layer Mask", "Sets the layer for the EasyUpdate Set3DPickableLayer function.")]
        LayerMask lm_LayerMask)
    {
        HedgehogTeam.EasyTouch.EasyTouch.Set3DPickableLayer(lm_LayerMask);
    }

//------------------------------------------------------------------------
    public bool Out
    {
        get { return true; }
    }
#endregion
}