﻿using UnityEngine;
using System.Collections;

[NodePath("EasyTouch/Actions")]

[NodeCopyright("Copyright 2015 Reclusive Ninja")]
[NodeToolTip("Returns if AutoUpdatePickedObject is enabled/disabled.")]
[NodeAuthor("Reclusive Ninja", "http://reclusiveninja.com")]

[FriendlyName("EasyTouch Get Auto Update Picked Object", "Returns if AutoUpdatePickedObject is enabled/disabled.")]
public class EasyTouch_GetAutoUpdatePickedObject : uScriptLogic
{
#region uScript Functions ------------------------------------------------------------------------
    public void In(
        [FriendlyName("Is Auto Update Picked Object Enabled?", "Returns if AutoUpdatePickedObject is enabled/disabled.")]
        out bool b_AutoUpdatePickedObject)
    {
        b_AutoUpdatePickedObject = HedgehogTeam.EasyTouch.EasyTouch.GetAutoUpdatePickedObject();
    }

//------------------------------------------------------------------------
    public bool Out
    {
        get { return true; }
    }
#endregion
}