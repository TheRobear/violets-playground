﻿using UnityEngine;
using System.Collections;

[NodePath("Particle Playground/Actions")]
public class ParticlePlayground_SetEmit : uScriptLogic
{
    public void In(
        GameObject go_ParticleObject,
        
        bool b_Emit)
    {
        ParticlePlayground.PlaygroundParticlesC ppg_Particles = go_ParticleObject.GetComponent<ParticlePlayground.PlaygroundParticlesC>();

        ppg_Particles.Emit(b_Emit);
    }

    public bool Out
    {
        get { return true; }
    }
}