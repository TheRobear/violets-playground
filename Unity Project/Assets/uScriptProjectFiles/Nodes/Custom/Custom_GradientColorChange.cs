﻿using UnityEngine;
using System.Collections;

[NodePath("Custom/Actions")]

[FriendlyName("Custom - Gradient Color Change")]
public class Custom_GradientColorChange : uScriptLogic
{
    public enum e_ColorChannel
    {
        Top,
        Bottom,
        Both
    }

    private Color c_TargetColor;

#region uScript Functions ------------------------------------------------------------------------
    public void In(
        GameObject go_Target,
        
        e_ColorChannel ecc_ColorChannel,
        
        [SocketState(false, false)]
        Color c_Top,

        [SocketState(false, false)]
        Color c_Bottom,
        
        float f_Time)
    {
        Color c_TopColor = go_Target.GetComponent<UnityEngine.UI.Image>().material.GetColor("_TopColor");
        Color c_BottomColor = go_Target.GetComponent<UnityEngine.UI.Image>().material.GetColor("_BottomColor");

        switch (ecc_ColorChannel)
        {
            case e_ColorChannel.Top:
                LeanTween.value(go_Target, c_TopColor, c_Top, f_Time).setOnUpdate((Color val)=>
                    go_Target.GetComponent<UnityEngine.UI.Image>().material.SetColor("_TopColor", val));
                break;

            case e_ColorChannel.Bottom:
                LeanTween.value(go_Target, c_BottomColor, c_Bottom, f_Time).setOnUpdate((Color val) =>
                    go_Target.GetComponent<UnityEngine.UI.Image>().material.SetColor("_BottomColor", val));
                break;

            case e_ColorChannel.Both:
                LeanTween.value(go_Target, c_TopColor, c_Top, f_Time).setOnUpdate((Color val) =>
                    go_Target.GetComponent<UnityEngine.UI.Image>().material.SetColor("_TopColor", val));
                LeanTween.value(go_Target, c_BottomColor, c_Bottom, f_Time).setOnUpdate((Color val) =>
                    go_Target.GetComponent<UnityEngine.UI.Image>().material.SetColor("_BottomColor", val));
                break;
        }
    }

//------------------------------------------------------------------------
    public bool Out
    {
        get { return true; }
    }
#endregion
}