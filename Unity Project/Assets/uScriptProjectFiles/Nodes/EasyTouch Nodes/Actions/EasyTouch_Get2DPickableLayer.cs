﻿using UnityEngine;
using System.Collections;

[NodePath("EasyTouch/Actions")]

[NodeCopyright("Copyright 2015 Reclusive Ninja")]
[NodeToolTip("Returns the pickable layer of 2D objects.")]
[NodeAuthor("Reclusive Ninja", "http://reclusiveninja.com")]

[FriendlyName("EasyTouch Get 2D Pickable Layer", "Returns the pickable layer of 2D objects.")]
public class EasyTouch_Get2DPickableLayer : uScriptLogic
{
#region uScript Functions ------------------------------------------------------------------------
    public void In(
        [FriendlyName("Layer Mask", "Returns the pickable layer of 2D objects.")]
        out LayerMask lm_LayerMask)
    {
        lm_LayerMask = HedgehogTeam.EasyTouch.EasyTouch.Get2DPickableLayer();
    }

//------------------------------------------------------------------------
    public bool Out
    {
        get { return true; }
    }
    #endregion
}