﻿using UnityEngine;
using System.Collections;

[NodePath("Reclusive Ninja/Actions")]

[NodeCopyright("Copyright 2015 Reclusive Ninja")]
[NodeToolTip("Returns the field of view of a camera.")]
[NodeAuthor("Reclusive Ninja", "http://reclusiveninja.com")]

[FriendlyName("Reclusive Ninja - Get Camera FOV", "Returns the field of view of a camera.")]
public class RN_GetCameraFOV : uScriptLogic
{
#region uScript Functions ------------------------------------------------------------------------
    public void In(
        [FriendlyName("Camera", "The target camera.")]
        Camera c_Camera,
        
        [FriendlyName("Field of View", "The field of view of the camera.")]
        out float f_FieldOfView)
    {
        f_FieldOfView = c_Camera.fieldOfView;
    }

//------------------------------------------------------------------------
    public bool Out
    {
        get { return true; }
    }
#endregion
}