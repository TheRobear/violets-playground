﻿using UnityEngine;
using System.Collections;

[NodePath("EasyTouch/Actions")]

[NodeCopyright("Copyright 2015 Reclusive Ninja")]
[NodeToolTip("Enables/Disables SetAutoUpdatePickedObject.")]
[NodeAuthor("Reclusive Ninja", "http://reclusiveninja.com")]

[FriendlyName("EasyTouch Set Auto Update Picked Object", "Enables/Disables SetAutoUpdatePickedObject.")]
public class EasyTouch_SetAutoUpdatePickedObject : uScriptLogic
{
#region uScript Functions ------------------------------------------------------------------------
    public void In(
        [FriendlyName("Enable SetAutoUpdatePickedObject", "Enables/Disables SetAutoUpdatePickedObject.")]
        bool b_AutoUpdatePickedObject)
    {
        HedgehogTeam.EasyTouch.EasyTouch.SetAutoUpdatePickedObject(b_AutoUpdatePickedObject);
    }

//------------------------------------------------------------------------
    public bool Out
    {
        get { return true; }
    }
#endregion
}