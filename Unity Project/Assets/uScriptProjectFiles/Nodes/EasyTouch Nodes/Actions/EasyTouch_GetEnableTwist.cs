﻿using UnityEngine;
using System.Collections;

[NodePath("EasyTouch/Actions")]

[NodeCopyright("Copyright 2015 Reclusive Ninja")]
[NodeToolTip("Returns if Twist is enabled/disabled.")]
[NodeAuthor("Reclusive Ninja", "http://reclusiveninja.com")]

[FriendlyName("EasyTouch Get Enable Twist", "Returns if Twist is enabled/disabled.")]
public class EasyTouch_GetEnableTwist : uScriptLogic
{
#region uScript Functions ------------------------------------------------------------------------
    public void In(
        [FriendlyName("Is Twist Enabled?", "Returns if Twist is enabled/disabled.")]
        out bool b_TwistEnabled)
    {
        b_TwistEnabled = HedgehogTeam.EasyTouch.EasyTouch.GetEnableTwist();
    }

//------------------------------------------------------------------------
    public bool Out
    {
        get { return true; }
    }
#endregion
}