﻿using UnityEngine;
using System.Collections;

[NodePath("EasyTouch/Events")]

[NodeCopyright("Copyright 2015 Reclusive Ninja")]
[NodeToolTip("Uses EasyTouch On_Cancel2Fingers to handle EasyTouch On_Cancel2Fingers events.")]
[NodeAuthor("Reclusive Ninja", "http://reclusiveninja.com")]

[FriendlyName("EasyTouch - On Cancel 2 Fingers", "Uses EasyTouch On_Cancel2Fingers to handle EasyTouch On_Cancel2Fingers events.")]
public class EasyTouch_On_Cancel2Fingers : uScriptEvent
{
#region uScript Event Handlers ------------------------------------------------------------------------
    //uScript event delcaration.
    public delegate void uScriptEventHandler(object sender, System.EventArgs args);

    //EasyTouch event delcaration.
    [FriendlyName("On_Cancel2Fingers")]
    public event uScriptEventHandler OnCancel2Fingers;
#endregion

#region Unity Related Functions ------------------------------------------------------------------------
    //Subscribe to the EasyTouch On_Cancel2Fingers delegate call when the object is enabled.
    void OnEnable()
    {
        HedgehogTeam.EasyTouch.EasyTouch.On_Cancel2Fingers += On_Cancel2Fingers;
    }

//------------------------------------------------------------------------
    //Unsubscribe to the EasyTouch On_Cancel2Fingers delegate call when the object is disabled.
    void OnDisable()
    {
        HedgehogTeam.EasyTouch.EasyTouch.On_Cancel2Fingers -= On_Cancel2Fingers;
    }

//------------------------------------------------------------------------
    //Unsubscribe to the EasyTouch On_Cancel2Fingers delegate call when the object is destroyed.
    void OnDestroy()
    {
        HedgehogTeam.EasyTouch.EasyTouch.On_Cancel2Fingers -= On_Cancel2Fingers;
    }
#endregion

#region EasyTouch Functions ------------------------------------------------------------------------
    //Calls the EasyTouch On_Cancel2Fingers delecare call.
    public void On_Cancel2Fingers(HedgehogTeam.EasyTouch.Gesture gesture)
    {
        if (gesture.pickedObject == gameObject)
        {
            TouchGesture();
        }
    }

//------------------------------------------------------------------------
    //Sends the event to the node chain connected to this event.
    void TouchGesture()
    {
        if (OnCancel2Fingers != null)
        {
            OnCancel2Fingers(this, new System.EventArgs());
        }
    }

#endregion
}