//uScript Generated Code - Build 1.0.3024
//Generated with Debug Info
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[NodePath("Graphs")]
[System.Serializable]
[FriendlyName("Untitled", "")]
public class VP_Test : uScriptLogic
{

   #pragma warning disable 414
   GameObject parentGameObject = null;
   uScript_GUI thisScriptsOnGuiListener = null; 
   bool m_RegisteredForEvents = false;
   delegate void ContinueExecution();
   ContinueExecution m_ContinueExecution;
   bool m_Breakpoint = false;
   const int MaxRelayCallCount = 1000;
   int relayCallCount = 0;
   
   //externally exposed events
   
   //external parameters
   
   //local nodes
   UnityEngine.GameObject local_1_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_1_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_12_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_12_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_2_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_2_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_4_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_4_UnityEngine_GameObject_previous = null;
   UnityEngine.Color local_RandomColor_UnityEngine_Color = new UnityEngine.Color( (float)0, (float)0, (float)0, (float)1 );
   UnityEngine.Vector3 local_WorldPoint_UnityEngine_Vector3 = new Vector3( (float)0, (float)0, (float)0 );
   
   //owner nodes
   
   //logic nodes
   //pointer to script instanced logic node
   uScriptAct_AnimatorSetParameterTrigger logic_uScriptAct_AnimatorSetParameterTrigger_uScriptAct_AnimatorSetParameterTrigger_3 = new uScriptAct_AnimatorSetParameterTrigger( );
   UnityEngine.GameObject[] logic_uScriptAct_AnimatorSetParameterTrigger_Target_3 = new UnityEngine.GameObject[] {};
   System.String logic_uScriptAct_AnimatorSetParameterTrigger_Name_3 = "Circle_Pop";
   bool logic_uScriptAct_AnimatorSetParameterTrigger_Out_3 = true;
   //pointer to script instanced logic node
   uScriptAct_SetGameObjectPosition logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_5 = new uScriptAct_SetGameObjectPosition( );
   UnityEngine.GameObject[] logic_uScriptAct_SetGameObjectPosition_Target_5 = new UnityEngine.GameObject[] {};
   UnityEngine.Vector3 logic_uScriptAct_SetGameObjectPosition_Position_5 = new Vector3( );
   System.Boolean logic_uScriptAct_SetGameObjectPosition_AsOffset_5 = (bool) false;
   System.Boolean logic_uScriptAct_SetGameObjectPosition_AsLocal_5 = (bool) false;
   bool logic_uScriptAct_SetGameObjectPosition_Out_5 = true;
   //pointer to script instanced logic node
   uScriptAct_PlayAudioSource logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_8 = new uScriptAct_PlayAudioSource( );
   UnityEngine.GameObject[] logic_uScriptAct_PlayAudioSource_target_8 = new UnityEngine.GameObject[] {};
   UnityEngine.AudioClip logic_uScriptAct_PlayAudioSource_audioClip_8 = default(UnityEngine.AudioClip);
   bool logic_uScriptAct_PlayAudioSource_Out_8 = true;
   //pointer to script instanced logic node
   uScriptAct_SetRandomColor logic_uScriptAct_SetRandomColor_uScriptAct_SetRandomColor_9 = new uScriptAct_SetRandomColor( );
   System.Single logic_uScriptAct_SetRandomColor_RedMin_9 = (float) 0;
   System.Single logic_uScriptAct_SetRandomColor_RedMax_9 = (float) 1;
   System.Single logic_uScriptAct_SetRandomColor_GreenMin_9 = (float) 0;
   System.Single logic_uScriptAct_SetRandomColor_GreenMax_9 = (float) 1;
   System.Single logic_uScriptAct_SetRandomColor_BlueMin_9 = (float) 0;
   System.Single logic_uScriptAct_SetRandomColor_BlueMax_9 = (float) 1;
   System.Single logic_uScriptAct_SetRandomColor_AlphaMin_9 = (float) 1;
   System.Single logic_uScriptAct_SetRandomColor_AlphaMax_9 = (float) 1;
   UnityEngine.Color logic_uScriptAct_SetRandomColor_TargetColor_9;
   bool logic_uScriptAct_SetRandomColor_Out_9 = true;
   //pointer to script instanced logic node
   uScriptAct_AssignMaterialColor logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_11 = new uScriptAct_AssignMaterialColor( );
   UnityEngine.GameObject[] logic_uScriptAct_AssignMaterialColor_Target_11 = new UnityEngine.GameObject[] {};
   UnityEngine.Color logic_uScriptAct_AssignMaterialColor_MatColor_11 = UnityEngine.Color.black;
   System.Int32 logic_uScriptAct_AssignMaterialColor_MatChannel_11 = (int) 0;
   bool logic_uScriptAct_AssignMaterialColor_Out_11 = true;
   
   //event nodes
   HedgehogTeam.EasyTouch.Gesture event_UnityEngine_GameObject_g_TargetGesture_0 = default(HedgehogTeam.EasyTouch.Gesture);
   System.Int32 event_UnityEngine_GameObject_i_FingerIndex_0 = (int) 0;
   System.Int32 event_UnityEngine_GameObject_i_TouchCount_0 = (int) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_StartPosition_0 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_StartPosition_0 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_Position_0 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_Position_0 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_DeltaPosition_0 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_DeltaPosition_0 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_ActionTime_0 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_DeltaTime_0 = (float) 0;
   HedgehogTeam.EasyTouch.EasyTouch.SwipeDirection event_UnityEngine_GameObject_etsw_SwipeDirection_0 = HedgehogTeam.EasyTouch.EasyTouch.SwipeDirection.None;
   System.String event_UnityEngine_GameObject_s_SwipeDirection_0 = "";
   System.Single event_UnityEngine_GameObject_f_SwipeLength_0 = (float) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_SwipeVector_0 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_SwipeVector_0 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_DeltaPinch_0 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_TwistAngle_0 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_TwoFingerDistance_0 = (float) 0;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_PickedObject_0 = default(UnityEngine.GameObject);
   UnityEngine.Camera event_UnityEngine_GameObject_c_PickedCamera_0 = default(UnityEngine.Camera);
   System.Boolean event_UnityEngine_GameObject_b_GUICamera_0 = (bool) false;
   System.Boolean event_UnityEngine_GameObject_b_IsOverGUI_0 = (bool) false;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_PickedUIElement_0 = default(UnityEngine.GameObject);
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_TouchToWorldPoint_0 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_TouchToWorldPoint_0 = new Vector3( );
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_0 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_0 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single event_UnityEngine_GameObject_f_AltitudeAngle_0 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_AzimuthAngle_0 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_MaximumPossiblePressure_0 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_pressure_0 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_Radius_0 = (float) 0;
   System.Single event_UnityEngine_GameObject_f_RadiusVariance_0 = (float) 0;
   HedgehogTeam.EasyTouch.EasyTouch.EvtType event_UnityEngine_GameObject_et_Type_0 = HedgehogTeam.EasyTouch.EasyTouch.EvtType.None;
   System.Single event_UnityEngine_GameObject_f_SwipeDragAngle_0 = (float) 0;
   UnityEngine.Vector2 event_UnityEngine_GameObject_v2_NormalizedPosition_0 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector3 event_UnityEngine_GameObject_v3_NormalizedPosition_0 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.GameObject event_UnityEngine_GameObject_go_CurrentPickedObject_0 = default(UnityEngine.GameObject);
   UnityEngine.GameObject event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_0 = default(UnityEngine.GameObject);
   System.Boolean event_UnityEngine_GameObject_b_OverUIElement_0 = (bool) false;
   UnityEngine.GameObject event_UnityEngine_GameObject_go_FirstUI_0 = default(UnityEngine.GameObject);
   UnityEngine.GameObject event_UnityEngine_GameObject_go_FirstUITwoFingers_0 = default(UnityEngine.GameObject);
   
   //property nodes
   
   //method nodes
   #pragma warning restore 414
   
   //functions to refresh properties from entities
   
   void SyncUnityHooks( )
   {
      SyncEventListeners( );
      if ( null == local_1_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_1_UnityEngine_GameObject = uScript_MasterComponent.LatestMaster;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_1_UnityEngine_GameObject_previous != local_1_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_1_UnityEngine_GameObject_previous )
         {
            {
               EasyTouch_On_Touch component = local_1_UnityEngine_GameObject_previous.GetComponent<EasyTouch_On_Touch>();
               if ( null != component )
               {
                  component.OnTouchStart -= Instance_OnTouchStart_0;
                  component.OnTouchDown -= Instance_OnTouchDown_0;
                  component.OnTouchUp -= Instance_OnTouchUp_0;
               }
            }
         }
         
         local_1_UnityEngine_GameObject_previous = local_1_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_1_UnityEngine_GameObject )
         {
            {
               EasyTouch_On_Touch component = local_1_UnityEngine_GameObject.GetComponent<EasyTouch_On_Touch>();
               if ( null == component )
               {
                  component = local_1_UnityEngine_GameObject.AddComponent<EasyTouch_On_Touch>();
               }
               if ( null != component )
               {
                  component.OnTouchStart += Instance_OnTouchStart_0;
                  component.OnTouchDown += Instance_OnTouchDown_0;
                  component.OnTouchUp += Instance_OnTouchUp_0;
               }
            }
         }
      }
      if ( null == local_2_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_2_UnityEngine_GameObject = GameObject.Find( "Circle_Pop" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_2_UnityEngine_GameObject_previous != local_2_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_2_UnityEngine_GameObject_previous = local_2_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_4_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_4_UnityEngine_GameObject = GameObject.Find( "Circle_Pop" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_4_UnityEngine_GameObject_previous != local_4_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_4_UnityEngine_GameObject_previous = local_4_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_12_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_12_UnityEngine_GameObject = GameObject.Find( "Circle_Pop" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_12_UnityEngine_GameObject_previous != local_12_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_12_UnityEngine_GameObject_previous = local_12_UnityEngine_GameObject;
         
         //setup new listeners
      }
   }
   
   void RegisterForUnityHooks( )
   {
      SyncEventListeners( );
      //if our game object reference was changed then we need to reset event listeners
      if ( local_1_UnityEngine_GameObject_previous != local_1_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_1_UnityEngine_GameObject_previous )
         {
            {
               EasyTouch_On_Touch component = local_1_UnityEngine_GameObject_previous.GetComponent<EasyTouch_On_Touch>();
               if ( null != component )
               {
                  component.OnTouchStart -= Instance_OnTouchStart_0;
                  component.OnTouchDown -= Instance_OnTouchDown_0;
                  component.OnTouchUp -= Instance_OnTouchUp_0;
               }
            }
         }
         
         local_1_UnityEngine_GameObject_previous = local_1_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_1_UnityEngine_GameObject )
         {
            {
               EasyTouch_On_Touch component = local_1_UnityEngine_GameObject.GetComponent<EasyTouch_On_Touch>();
               if ( null == component )
               {
                  component = local_1_UnityEngine_GameObject.AddComponent<EasyTouch_On_Touch>();
               }
               if ( null != component )
               {
                  component.OnTouchStart += Instance_OnTouchStart_0;
                  component.OnTouchDown += Instance_OnTouchDown_0;
                  component.OnTouchUp += Instance_OnTouchUp_0;
               }
            }
         }
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_2_UnityEngine_GameObject_previous != local_2_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_2_UnityEngine_GameObject_previous = local_2_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_4_UnityEngine_GameObject_previous != local_4_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_4_UnityEngine_GameObject_previous = local_4_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_12_UnityEngine_GameObject_previous != local_12_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_12_UnityEngine_GameObject_previous = local_12_UnityEngine_GameObject;
         
         //setup new listeners
      }
   }
   
   void SyncEventListeners( )
   {
   }
   
   void UnregisterEventListeners( )
   {
      if ( null != local_1_UnityEngine_GameObject )
      {
         {
            EasyTouch_On_Touch component = local_1_UnityEngine_GameObject.GetComponent<EasyTouch_On_Touch>();
            if ( null != component )
            {
               component.OnTouchStart -= Instance_OnTouchStart_0;
               component.OnTouchDown -= Instance_OnTouchDown_0;
               component.OnTouchUp -= Instance_OnTouchUp_0;
            }
         }
      }
   }
   
   public override void SetParent(GameObject g)
   {
      parentGameObject = g;
      
      logic_uScriptAct_AnimatorSetParameterTrigger_uScriptAct_AnimatorSetParameterTrigger_3.SetParent(g);
      logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_5.SetParent(g);
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_8.SetParent(g);
      logic_uScriptAct_SetRandomColor_uScriptAct_SetRandomColor_9.SetParent(g);
      logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_11.SetParent(g);
   }
   public void Awake()
   {
      
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_8.Finished += uScriptAct_PlayAudioSource_Finished_8;
   }
   
   public void Start()
   {
      SyncUnityHooks( );
      m_RegisteredForEvents = true;
      
   }
   
   public void OnEnable()
   {
      RegisterForUnityHooks( );
      m_RegisteredForEvents = true;
   }
   
   public void OnDisable()
   {
      UnregisterEventListeners( );
      m_RegisteredForEvents = false;
   }
   
   public void Update()
   {
      //reset each Update, and increments each method call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      if ( null != m_ContinueExecution )
      {
         ContinueExecution continueEx = m_ContinueExecution;
         m_ContinueExecution = null;
         m_Breakpoint = false;
         continueEx( );
         return;
      }
      UpdateEditorValues( );
      
      //other scripts might have added GameObjects with event scripts
      //so we need to verify all our event listeners are registered
      SyncEventListeners( );
      
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_8.Update( );
   }
   
   public void OnDestroy()
   {
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_8.Finished -= uScriptAct_PlayAudioSource_Finished_8;
   }
   
   void Instance_OnTouchStart_0(object o, EasyTouch_On_Touch.OnTouchEventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_0 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_0 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_0 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_0 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_0 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_0 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_0 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_0 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_0 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_0 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_0 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_0 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_0 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_0 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_0 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_0 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_0 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_0 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_0 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_0 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_0 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_0 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_0 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_0 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_0 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_0 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_0 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_0 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_0 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_0 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_0 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_0 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_0 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_0 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_0 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_0 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_0 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_0 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_0 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_0 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_0 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_0 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_0 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnTouchStart_0( );
   }
   
   void Instance_OnTouchDown_0(object o, EasyTouch_On_Touch.OnTouchEventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_0 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_0 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_0 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_0 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_0 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_0 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_0 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_0 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_0 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_0 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_0 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_0 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_0 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_0 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_0 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_0 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_0 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_0 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_0 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_0 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_0 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_0 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_0 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_0 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_0 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_0 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_0 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_0 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_0 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_0 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_0 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_0 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_0 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_0 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_0 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_0 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_0 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_0 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_0 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_0 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_0 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_0 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_0 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnTouchDown_0( );
   }
   
   void Instance_OnTouchUp_0(object o, EasyTouch_On_Touch.OnTouchEventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      event_UnityEngine_GameObject_g_TargetGesture_0 = e.g_TargetGesture;
      event_UnityEngine_GameObject_i_FingerIndex_0 = e.i_FingerIndex;
      event_UnityEngine_GameObject_i_TouchCount_0 = e.i_TouchCount;
      event_UnityEngine_GameObject_v2_StartPosition_0 = e.v2_StartPosition;
      event_UnityEngine_GameObject_v3_StartPosition_0 = e.v3_StartPosition;
      event_UnityEngine_GameObject_v2_Position_0 = e.v2_Position;
      event_UnityEngine_GameObject_v3_Position_0 = e.v3_Position;
      event_UnityEngine_GameObject_v2_DeltaPosition_0 = e.v2_DeltaPosition;
      event_UnityEngine_GameObject_v3_DeltaPosition_0 = e.v3_DeltaPosition;
      event_UnityEngine_GameObject_f_ActionTime_0 = e.f_ActionTime;
      event_UnityEngine_GameObject_f_DeltaTime_0 = e.f_DeltaTime;
      event_UnityEngine_GameObject_etsw_SwipeDirection_0 = e.etsw_SwipeDirection;
      event_UnityEngine_GameObject_s_SwipeDirection_0 = e.s_SwipeDirection;
      event_UnityEngine_GameObject_f_SwipeLength_0 = e.f_SwipeLength;
      event_UnityEngine_GameObject_v2_SwipeVector_0 = e.v2_SwipeVector;
      event_UnityEngine_GameObject_v3_SwipeVector_0 = e.v3_SwipeVector;
      event_UnityEngine_GameObject_f_DeltaPinch_0 = e.f_DeltaPinch;
      event_UnityEngine_GameObject_f_TwistAngle_0 = e.f_TwistAngle;
      event_UnityEngine_GameObject_f_TwoFingerDistance_0 = e.f_TwoFingerDistance;
      event_UnityEngine_GameObject_go_PickedObject_0 = e.go_PickedObject;
      event_UnityEngine_GameObject_c_PickedCamera_0 = e.c_PickedCamera;
      event_UnityEngine_GameObject_b_GUICamera_0 = e.b_GUICamera;
      event_UnityEngine_GameObject_b_IsOverGUI_0 = e.b_IsOverGUI;
      event_UnityEngine_GameObject_go_PickedUIElement_0 = e.go_PickedUIElement;
      event_UnityEngine_GameObject_v2_TouchToWorldPoint_0 = e.v2_TouchToWorldPoint;
      event_UnityEngine_GameObject_v3_TouchToWorldPoint_0 = e.v3_TouchToWorldPoint;
      event_UnityEngine_GameObject_v2_TouchToWorldPointPickedObject_0 = e.v2_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_v3_TouchToWorldPointPickedObject_0 = e.v3_TouchToWorldPointPickedObject;
      event_UnityEngine_GameObject_f_AltitudeAngle_0 = e.f_AltitudeAngle;
      event_UnityEngine_GameObject_f_AzimuthAngle_0 = e.f_AzimuthAngle;
      event_UnityEngine_GameObject_f_MaximumPossiblePressure_0 = e.f_MaximumPossiblePressure;
      event_UnityEngine_GameObject_f_pressure_0 = e.f_pressure;
      event_UnityEngine_GameObject_f_Radius_0 = e.f_Radius;
      event_UnityEngine_GameObject_f_RadiusVariance_0 = e.f_RadiusVariance;
      event_UnityEngine_GameObject_et_Type_0 = e.et_Type;
      event_UnityEngine_GameObject_f_SwipeDragAngle_0 = e.f_SwipeDragAngle;
      event_UnityEngine_GameObject_v2_NormalizedPosition_0 = e.v2_NormalizedPosition;
      event_UnityEngine_GameObject_v3_NormalizedPosition_0 = e.v3_NormalizedPosition;
      event_UnityEngine_GameObject_go_CurrentPickedObject_0 = e.go_CurrentPickedObject;
      event_UnityEngine_GameObject_go_CurrentPickedObjectTwoFinger_0 = e.go_CurrentPickedObjectTwoFinger;
      event_UnityEngine_GameObject_b_OverUIElement_0 = e.b_OverUIElement;
      event_UnityEngine_GameObject_go_FirstUI_0 = e.go_FirstUI;
      event_UnityEngine_GameObject_go_FirstUITwoFingers_0 = e.go_FirstUITwoFingers;
      //relay event to nodes
      Relay_OnTouchUp_0( );
   }
   
   void uScriptAct_PlayAudioSource_Finished_8(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_Finished_8( );
   }
   
   void Relay_OnTouchStart_0()
   {
      if (true == CheckDebugBreak("bc13682f-6f43-4158-83c1-cc35f441fdb5", "EasyTouch___On_Touch", Relay_OnTouchStart_0)) return; 
      local_WorldPoint_UnityEngine_Vector3 = event_UnityEngine_GameObject_v3_TouchToWorldPoint_0;
      Relay_In_5();
   }
   
   void Relay_OnTouchDown_0()
   {
      if (true == CheckDebugBreak("bc13682f-6f43-4158-83c1-cc35f441fdb5", "EasyTouch___On_Touch", Relay_OnTouchDown_0)) return; 
      local_WorldPoint_UnityEngine_Vector3 = event_UnityEngine_GameObject_v3_TouchToWorldPoint_0;
   }
   
   void Relay_OnTouchUp_0()
   {
      if (true == CheckDebugBreak("bc13682f-6f43-4158-83c1-cc35f441fdb5", "EasyTouch___On_Touch", Relay_OnTouchUp_0)) return; 
      local_WorldPoint_UnityEngine_Vector3 = event_UnityEngine_GameObject_v3_TouchToWorldPoint_0;
   }
   
   void Relay_In_3()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("0ab8e64c-c31e-4ea0-9dc2-866ff2a2fa02", "Set_Animator_Parameter__Trigger_", Relay_In_3)) return; 
         {
            {
               int index = 0;
               {
                  //if our game object reference was changed then we need to reset event listeners
                  if ( local_4_UnityEngine_GameObject_previous != local_4_UnityEngine_GameObject || false == m_RegisteredForEvents )
                  {
                     //tear down old listeners
                     
                     local_4_UnityEngine_GameObject_previous = local_4_UnityEngine_GameObject;
                     
                     //setup new listeners
                  }
               }
               if ( logic_uScriptAct_AnimatorSetParameterTrigger_Target_3.Length <= index)
               {
                  System.Array.Resize(ref logic_uScriptAct_AnimatorSetParameterTrigger_Target_3, index + 1);
               }
               logic_uScriptAct_AnimatorSetParameterTrigger_Target_3[ index++ ] = local_4_UnityEngine_GameObject;
               
            }
            {
            }
         }
         logic_uScriptAct_AnimatorSetParameterTrigger_uScriptAct_AnimatorSetParameterTrigger_3.In(logic_uScriptAct_AnimatorSetParameterTrigger_Target_3, logic_uScriptAct_AnimatorSetParameterTrigger_Name_3);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_AnimatorSetParameterTrigger_uScriptAct_AnimatorSetParameterTrigger_3.Out;
         
         if ( test_0 == true )
         {
            Relay_Play_8();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript VP_Test.uscript at Set Animator Parameter (Trigger).  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_5()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("b2201df5-ca1d-4752-88e4-82eb488c4747", "Set_Position", Relay_In_5)) return; 
         {
            {
               int index = 0;
               {
                  //if our game object reference was changed then we need to reset event listeners
                  if ( local_2_UnityEngine_GameObject_previous != local_2_UnityEngine_GameObject || false == m_RegisteredForEvents )
                  {
                     //tear down old listeners
                     
                     local_2_UnityEngine_GameObject_previous = local_2_UnityEngine_GameObject;
                     
                     //setup new listeners
                  }
               }
               if ( logic_uScriptAct_SetGameObjectPosition_Target_5.Length <= index)
               {
                  System.Array.Resize(ref logic_uScriptAct_SetGameObjectPosition_Target_5, index + 1);
               }
               logic_uScriptAct_SetGameObjectPosition_Target_5[ index++ ] = local_2_UnityEngine_GameObject;
               
            }
            {
               logic_uScriptAct_SetGameObjectPosition_Position_5 = local_WorldPoint_UnityEngine_Vector3;
               
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_5.In(logic_uScriptAct_SetGameObjectPosition_Target_5, logic_uScriptAct_SetGameObjectPosition_Position_5, logic_uScriptAct_SetGameObjectPosition_AsOffset_5, logic_uScriptAct_SetGameObjectPosition_AsLocal_5);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_5.Out;
         
         if ( test_0 == true )
         {
            Relay_In_3();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript VP_Test.uscript at Set Position.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_Finished_8()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("d8061fac-888c-4685-b3da-108873f37672", "Play_AudioSource", Relay_Finished_8)) return; 
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript VP_Test.uscript at Play AudioSource.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_Play_8()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("d8061fac-888c-4685-b3da-108873f37672", "Play_AudioSource", Relay_Play_8)) return; 
         {
            {
               int index = 0;
               {
                  //if our game object reference was changed then we need to reset event listeners
                  if ( local_4_UnityEngine_GameObject_previous != local_4_UnityEngine_GameObject || false == m_RegisteredForEvents )
                  {
                     //tear down old listeners
                     
                     local_4_UnityEngine_GameObject_previous = local_4_UnityEngine_GameObject;
                     
                     //setup new listeners
                  }
               }
               if ( logic_uScriptAct_PlayAudioSource_target_8.Length <= index)
               {
                  System.Array.Resize(ref logic_uScriptAct_PlayAudioSource_target_8, index + 1);
               }
               logic_uScriptAct_PlayAudioSource_target_8[ index++ ] = local_4_UnityEngine_GameObject;
               
            }
            {
            }
         }
         logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_8.Play(logic_uScriptAct_PlayAudioSource_target_8, logic_uScriptAct_PlayAudioSource_audioClip_8);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_8.Out;
         
         if ( test_0 == true )
         {
            Relay_In_9();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript VP_Test.uscript at Play AudioSource.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_Stop_8()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("d8061fac-888c-4685-b3da-108873f37672", "Play_AudioSource", Relay_Stop_8)) return; 
         {
            {
               int index = 0;
               {
                  //if our game object reference was changed then we need to reset event listeners
                  if ( local_4_UnityEngine_GameObject_previous != local_4_UnityEngine_GameObject || false == m_RegisteredForEvents )
                  {
                     //tear down old listeners
                     
                     local_4_UnityEngine_GameObject_previous = local_4_UnityEngine_GameObject;
                     
                     //setup new listeners
                  }
               }
               if ( logic_uScriptAct_PlayAudioSource_target_8.Length <= index)
               {
                  System.Array.Resize(ref logic_uScriptAct_PlayAudioSource_target_8, index + 1);
               }
               logic_uScriptAct_PlayAudioSource_target_8[ index++ ] = local_4_UnityEngine_GameObject;
               
            }
            {
            }
         }
         logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_8.Stop(logic_uScriptAct_PlayAudioSource_target_8, logic_uScriptAct_PlayAudioSource_audioClip_8);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_8.Out;
         
         if ( test_0 == true )
         {
            Relay_In_9();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript VP_Test.uscript at Play AudioSource.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_9()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("67ab6b8f-6d43-44d6-b32a-527c37d8a6de", "Set_Random_Color", Relay_In_9)) return; 
         {
            {
            }
            {
            }
            {
            }
            {
            }
            {
            }
            {
            }
            {
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_SetRandomColor_uScriptAct_SetRandomColor_9.In(logic_uScriptAct_SetRandomColor_RedMin_9, logic_uScriptAct_SetRandomColor_RedMax_9, logic_uScriptAct_SetRandomColor_GreenMin_9, logic_uScriptAct_SetRandomColor_GreenMax_9, logic_uScriptAct_SetRandomColor_BlueMin_9, logic_uScriptAct_SetRandomColor_BlueMax_9, logic_uScriptAct_SetRandomColor_AlphaMin_9, logic_uScriptAct_SetRandomColor_AlphaMax_9, out logic_uScriptAct_SetRandomColor_TargetColor_9);
         local_RandomColor_UnityEngine_Color = logic_uScriptAct_SetRandomColor_TargetColor_9;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_SetRandomColor_uScriptAct_SetRandomColor_9.Out;
         
         if ( test_0 == true )
         {
            Relay_In_11();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript VP_Test.uscript at Set Random Color.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_11()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("216c6dcd-c208-43ac-874f-98ad6d6b38a0", "Assign_Material_Color", Relay_In_11)) return; 
         {
            {
               int index = 0;
               {
                  //if our game object reference was changed then we need to reset event listeners
                  if ( local_12_UnityEngine_GameObject_previous != local_12_UnityEngine_GameObject || false == m_RegisteredForEvents )
                  {
                     //tear down old listeners
                     
                     local_12_UnityEngine_GameObject_previous = local_12_UnityEngine_GameObject;
                     
                     //setup new listeners
                  }
               }
               if ( logic_uScriptAct_AssignMaterialColor_Target_11.Length <= index)
               {
                  System.Array.Resize(ref logic_uScriptAct_AssignMaterialColor_Target_11, index + 1);
               }
               logic_uScriptAct_AssignMaterialColor_Target_11[ index++ ] = local_12_UnityEngine_GameObject;
               
            }
            {
               logic_uScriptAct_AssignMaterialColor_MatColor_11 = local_RandomColor_UnityEngine_Color;
               
            }
            {
            }
         }
         logic_uScriptAct_AssignMaterialColor_uScriptAct_AssignMaterialColor_11.In(logic_uScriptAct_AssignMaterialColor_Target_11, logic_uScriptAct_AssignMaterialColor_MatColor_11, logic_uScriptAct_AssignMaterialColor_MatChannel_11);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript VP_Test.uscript at Assign Material Color.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   private void UpdateEditorValues( )
   {
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "VP_Test.uscript:1", local_1_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "3751de05-e873-4c21-ba9b-bf50ca7cf823", local_1_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "VP_Test.uscript:2", local_2_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "df619874-dfc9-4549-9f77-a54e95b50c57", local_2_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "VP_Test.uscript:4", local_4_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "01430a2f-8bf9-449b-a8f6-4f439fe9957a", local_4_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "VP_Test.uscript:WorldPoint", local_WorldPoint_UnityEngine_Vector3);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "8e2d524a-187e-4517-acec-17d12785c8d0", local_WorldPoint_UnityEngine_Vector3);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "VP_Test.uscript:12", local_12_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "aa713963-e419-46f2-9c1c-c9a68f916fe7", local_12_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "VP_Test.uscript:RandomColor", local_RandomColor_UnityEngine_Color);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "cc16881a-6337-4476-96b2-ccb63e0f5ef5", local_RandomColor_UnityEngine_Color);
   }
   bool CheckDebugBreak(string guid, string name, ContinueExecution method)
   {
      if (true == m_Breakpoint) return true;
      
      if (true == uScript_MasterComponent.FindBreakpoint(guid))
      {
         if (uScript_MasterComponent.LatestMasterComponent.CurrentBreakpoint == guid)
         {
            uScript_MasterComponent.LatestMasterComponent.CurrentBreakpoint = "";
         }
         else
         {
            uScript_MasterComponent.LatestMasterComponent.CurrentBreakpoint = guid;
            UpdateEditorValues( );
            UnityEngine.Debug.Log("uScript BREAK Node:" + name + " ((Time: " + Time.time + "");
            UnityEngine.Debug.Break();
            #if (!UNITY_FLASH)
            m_ContinueExecution = new ContinueExecution(method);
            #endif
            m_Breakpoint = true;
            return true;
         }
      }
      return false;
   }
}
