﻿using UnityEngine;
using System.Collections;

[NodePath("EasyTouch/Actions")]

[NodeCopyright("Copyright 2015 Reclusive Ninja")]
[NodeToolTip("Returns if EasyTouch is enabled/disabled.")]
[NodeAuthor("Reclusive Ninja", "http://reclusiveninja.com")]

[FriendlyName("EasyTouch Get Enabled", "Returns if EasyTouch is enabled/disabled.")]
public class EasyTouch_GetEnabled : uScriptLogic
{
#region uScript Functions ------------------------------------------------------------------------
    public void In(
        [FriendlyName("Is EasyTouch Enabled?", "Returns if EasyTouch is enabled/disabled.")]
        out bool b_EasyTouchEnabled)
    {
        b_EasyTouchEnabled = HedgehogTeam.EasyTouch.EasyTouch.GetEnablePinch();
    }

//------------------------------------------------------------------------
    public bool Out
    {
        get { return true; }
    }
#endregion
}