﻿using UnityEngine;
using System.Collections;

[NodePath("Custom/Actions")]
public class Custom_SetLineRendererState : uScriptLogic
{
	public void In(
		GameObject[] go_GameObject,
		
		bool b_Enable)
	{
		foreach (GameObject gObject in go_GameObject)
		{
			gObject.GetComponentInChildren<LineRenderer>().enabled = b_Enable;
		}	
	}
	
//------------------------------------------------------------------------
	public bool Out
	{
		get { return true; }
	}
}