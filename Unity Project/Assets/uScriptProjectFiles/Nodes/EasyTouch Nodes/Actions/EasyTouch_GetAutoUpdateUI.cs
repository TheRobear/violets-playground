﻿using UnityEngine;
using System.Collections;

[NodePath("EasyTouch/Actions")]

[NodeCopyright("Copyright 2015 Reclusive Ninja")]
[NodeToolTip("Returns if AutoUpdateUI is enabled/disabled.")]
[NodeAuthor("Reclusive Ninja", "http://reclusiveninja.com")]

[FriendlyName("EasyTouch Get Auto Update UI", "Returns if AutoUpdateUI is enabled/disabled.")]
public class EasyTouch_GetAutoUpdateUI : uScriptLogic
{
#region uScript Functions ------------------------------------------------------------------------
    public void In(
        [FriendlyName("Is Auto Update UI Enabled?", "Returns if AutoUpdateUI is enabled/disabled.")]
        out bool b_AutoUpdateUIEnabled)
    {
        b_AutoUpdateUIEnabled = HedgehogTeam.EasyTouch.EasyTouch.GetAutoUpdateUI();
    }

//------------------------------------------------------------------------
    public bool Out
    {
        get { return true; }
    }
#endregion
}