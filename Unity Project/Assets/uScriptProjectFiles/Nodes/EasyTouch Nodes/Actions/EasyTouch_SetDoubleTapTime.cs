﻿using UnityEngine;
using System.Collections;

[NodePath("EasyTouch/Actions")]

[NodeCopyright("Copyright 2015 Reclusive Ninja")]
[NodeToolTip("Sets the Double Tap Time for EasyTouch.")]
[NodeAuthor("Reclusive Ninja", "http://reclusiveninja.com")]

[FriendlyName("EasyTouch Set Double Tap Time", "Sets the Double Tap Time for EasyTouch.")]
public class EasyTouch_SetDoubleTapTime : uScriptLogic
{
 #region uScript Functions ------------------------------------------------------------------------
    public void In(
        [FriendlyName("Double Tap Time", "Sets the Double Tap Time for EasyTouch.")]
        float f_DoubleTapTime
        )
    {
        HedgehogTeam.EasyTouch.EasyTouch.SetDoubleTapTime(f_DoubleTapTime);
    }

//------------------------------------------------------------------------
    public bool Out
    {
        get { return true; }
    }
#endregion
}