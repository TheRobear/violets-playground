﻿using UnityEngine;
using System.Collections;

public class ParticlePlayground_SetProperties : uScriptLogic
{
    public void In(
        GameObject go_ParticleObject,
        
        Vector3 v3_Position,

        Vector3 v3_VelMin,

        Vector3 v3_VelMax,
        
        Color c_Color)
    {
        ParticlePlayground.PlaygroundParticlesC ppg_Particles = go_ParticleObject.GetComponent<ParticlePlayground.PlaygroundParticlesC>();

        ppg_Particles.Emit(ppg_Particles.particleCount, v3_Position, v3_VelMin, v3_VelMax, c_Color);
    }

    public bool Out
    {
        get { return true; }
    }
}