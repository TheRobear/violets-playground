﻿using UnityEngine;
using System.Collections;

[NodePath("Custom/Actions")]

[FriendlyName("Custom - Compare Color")]
public class Custom_CompareColor : uScriptLogic
{
    private bool b_Compare = false;

#region uScript Functions ------------------------------------------------------------------------
    public void In(
        [FriendlyName("A", "First value to compare.")]
        Color c_A,

        [FriendlyName("B", "Second value to compare.")]
        Color c_B)
    {
        b_Compare = c_A == c_B;
    }

//------------------------------------------------------------------------
    public bool Same
    {
        get { return b_Compare; }
    }

//------------------------------------------------------------------------
    public bool Different
    {
        get { return !b_Compare; }
    }
#endregion
}