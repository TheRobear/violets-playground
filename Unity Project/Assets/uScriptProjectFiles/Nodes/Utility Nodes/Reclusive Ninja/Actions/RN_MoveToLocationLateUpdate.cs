﻿using UnityEngine;
using System.Collections;

[NodePath("Reclusive Ninja/Actions")]

[NodeCopyright("Copyright 2015 Reclusive Ninja")]
[NodeToolTip("Moves a game object to the end location using Late Update.")]
[NodeAuthor("Reclusive Ninja", "http://reclusiveninja.com")]

[FriendlyName("Reclusive Ninja - Move To Location Late Update", "Moves a game object to the end location using Late Update.")]
public class RN_MoveToLocationLateUpdate : uScriptLogic
{
    public bool Out
    {
        get { return true; }
    }

    public bool Cancelled
    {
        get { return b_cancelled; }
    }

    public event System.EventHandler Finished;

    private GameObject[] g_gameObject;
    private Vector3 v3_location;
    private Vector3[] v3_startingLocation;
    private float f_time;
    private float f_currentTime;
    private bool b_cancelled;
    private bool b_asOffset;

    public void In(
        [FriendlyName("Target", "The object(s) you want to be moved.")]
        GameObject[] g_GameObject,
        
        [FriendlyName("End Location", "The location you want the object(s) to move to.")]
        Vector3 v3_Location,

        [FriendlyName("Use As Offset", "Whether or not to treat End Location as an offset, rather than an absolute position.")]
        bool b_AsOffset,
        
        [FriendlyName("Time", "The time it will take to complete the movement.")]
        float f_Time)
    {
        b_cancelled = false;
        f_time = f_Time;
        f_currentTime = 0;

        if (0 == f_time)
        {
            if (true == b_AsOffset)
            {
                foreach (GameObject tmpObj in g_GameObject)
                {
                    if (null == tmpObj)
                    {
                        continue;
                    }

                    tmpObj.transform.position = tmpObj.transform.position + v3_Location;
                }
            }
            else
            {
                foreach (GameObject tmpObj in g_GameObject)
                {
                    if (null == tmpObj)
                    {
                        continue;
                    }
                    tmpObj.transform.position = v3_Location;
                }
            }
        }
        else 
        {
            b_asOffset = b_AsOffset;
            g_gameObject = g_GameObject;
            v3_location = v3_Location;
            v3_startingLocation = new Vector3[g_gameObject.Length];

            for(int i = 0; i < g_gameObject.Length; i++)
            {
                GameObject tmpObj = g_gameObject[i];

                if (null == tmpObj)
                {
                    continue;
                }

                v3_startingLocation[i] = tmpObj.transform.position;
            }
        }
    }

    public void Cancel(
        [FriendlyName("Target", "The object(s) you want to be moved.")]
        GameObject[] g_GameObject,
        
        [FriendlyName("Location", "The location you want the object(s) to move to.")]
        Vector3 v3_Location,

        [FriendlyName("Use As Offset", "Whether or not to treat End Location as an offset, rather than an absolute position.")]
        bool b_AsOffset,
        
        [FriendlyName("Time", "The time it will take to complete the movement.")]
        float f_Time
        )
    {
        if (f_currentTime != f_time)
        {
            b_cancelled = true;
            f_currentTime = f_time;
        }
    }

    public void LateUpdate()
    {
        if (f_currentTime == f_time)
        {
            return;
        }

        f_currentTime += Time.deltaTime;

        if (f_currentTime >= f_time)
        {
            f_currentTime = f_time;
        }

        if (true == b_asOffset)
        {
            for (int i = 0; i < g_gameObject.Length; i++)
            {
                GameObject tmpObj = g_gameObject[i];

                if (null == tmpObj)
                {
                    continue;
                }

                tmpObj.transform.position = Vector3.Lerp(v3_startingLocation[i], v3_location + v3_startingLocation[i], f_time);
            }
        }
        else
        {
            for (int i = 0; i < g_gameObject.Length; i++)
            {
                GameObject tmpObj = g_gameObject[i];

                if (null == tmpObj)
                {
                    continue;
                }

                tmpObj.transform.position = Vector3.Lerp(v3_startingLocation[i], v3_location, f_time);
            }
        }

        if (f_currentTime == f_time)
        {
            if (Finished != null)
            {
                Finished(this, new System.EventArgs());
            }
        }
    }
}