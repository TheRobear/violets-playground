﻿using UnityEngine;
using System.Collections;

[NodePath("Reclusive Ninja/Actions")]

[NodeCopyright("Copyright 2015 Reclusive Ninja")]
[NodeToolTip("Returns the target transform direction.")]
[NodeAuthor("Reclusive Ninja", "http://reclusiveninja.com")]

[FriendlyName("Reclusive Ninja - Return Transform Direction", "Returns the target transform direction.")]
public class RN_ReturnTransformDirection : uScriptLogic
{
    public enum Direction { Forward, Right, Up };

#region uScript Functions ------------------------------------------------------------------------
    public void In(
        [FriendlyName("Target", "The target game object.")]
        GameObject go_GameObject,
        
        [FriendlyName("Direction", "The desired direction.")]
        Direction d_Direction,
        
        [FriendlyName("Vector3", "The returning value of the direction.")]
        out Vector3 v3_Direction)
    {
        Vector3 v3_direction = new Vector3();

        switch (d_Direction)
        {
            case Direction.Forward:
                v3_direction = go_GameObject.transform.forward;
                break;

            case Direction.Right:
                v3_direction = go_GameObject.transform.right;
                break;

            case Direction.Up:
                v3_direction = go_GameObject.transform.up;
                break;
        }

        v3_Direction = v3_direction;
    }

//------------------------------------------------------------------------
    public bool Out
    {
        get { return true; }
    }
#endregion
}