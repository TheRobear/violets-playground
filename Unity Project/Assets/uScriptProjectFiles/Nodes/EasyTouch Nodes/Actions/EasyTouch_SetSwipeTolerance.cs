﻿using UnityEngine;
using System.Collections;

[NodePath("EasyTouch/Actions")]

[NodeCopyright("Copyright 2015 Reclusive Ninja")]
[NodeToolTip("Sets the Swipe Tolerance for EasyTouch.")]
[NodeAuthor("Reclusive Ninja", "http://reclusiveninja.com")]

[FriendlyName("EasyTouch Set Swipe Tolerance", "Sets the Swipe Tolerance for EasyTouch.")]
public class EasyTouch_SetSwipeTolerance : uScriptLogic
{
#region uScript Functions ------------------------------------------------------------------------
    public void In(
        [FriendlyName("Swipe Tolerance", "Sets the Swipe Tolerance for EasyTouch.")]
        float f_SwipeTolerance)
    {
        HedgehogTeam.EasyTouch.EasyTouch.SetSwipeTolerance(f_SwipeTolerance);
    }
//------------------------------------------------------------------------
    public bool Out
    {
        get { return true; }
    }
#endregion
}