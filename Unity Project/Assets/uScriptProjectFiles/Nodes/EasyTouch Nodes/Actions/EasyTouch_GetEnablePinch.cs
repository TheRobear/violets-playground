﻿using UnityEngine;
using System.Collections;

[NodePath("EasyTouch/Actions")]

[NodeCopyright("Copyright 2015 Reclusive Ninja")]
[NodeToolTip("Returns if Pinch is enabled/disabled.")]
[NodeAuthor("Reclusive Ninja", "http://reclusiveninja.com")]

[FriendlyName("EasyTouch Get Enable Pinch", "Returns if Pinch is enabled/disabled.")]
public class EasyTouch_GetEnablePinch : uScriptLogic
{
#region uScript Functions ------------------------------------------------------------------------
	public void In(
		[FriendlyName("Is Pinch Enabled?", "Returns if Pinch is enabled/disabled.")]
		out bool b_PinchEnabled)
	{
        b_PinchEnabled = HedgehogTeam.EasyTouch.EasyTouch.GetEnablePinch();
	}

//------------------------------------------------------------------------
	public bool Out
	{
		get { return true; }
	}
#endregion
}