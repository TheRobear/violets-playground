﻿using UnityEngine;
using System.Collections;

[NodePath("EasyTouch/Actions")]

[NodeCopyright("Copyright 2015 Reclusive Ninja")]
[NodeToolTip("Sets the Two Finger Pick Method for EasyTouch.")]
[NodeAuthor("Reclusive Ninja", "http://reclusiveninja.com")]

[FriendlyName("EasyTouch Set Two Finger Pick Method", "Sets the Two Finger Pick Method for EasyTouch.")]
public class EasyTouch_SetTwoFingerPickMethod : uScriptLogic
{
#region uScript Functions ------------------------------------------------------------------------
    public void In(
        [FriendlyName("Two Finger Pick Method", "Sets the Two Finger Pick Method for EasyTouch.")]
        HedgehogTeam.EasyTouch.EasyTouch.TwoFingerPickMethod ettfpm_TwoFingPickMethod)
    {
        HedgehogTeam.EasyTouch.EasyTouch.SetTwoFingerPickMethod(ettfpm_TwoFingPickMethod);
    }
//------------------------------------------------------------------------
    public bool Out
    {
        get { return true; }
    }
#endregion
}