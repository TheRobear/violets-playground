﻿using UnityEngine;
using System.Collections;

[NodePath("EasyTouch/Actions")]

[NodeCopyright("Copyright 2015 Reclusive Ninja")]
[NodeToolTip("Returns the pickable layer of 3D objects.")]
[NodeAuthor("Reclusive Ninja", "http://reclusiveninja.com")]

[FriendlyName("EasyTouch Get 3D Pickable Layer", "Returns the pickable layer of 3D objects.")]
public class EasyTouch_Get3DPickableLayer : uScriptLogic
{
#region uScript Functions ------------------------------------------------------------------------
    public void In(
        [FriendlyName("Layer Mask", "Returns the pickable layer of 3D objects.")]
        out LayerMask lm_LayerMask)
    {
        lm_LayerMask = HedgehogTeam.EasyTouch.EasyTouch.Get3DPickableLayer();
    }

//------------------------------------------------------------------------
    public bool Out
    {
        get { return true; }
    }
#endregion
}