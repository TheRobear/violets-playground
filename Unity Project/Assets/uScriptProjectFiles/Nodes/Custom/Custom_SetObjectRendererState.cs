﻿using UnityEngine;
using System.Collections;

[NodePath("Custom/Actions")]

[FriendlyName("Custom - Set Object Renderer State")]
public class Custom_SetObjectRendererState : uScriptLogic
{
    public enum e_RenderState
    {
        Yes,
        No
    }
#region uScript Functions ------------------------------------------------------------------------
    public void In(
        [FriendlyName("Target", "The target game object.")]
        GameObject[] go_Target,
        
        [FriendlyName("Visible", "Is the object visible?")]
        e_RenderState ers_RenderState)
    {
        foreach (GameObject target in go_Target)
        {
            switch (ers_RenderState)
            {
                case e_RenderState.Yes:
                    target.GetComponent<Renderer>().enabled = true;
                    break;

                case e_RenderState.No:
                    target.GetComponent<Renderer>().enabled = false;
                    break;
            }
        }
    }

//------------------------------------------------------------------------
    public bool Out
    {
        get { return true; }
    }
#endregion
}