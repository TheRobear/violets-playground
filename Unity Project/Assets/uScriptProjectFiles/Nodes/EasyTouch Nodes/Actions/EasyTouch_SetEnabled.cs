﻿using UnityEngine;
using System.Collections;

[NodePath("EasyTouch/Actions")]

[NodeCopyright("Copyright 2015 Reclusive Ninja")]
[NodeToolTip("Enables/Disables EasyTouch.")]
[NodeAuthor("Reclusive Ninja", "http://reclusiveninja.com")]

[FriendlyName("EasyTouch Set Enabled", "Enables/Disables EasyTouch.")]
public class EasyTouch_SetEnabled : uScriptLogic
{
#region uScript Functions ------------------------------------------------------------------------
    public void In(
        [FriendlyName("Enable EasyTouch", "Enables/Disables EasyTouch.")]
        bool b_EnableEasyTouch)
    {
        HedgehogTeam.EasyTouch.EasyTouch.SetEnabled(b_EnableEasyTouch);
    }

//------------------------------------------------------------------------
    public bool Out
    {
        get { return true; }
    }
#endregion
}