﻿using UnityEngine;
using System.Collections;

[NodePath("Reclusive Ninja/Actions")]

[NodeCopyright("Copyright 2015 Reclusive Ninja")]
[NodeToolTip("Allows a Vector2 to be split into two float values for each respective axis.")]
[NodeAuthor("Reclusive Ninja", "http://reclusiveninja.com")]

[FriendlyName("Reclusive Ninja - Vector2 To Floats", "Allows a Vector2 to be split into two float values for each respective axis.")]
public class RN_Vector2ToFloats : uScriptLogic
{
#region uScript Functions ------------------------------------------------------------------------
    public void In(
        [FriendlyName("Vector2", "The Vector2 value to split.")]
        Vector2 v2_Vector2,

        [FriendlyName("X Value", "The float value for X.")]
        out float f_XValue,

        [FriendlyName("Y Value", "The float value for Y.")]
        out float f_YValue)
    {
        f_XValue = v2_Vector2.x;

        f_YValue = v2_Vector2.y;

    }

    //------------------------------------------------------------------------
    public bool Out
    {
        get { return true; }
    }
#endregion
}