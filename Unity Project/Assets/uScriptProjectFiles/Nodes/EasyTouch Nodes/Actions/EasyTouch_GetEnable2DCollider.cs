﻿using UnityEngine;
using System.Collections;

[NodePath("EasyTouch/Actions")]

[NodeCopyright("Copyright 2015 Reclusive Ninja")]
[NodeToolTip("Returns if 2D collider detection is enabled/disabled.")]
[NodeAuthor("Reclusive Ninja", "http://reclusiveninja.com")]

[FriendlyName("EasyTouch Get Enable 2D Collider", "Returns if 2D collider detection is enabled/disabled.")]
public class EasyTouch_GetEnable2DCollider : uScriptLogic
{
#region uScript Functions ------------------------------------------------------------------------
    public void In(
        [FriendlyName("Is 2D Collider Detection Enabled?", "Returns if 2D collider detection is enabled/disabled.")]
        out bool b_Is2DColliderEnabled)
    {
        b_Is2DColliderEnabled = HedgehogTeam.EasyTouch.EasyTouch.GetEnable2DCollider();
    }

//------------------------------------------------------------------------
    public bool Out
    {
        get { return true; }
    }
    #endregion
}