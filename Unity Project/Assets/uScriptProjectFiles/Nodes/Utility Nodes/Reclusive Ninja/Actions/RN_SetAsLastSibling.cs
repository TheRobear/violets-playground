﻿using UnityEngine;
using System.Collections;

[NodePath("Reclusive Ninja/Actions")]

[NodeCopyright("Copyright 2015 Reclusive Ninja")]
[NodeToolTip("Move the transform to the end of the local transform list.")]
[NodeAuthor("Reclusive Ninja", "http://reclusiveninja.com")]

[FriendlyName("Reclusive Ninja - Set As Last Sibling", "Move the transform to the end of the local transform list.")]
public class RN_SetAsLastSibling : uScriptLogic
{
#region uScript Functions ------------------------------------------------------------------------
    public void In(
        [FriendlyName("Target", "The target game object.")]
        GameObject go_GameObject)
    {
        go_GameObject.transform.SetAsLastSibling();
    }

//------------------------------------------------------------------------
    public bool Out
    {
        get { return true; }
    }
#endregion
}