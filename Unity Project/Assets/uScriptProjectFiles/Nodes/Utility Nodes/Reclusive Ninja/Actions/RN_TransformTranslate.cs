﻿using UnityEngine;
using System.Collections;

[NodePath("Reclusive Ninja/Actions")]

[NodeCopyright("Copyright 2015 Reclusive Ninja")]
[NodeToolTip("Moves the transform in the direction and distance of translation.")]
[NodeAuthor("Reclusive Ninja", "http://reclusiveninja.com")]

[FriendlyName("Reclusive Ninja - Transform Translate", "Moves the transform in the direction and distance of translation.")]
public class RN_TransformTranslate : uScriptLogic
{
#region uScript Functions ------------------------------------------------------------------------
    public void In(
        [FriendlyName("Target", "The target game object.")]
        GameObject go_GameObject,
        
        [FriendlyName("Translation", "The Vector3 value to move the target object.")]
        Vector3 v3_translation)
    {
        go_GameObject.transform.Translate(v3_translation);
    }

//------------------------------------------------------------------------
    public bool Out
    {
        get { return true; }
    }
#endregion
}