﻿using UnityEngine;
using System.Collections;

[NodePath("Reclusive Ninja/Actions")]

[NodeCopyright("Copyright 2015 Reclusive Ninja")]
[NodeToolTip("Returns the name of a camera.")]
[NodeAuthor("Reclusive Ninja", "http://reclusiveninja.com")]

[FriendlyName("Reclusive Ninja - Get Camera Name", "Returns the name of a camera.")]
public class RN_GetCameraName : uScriptLogic
{
#region uScript Functions ------------------------------------------------------------------------
    public void In(
        [FriendlyName("Camera", "The target camera.")]
        Camera c_Camera,
        
        [FriendlyName("Camera Name", "The name of the target camera.")]
        out string s_CameraName)
    {
        s_CameraName = c_Camera.name;
    }

//------------------------------------------------------------------------
    public bool Out
    {
        get { return true; }
    }
#endregion
}