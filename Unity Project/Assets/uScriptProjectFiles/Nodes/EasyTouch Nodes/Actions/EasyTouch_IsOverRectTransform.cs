﻿using UnityEngine;
using System.Collections;

[NodePath("EasyTouch/Actions")]

[NodeCopyright("Copyright 2015 Reclusive Ninja")]
[NodeToolTip("Return true if current touch is a Rect Transform.")]
[NodeAuthor("Reclusive Ninja", "http://reclusiveninja.com")]

[FriendlyName("EasyTouch Is Over Rect Transform", "Return true if current touch is a Rect Transform.")]
public class EasyTouch_IsOverRectTransform : uScriptLogic
{
    private bool b_IsOverRect = false;
#region uScript Functions ------------------------------------------------------------------------
    public void In(
        [FriendlyName("Gesture", "The gesture to check.")]
        HedgehogTeam.EasyTouch.Gesture g_Gesture,
        
        [FriendlyName("Target", "The object that contains the rect transform.")]
        GameObject go_RectTransform,
        
        [FriendlyName("Camera", "The target camera to check")]
        [SocketState(false, false)]
        Camera c_Camera)
    {
        RectTransform rt_Transform = go_RectTransform.GetComponent<RectTransform>();
        Debug.Log(rt_Transform.ToString());

        if (c_Camera)
        {
            b_IsOverRect = g_Gesture.IsOverRectTransform(rt_Transform, c_Camera);
            Debug.Log(b_IsOverRect);
        }
        else if (!c_Camera)
        {
            b_IsOverRect = g_Gesture.IsOverRectTransform(rt_Transform, null);
            Debug.Log(b_IsOverRect);
        }
    }

//------------------------------------------------------------------------
    [FriendlyName("Is Over Rect Transform")]
    public bool OverRectTransform
    {
        get { return b_IsOverRect; }
    }

//------------------------------------------------------------------------
    [FriendlyName("Is Not Over Rect Transform")]
    public bool NotOverRectTransform
    {
        get { return !b_IsOverRect; }
    }
#endregion
}