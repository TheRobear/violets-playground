﻿using UnityEngine;
using System.Collections;

[NodePath("EasyTouch/Actions")]

[NodeCopyright("Copyright 2015 Reclusive Ninja")]
[NodeToolTip("Enables/Disables 2D collider detection.")]
[NodeAuthor("Reclusive Ninja", "http://reclusiveninja.com")]

[FriendlyName("EasyTouch Set Enable 2D Collider", "Enables/Disables 2D collider detection.")]
public class EasyTouch_SetEnable2DCollider : uScriptLogic
{
#region uScript Functions ------------------------------------------------------------------------
    public void In(
        [FriendlyName("Enable 2D Collider", "Enables/Disables 2D collider detection.")]
        bool b_Enable2DCollider
        )
    {
        HedgehogTeam.EasyTouch.EasyTouch.SetEnable2DCollider(b_Enable2DCollider);
    }
//------------------------------------------------------------------------
    public bool Out
    {
        get { return true; }
    }
    #endregion
}