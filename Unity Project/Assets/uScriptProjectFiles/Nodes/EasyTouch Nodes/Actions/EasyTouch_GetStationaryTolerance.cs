﻿using UnityEngine;
using System.Collections;

[NodePath("EasyTouch/Actions")]

[NodeCopyright("Copyright 2015 Reclusive Ninja")]
[NodeToolTip("Returns the Stationary Tolerance of EasyTouch.")]
[NodeAuthor("Reclusive Ninja", "http://reclusiveninja.com")]

[FriendlyName("EasyTouch Get Stationary Tolerance", "Returns the Stationary Tolerance of EasyTouch.")]
public class EasyTouch_GetStationaryTolerance : uScriptLogic
{
#region uScript Functions ------------------------------------------------------------------------
    public void In(
        [FriendlyName("Stationary Tolerance", "Returns the Stationary Tolerance of EasyTouch.")]
        out float f_StationaryTolerance)
    {
        f_StationaryTolerance = HedgehogTeam.EasyTouch.EasyTouch.GetStationaryTolerance();
    }

//------------------------------------------------------------------------
    public bool Out
    {
        get { return true; }
    }
#endregion
}