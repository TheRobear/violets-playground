﻿using UnityEngine;
using System.Collections;

[NodePath("EasyTouch/Actions")]

[NodeCopyright("Copyright 2015 Reclusive Ninja")]
[NodeToolTip("Returns the Min Pinch Length.")]
[NodeAuthor("Reclusive Ninja", "http://reclusiveninja.com")]

[FriendlyName("EasyTouch Get Min Pinch Length", "Returns the Min Pinch Length.")]
public class EasyTouch_GetMinPinchLength : uScriptLogic
{
#region uScript Functions ------------------------------------------------------------------------
	public void In(
		[FriendlyName("Min Pinch Length", "Returns the Min Pinch Length.")]
		out float f_MinPinchLength)
	{
		f_MinPinchLength = HedgehogTeam.EasyTouch.EasyTouch.GetMinPinchLength();
	}

//------------------------------------------------------------------------
	public bool Out
	{
		get { return true; }
	}
#endregion
}