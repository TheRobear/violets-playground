﻿using UnityEngine;
using System.Collections;

[NodePath("EasyTouch/Actions")]

[NodeCopyright("Copyright 2015 Reclusive Ninja")]
[NodeToolTip("Returns if 2 Fingers Gesture is enabled for EasyTouch.")]
[NodeAuthor("Reclusive Ninja", "http://reclusiveninja.com")]

[FriendlyName("EasyTouch Get Enable 2 Fingers Gesture", "Returns if 2 Fingers Gesture is enabled for EasyTouch.")]
public class EasyTouch_GetEnable2FingersGesture : uScriptLogic
{
#region uScript Functions ------------------------------------------------------------------------
    public void In(
        [FriendlyName("2 Fingers Gesture Enabled?", "Returns if 2 Fingers Gesture is enabled for EasyTouch.")]
        out bool b_Enable2FingerGesture)
    {
        b_Enable2FingerGesture = HedgehogTeam.EasyTouch.EasyTouch.GetEnable2FingersGesture();
    }

//------------------------------------------------------------------------
    public bool Out
    {
        get { return true; }
    }
#endregion
}