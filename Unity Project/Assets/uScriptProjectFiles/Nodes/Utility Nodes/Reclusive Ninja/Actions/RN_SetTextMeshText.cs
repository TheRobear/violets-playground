﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[NodePath("Reclusive Ninja/Actions")]

[NodeCopyright("Copyright 2015 Reclusive Ninja")]
[NodeToolTip("Set the text value of a Text Mesh object.")]
[NodeAuthor("Reclusive Ninja", "http://reclusiveninja.com")]

[FriendlyName("Reclusive Ninja - Set Text Mesh Text", "Set the text value of a Text Mesh object.")]
public class RN_SetTextMeshText : uScriptLogic
{
#region uScript Functions ------------------------------------------------------------------------
    public void In(
        [FriendlyName("Target", "The target game object.")]
        GameObject go_GameObject,
        
        [FriendlyName("Text", "The value to set the text mesh text to.")]
        string s_Text)
    {
        go_GameObject.GetComponent<TextMesh>().text = s_Text;
    }

//------------------------------------------------------------------------
    public bool Out
    {
        get { return true; }
    }
#endregion
}