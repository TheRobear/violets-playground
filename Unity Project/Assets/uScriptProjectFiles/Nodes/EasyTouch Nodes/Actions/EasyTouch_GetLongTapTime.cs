﻿using UnityEngine;
using System.Collections;

[NodePath("EasyTouch/Actions")]

[NodeCopyright("Copyright 2015 Reclusive Ninja")]
[NodeToolTip("Returns the Long Tap Time of EasyTouch.")]
[NodeAuthor("Reclusive Ninja", "http://reclusiveninja.com")]

[FriendlyName("EasyTouch Get Long Tap Time", "Returns the Long Tap Time of EasyTouch.")]
public class EasyTouch_GetLongTapTime : uScriptLogic
{
#region uScript Functions ------------------------------------------------------------------------
    public void In(
        [FriendlyName("Long Tap Time", "Returns the Long Tap Time of EasyTouch.")]
        out float f_LongTapTime)
    {
        f_LongTapTime = HedgehogTeam.EasyTouch.EasyTouch.GetlongTapTime();
    }

//------------------------------------------------------------------------
    public bool Out
    {
        get { return true; }
    }
#endregion
}