﻿using UnityEngine;
using System.Collections;

[NodePath("Custom/Actions")]

[FriendlyName("Custom - Random Switch")]
public class Custom_RandomSwitch : uScriptLogic
{
    private int i_CurrentOutput = 1;
    private bool b_SwitchOpen = true;

    private bool b_Case1 = false;
    private bool b_Case2 = false;
    private bool b_Case3 = false;
    private bool b_Case4 = false;
    private bool b_Case5 = false;
    private bool b_Case6 = false;
    private bool b_Case7 = false;
    private bool b_Case8 = false;
    private bool b_Case9 = false;

#region uScript Functions ------------------------------------------------------------------------
    public void In(
        [FriendlyName("Max Cases", "Maximum usable case outputs.")]
        [SocketState(false, false)]
        int i_MaxOutputs,

        [FriendlyName("Current Case", "The current case in use.")]
        out int io_CurrentOutput)
    {
        b_Case1 = false;
        b_Case2 = false;
        b_Case3 = false;
        b_Case4 = false;
        b_Case5 = false;
        b_Case6 = false;
        b_Case7 = false;
        b_Case8 = false;
        b_Case9 = false;

        i_MaxOutputs = Mathf.Clamp(i_MaxOutputs, 1, 9);

        i_CurrentOutput = Random.Range(1, i_MaxOutputs + 1);

        if (b_SwitchOpen)
        {
            switch (i_CurrentOutput)
            {
                case 1:
                    io_CurrentOutput = i_CurrentOutput;
                    b_Case1 = true;
                    break;

                case 2:
                    io_CurrentOutput = i_CurrentOutput;
                    b_Case2 = true;
                    break;

                case 3:
                    io_CurrentOutput = i_CurrentOutput;
                    b_Case3 = true;
                    break;

                case 4:
                    io_CurrentOutput = i_CurrentOutput;
                    b_Case4 = true;
                    break;

                case 5:
                    io_CurrentOutput = i_CurrentOutput;
                    b_Case5 = true;
                    break;

                case 6:
                    io_CurrentOutput = i_CurrentOutput;
                    b_Case6 = true;
                    break;

                case 7:
                    io_CurrentOutput = i_CurrentOutput;
                    b_Case7 = true;
                    break;

                case 8:
                    io_CurrentOutput = i_CurrentOutput;
                    b_Case8 = true;
                    break;

                case 9:
                    io_CurrentOutput = i_CurrentOutput;
                    b_Case9 = true;
                    break;

                default:
                    io_CurrentOutput = 0;
                    break;
            }
        }
        else
        {
            io_CurrentOutput = i_CurrentOutput;
        }
    }

//------------------------------------------------------------------------
    [FriendlyName("Case 1")]
    public bool Case1
    {
        get { return b_Case1; }
    }

//------------------------------------------------------------------------
    [FriendlyName("Case 2")]
    public bool Case2
    {
        get { return b_Case2; }
    }

//------------------------------------------------------------------------
    [FriendlyName("Case 3")]
    public bool Case3
    {
        get { return b_Case3; }
    }

//------------------------------------------------------------------------
    [FriendlyName("Case 4")]
    public bool Case4
    {
        get { return b_Case4; }
    }

//------------------------------------------------------------------------
    [FriendlyName("Case 5")]
    public bool Case5
    {
        get { return b_Case5; }
    }

//------------------------------------------------------------------------
    [FriendlyName("Case 6")]
    public bool Case6
    {
        get { return b_Case6; }
    }

//------------------------------------------------------------------------
    [FriendlyName("Case 7")]
    public bool Case7
    {
        get { return b_Case7; }
    }

//------------------------------------------------------------------------
    [FriendlyName("Case 8")]
    public bool Case8
    {
        get { return b_Case8; }
    }

//------------------------------------------------------------------------
    [FriendlyName("Case 9")]
    public bool Case9
    {
        get { return b_Case9; }
    }
#endregion
}