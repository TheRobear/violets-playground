﻿using UnityEngine;
using System.Collections;

[NodePath("Reclusive Ninja/Actions")]

[NodeCopyright("Copyright 2015 Reclusive Ninja")]
[NodeToolTip("Formats a float to anumeric string using .Net modifiers. See https://msdn.microsoft.com/en-us/library/dwhawy9k%28v=vs.110%29.aspx")]
[NodeAuthor("Reclusive Ninja", "http://reclusiveninja.com")]

[FriendlyName("Reclusive Ninja - Format Float To Numeric String", "Formats a float to a numeric string using .Net modifiers. See https://msdn.microsoft.com/en-us/library/dwhawy9k%28v=vs.110%29.aspx")]
public class RN_FormatFloatString : uScriptLogic
{
#region uScript Functions ------------------------------------------------------------------------
    public void In(
        [FriendlyName("Float", "The float to format to a string.")]
        float f_Float,

        [FriendlyName("Modifier", "The modifier to format the numeric string.")]
        string s_Modifier,

        [FriendlyName("Result", "The result after the numeric string has been formatted.")]
        out string s_Result
        )
    {
        s_Result = f_Float.ToString(s_Modifier);
    }

//------------------------------------------------------------------------
    public bool Out
    {
        get { return true; }
    }
#endregion
}