﻿using UnityEngine;
using System.Collections;

public class ParticlePLayground_SetSourceWorldObject : uScriptLogic
{
    public void In(
        GameObject go_Particles,
        
        ParticlePlayground.WorldObject wo_Source)
    {
        ParticlePlayground.PlaygroundParticlesC ppg_Particles = go_Particles.GetComponent<ParticlePlayground.PlaygroundParticlesC>();

        ppg_Particles.source = ParticlePlayground.SOURCEC.WorldObject;

        ppg_Particles.worldObject = wo_Source;
    }

    public bool Out
    {
        get { return true; }
    }
}