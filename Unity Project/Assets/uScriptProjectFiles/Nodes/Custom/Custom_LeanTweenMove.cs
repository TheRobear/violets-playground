﻿using UnityEngine;
using System.Collections;

[NodePath("Custom/Actions")]
public class Custom_LeanTweenMove : uScriptLogic
{
	LTDescr ltd_Tween;
	
	public void In(
		GameObject go_GameObject,
		
		Vector3 v3_Location,
		
		float f_Time)
	{
		ltd_Tween = LeanTween.move(go_GameObject, v3_Location, f_Time);
		ltd_Tween.setOnComplete(OnCompleteMethod);
	}
	
//------------------------------------------------------------------------
	public bool Out
	{
		get { return true; }
	}
	
//------------------------------------------------------------------------
	public event System.EventHandler OnComplete;
	
//------------------------------------------------------------------------
	void OnCompleteMethod()
	{
		if (OnComplete != null)
		{
			OnComplete(this, new System.EventArgs());
		}
	}
}