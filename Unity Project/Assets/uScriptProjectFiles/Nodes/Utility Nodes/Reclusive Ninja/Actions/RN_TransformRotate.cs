﻿using UnityEngine;
using System.Collections;

[NodePath("Reclusive Ninja/Actions")]

[NodeCopyright("Copyright 2015 Reclusive Ninja")]
[NodeToolTip("Applies a rotation of euler angles to an object.")]
[NodeAuthor("Reclusive Ninja", "http://reclusiveninja.com")]

[FriendlyName("Reclusive Ninja - Transform Rotate", "Applies a rotation of euler angles to an object.")]
public class RN_TransformRotate : uScriptLogic
{
#region uScript Functions ------------------------------------------------------------------------
    public void In(
        [FriendlyName("Target", "The target object.")]
        GameObject go_GameObject,
        
        [FriendlyName("Euler Angles", "The Vector3 values to rotate the target object.")]
        Vector3 v3_EulerAngles)
    {
        go_GameObject.transform.Rotate(v3_EulerAngles);
    }

//------------------------------------------------------------------------
    public bool Out
    {
        get { return true; }
    }
#endregion
}