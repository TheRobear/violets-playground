﻿using UnityEngine;
using System.Collections;

[NodePath("EasyTouch/Events")]

[NodeCopyright("Copyright 2015 Reclusive Ninja")]
[NodeToolTip("Uses EasyTouch On_LongTap2Fingers to handle EasyTouch On_LongTap2Fingers events.")]
[NodeAuthor("Reclusive Ninja", "http://reclusiveninja.com")]

[FriendlyName("EasyTouch - On Long Tap 2 Fingers", "Uses EasyTouch On_LongTap2Fingers to handle EasyTouch On_LongTap2Fingers events.")]
public class EasyTouch_On_LongTap2Fingers : uScriptEvent
{
#region uScript Event Handlers ------------------------------------------------------------------------
    //uScript event delcaration.
    public delegate void uScriptEventHandler(object sender, OnLongTap2FingersEventArgs args);

    //Defining additional read-only variable sockets.
    public class OnLongTap2FingersEventArgs : System.EventArgs
    {
        private HedgehogTeam.EasyTouch.Gesture g_Gesture;

        [FriendlyName("Gesture", "The gesture used in this EasyTouch event.")]
        [SocketState(false, false)]
        public HedgehogTeam.EasyTouch.Gesture g_TargetGesture
        {
            get { return g_Gesture; }
        }

        [FriendlyName("Finger Index", "The index of finger that started the drag event.")]
        [SocketState(false, false)]
        public int i_FingerIndex
        {
            get { return g_Gesture.fingerIndex; }
        }

        [FriendlyName("Touch Count", "The number of active touches.")]
        [SocketState(false, false)]
        public int i_TouchCount
        {
            get { return g_Gesture.touchCount; }
        }

        [FriendlyName("Start Position Vector2", "The start position of the current gesture that fired the event as a Vector2.")]
        [SocketState(false, false)]
        public Vector2 v2_StartPosition
        {
            get { return g_Gesture.startPosition; }
        }

        [FriendlyName("Start Position Vector3", "The start position of the current gesture that fired the event as a Vector3.")]
        [SocketState(false, false)]
        public Vector3 v3_StartPosition
        {
            get { return (Vector3)g_Gesture.startPosition; }
        }

        [FriendlyName("Position Vector2", "The current position of the current gesture that fired the event as a Vector2.")]
        [SocketState(false, false)]
        public Vector2 v2_Position
        {
            get { return g_Gesture.position; }
        }

        [FriendlyName("Position Vector3", "The current position of the current gesture that fired the event as a Vector3.")]
        [SocketState(false, false)]
        public Vector3 v3_Position
        {
            get { return (Vector3)g_Gesture.position; }
        }

        [FriendlyName("Delta Position Vector2", "The delta position since last call as a Vector2.")]
        [SocketState(false, false)]
        public Vector2 v2_DeltaPosition
        {
            get { return g_Gesture.deltaPosition; }
        }

        [FriendlyName("Delta Position Vector3", "The delta position since last call as a Vector3.")]
        [SocketState(false, false)]
        public Vector3 v3_DeltaPosition
        {
            get { return (Vector3)g_Gesture.deltaPosition; }
        }

        [FriendlyName("Action Time", "The elapsed time in second since the begin of gesture.")]
        [SocketState(false, false)]
        public float f_ActionTime
        {
            get { return g_Gesture.actionTime; }
        }

        [FriendlyName("Delta Time", "The delta time since last call.")]
        [SocketState(false, false)]
        public float f_DeltaTime
        {
            get { return g_Gesture.deltaTime; }
        }

        [FriendlyName("Swipe", "The swipe or drag direction.")]
        [SocketState(false, false)]
        public HedgehogTeam.EasyTouch.EasyTouch.SwipeDirection etsw_SwipeDirection
        {
            get { return g_Gesture.swipe; }
        }

        [FriendlyName("Swipe As String", "Returns the swipe or drag direction as a string.")]
        [SocketState(false, false)]
        public string s_SwipeDirection
        {
            get { return g_Gesture.swipe.ToString(); }
        }

        [FriendlyName("Swipe Length", "The swipe length is pixels.")]
        [SocketState(false, false)]
        public float f_SwipeLength
        {
            get { return g_Gesture.swipeLength; }
        }

        [FriendlyName("Swipe Vector Vector2", "The swipe vector direciton as a Vector2.")]
        [SocketState(false, false)]
        public Vector2 v2_SwipeVector
        {
            get { return g_Gesture.swipeVector; }
        }

        [FriendlyName("Swipe Vector Vector3", "The swipe vector direciton as a Vector3.")]
        [SocketState(false, false)]
        public Vector3 v3_SwipeVector
        {
            get { return (Vector3)g_Gesture.swipeVector; }
        }

        [FriendlyName("Delta Pinch", "The delta pinch length in pixel since last call.")]
        [SocketState(false, false)]
        public float f_DeltaPinch
        {
            get { return g_Gesture.deltaPinch; }
        }

        [FriendlyName("Twist Angle", "The delta angle of the twist since the last call.")]
        [SocketState(false, false)]
        public float f_TwistAngle
        {
            get { return g_Gesture.twistAngle; }
        }

        [FriendlyName("Two Finger Distance", "The distance in pixel between two finger for a two finger gesture.")]
        [SocketState(false, false)]
        public float f_TwoFingerDistance
        {
            get { return g_Gesture.twoFingerDistance; }
        }

        [FriendlyName("Picked Object", "The current picked gameObject.")]
        [SocketState(false, false)]
        public GameObject go_PickedObject
        {
            get { return g_Gesture.pickedObject; }
        }

        [FriendlyName("Picked Camera", "The picked camera.")]
        [SocketState(false, false)]
        public Camera c_PickedCamera
        {
            get { return g_Gesture.pickedCamera; }
        }

        [FriendlyName("Is Camera a GUI Camera", "Is the picked camera is flag as GUI.")]
        [SocketState(false, false)]
        public bool b_GUICamera
        {
            get { return g_Gesture.isGuiCamera; }
        }

        [FriendlyName("Over GUI", "Is the current touch is over an Unity UI.")]
        [SocketState(false, false)]
        public bool b_IsOverGUI
        {
            get { return g_Gesture.isOverGui; }
        }

        [FriendlyName("Picked UI Element", "The current picked Unity UI.")]
        [SocketState(false, false)]
        public GameObject go_PickedUIElement
        {
            get { return g_Gesture.pickedUIElement; }
        }

        [FriendlyName("Touch To World Point Vector 2", "The current world position of the touch event as a Vector2.")]
        [SocketState(false, false)]
        public Vector2 v2_TouchToWorldPoint
        {
            get { return g_Gesture.GetTouchToWorldPoint(g_Gesture.position); }
        }

        [FriendlyName("Touch To World Point Vector 3", "The current world position of the touch event as a Vector3.")]
        [SocketState(false, false)]
        public Vector3 v3_TouchToWorldPoint
        {
            get { return g_Gesture.GetTouchToWorldPoint(g_Gesture.position); }
        }

        [FriendlyName("Touch To World Point Picked Object Vector 2", "The current world position of the picked object as a Vector2.")]
        [SocketState(false, false)]
        public Vector2 v2_TouchToWorldPointPickedObject
        {
            get
            {
                Vector2 tmpObjWorldPoint = new Vector2();

                if (g_Gesture.pickedObject)
                {
                    tmpObjWorldPoint = g_Gesture.GetTouchToWorldPoint(g_Gesture.pickedObject.transform.position);
                }

                return tmpObjWorldPoint;
            }
        }

        [FriendlyName("Touch To World Point Picked Object Vector 3", "The current world position of the picked object as a Vector3.")]
        [SocketState(false, false)]
        public Vector3 v3_TouchToWorldPointPickedObject
        {
            get
            {
                Vector3 tmpObjWorldPoint = new Vector3();

                if (g_Gesture.pickedObject)
                {
                    tmpObjWorldPoint = g_Gesture.GetTouchToWorldPoint(g_Gesture.pickedObject.transform.position);
                }

                return tmpObjWorldPoint;
            }
        }

        [FriendlyName("Altitude Angle", "Value of 0 radians indicates that the stylus is parallel to the surface, pi/2 indicates that it is perpendicular.")]
        [SocketState(false, false)]
        public float f_AltitudeAngle
        {
            get { return g_Gesture.altitudeAngle; }
        }

        [FriendlyName("Azimuth Angle", "Value of 0 radians indicates that the stylus is pointed along the x-axis of the device.")]
        [SocketState(false, false)]
        public float f_AzimuthAngle
        {
            get { return g_Gesture.azimuthAngle; }
        }

        [FriendlyName("Maximum Possible Pressure", "The maximum possible pressure value for a platform. If Input.touchPressureSupported returns false, the value of this property will always be 1.0f.")]
        [SocketState(false, false)]
        public float f_MaximumPossiblePressure
        {
            get { return g_Gesture.maximumPossiblePressure; }
        }

        [FriendlyName("Pressure", "The current amount of pressure being applied to a touch. 1.0f is considered to be the pressure of an average touch. If Input.touchPressureSupported returns false, the value of this property will always be 1.0f.")]
        [SocketState(false, false)]
        public float f_pressure
        {
            get { return g_Gesture.pressure; }
        }

        [FriendlyName("Radius", "An estimated value of the radius of a touch. Add radiusVariance to get the maximum touch size, subtract it to get the minimum touch size.")]
        [SocketState(false, false)]
        public float f_Radius
        {
            get { return g_Gesture.radius; }
        }

        [FriendlyName("Radius Variance", "The amount that the radius varies by for a touch.")]
        [SocketState(false, false)]
        public float f_RadiusVariance
        {
            get { return g_Gesture.radiusVariance; }
        }

        [FriendlyName("Type", "A value that indicates whether a touch was of Direct, Indirect (or remote), or Stylus type.")]
        [SocketState(false, false)]
        public HedgehogTeam.EasyTouch.EasyTouch.EvtType et_Type
        {
            get { return g_Gesture.type; }
        }

        [FriendlyName("Swipe/Drag Angle", "The angle of the current swipe or drag.")]
        [SocketState(false, false)]
        public float f_SwipeDragAngle
        {
            get { return g_Gesture.GetSwipeOrDragAngle(); }
        }

        [FriendlyName("Normalized Position Vector2", "Gets the normalized position as a Vector2.")]
        [SocketState(false, false)]
        public Vector2 v2_NormalizedPosition
        {
            get { return g_Gesture.NormalizedPosition(); }
        }

        [FriendlyName("Normalized Position Vector3", "Gets the normalized position as a Vector3.")]
        [SocketState(false, false)]
        public Vector3 v3_NormalizedPosition
        {
            get { return (Vector3)g_Gesture.NormalizedPosition(); }
        }

        [FriendlyName("Get Current Picked Object", "Gets the real picked object under the touch.")]
        [SocketState(false, false)]
        public GameObject go_CurrentPickedObject
        {
            get { return g_Gesture.GetCurrentPickedObject(false); }
        }

        [FriendlyName("Get Current Picked Object Two Finger Gesture", "Gets the real picked object under the touch during a two finger gesture.")]
        [SocketState(false, false)]
        public GameObject go_CurrentPickedObjectTwoFinger
        {
            get { return g_Gesture.GetCurrentPickedObject(true); }
        }

        [FriendlyName("Over UI Element", "Is the current touch is over an Unity UI.")]
        [SocketState(false, false)]
        public bool b_OverUIElement
        {
            get { return g_Gesture.IsOverUIElement(); }
        }

        [FriendlyName("Get Current First Picked UI Element", "Is the current touch is over a specific RectTransform.")]
        [SocketState(false, false)]
        public GameObject go_FirstUI
        {
            get { return g_Gesture.GetCurrentFirstPickedUIElement(false); }
        }

        [FriendlyName("Get Current First Picked UI Element Two Finger Gesture", "Is the current touch is over a specific RectTransform during a two finger gesture.")]
        [SocketState(false, false)]
        public GameObject go_FirstUITwoFingers
        {
            get { return g_Gesture.GetCurrentFirstPickedUIElement(true); }
        }

        public OnLongTap2FingersEventArgs(HedgehogTeam.EasyTouch.Gesture gesture)
        {
            g_Gesture = gesture;
        }
    }

    //EasyTouch event delcaration.
    [FriendlyName("On_LongTapStart2Fingers")]
    public event uScriptEventHandler OnLongTapStart2Fingers;

    [FriendlyName("On_LongTap2Fingers")]
    public event uScriptEventHandler OnLongTap2Fingers;

    [FriendlyName("On_LongTapEnd2Fingers")]
    public event uScriptEventHandler OnLongTapEnd2Fingers;
#endregion

#region Unity Related Functions ------------------------------------------------------------------------
    //Subscribe to the EasyTouch On_LongTap2Fingers delegate call when the object is enabled.
    void OnEnable()
    {
        HedgehogTeam.EasyTouch.EasyTouch.On_LongTapStart2Fingers += On_LongTapStart2Fingers;
        HedgehogTeam.EasyTouch.EasyTouch.On_LongTap2Fingers += On_LongTap2Fingers;
        HedgehogTeam.EasyTouch.EasyTouch.On_LongTapEnd2Fingers += On_LongTapEnd2Fingers;
    }

//------------------------------------------------------------------------
    //Unsubscribe to the EasyTouch On_LongTap2Fingers delegate call when the object is disabled.
    void OnDisable()
    {
        HedgehogTeam.EasyTouch.EasyTouch.On_LongTapStart2Fingers -= On_LongTapStart2Fingers;
        HedgehogTeam.EasyTouch.EasyTouch.On_LongTap2Fingers -= On_LongTap2Fingers;
        HedgehogTeam.EasyTouch.EasyTouch.On_LongTapEnd2Fingers -= On_LongTapEnd2Fingers;
    }

//------------------------------------------------------------------------
    //Unsubscribe to the EasyTouch On_LongTap2Fingers delegate call when the object is destroyed.
    void OnDestroy()
    {
        HedgehogTeam.EasyTouch.EasyTouch.On_LongTapStart2Fingers -= On_LongTapStart2Fingers;
        HedgehogTeam.EasyTouch.EasyTouch.On_LongTap2Fingers -= On_LongTap2Fingers;
        HedgehogTeam.EasyTouch.EasyTouch.On_LongTapEnd2Fingers -= On_LongTapEnd2Fingers;
    }
#endregion

#region EasyTouch Funcitons ------------------------------------------------------------------------
    //Calls the EasyTouch On_LongTapStart2Fingers declare call.
    public void On_LongTapStart2Fingers(HedgehogTeam.EasyTouch.Gesture gesture)
    {
        LongTapStart2FingersGesture(gesture);
    }

//------------------------------------------------------------------------
    //Calls the EasyTouch On_LongTap2Fingers delecare call.
    public void On_LongTap2Fingers(HedgehogTeam.EasyTouch.Gesture gesture)
    {
        LongTap2FingersGesture(gesture);
    }

//------------------------------------------------------------------------
    //Calls the EasyTouch On_LongTapEnd2Fingers delcare call.
    public void On_LongTapEnd2Fingers(HedgehogTeam.EasyTouch.Gesture gesture)
    {
        LongTapEnd2FingersGesture(gesture);
    }

//------------------------------------------------------------------------
    //Sends the event tp the node chain connected to On_LongTapStart2Fingers socket.
    void LongTapStart2FingersGesture(HedgehogTeam.EasyTouch.Gesture gesture)
    {
        if (OnLongTapStart2Fingers != null)
        {
            OnLongTapStart2Fingers(this, new OnLongTap2FingersEventArgs(gesture));
        }
    }
    
//------------------------------------------------------------------------
    //Sends the event to the node chain connected to On_LongTap2Fingers socket.
    void LongTap2FingersGesture(HedgehogTeam.EasyTouch.Gesture gesture)
    {
        if (OnLongTap2Fingers != null)
        {
            OnLongTap2Fingers(this, new OnLongTap2FingersEventArgs(gesture));
        }
    }

//------------------------------------------------------------------------
    //Sends the event to the node chain connected to On_LongTapEnd2Fingers socket.
    void LongTapEnd2FingersGesture(HedgehogTeam.EasyTouch.Gesture gesture)
    {
        if (OnLongTapEnd2Fingers != null)
        {
            OnLongTapEnd2Fingers(this, new OnLongTap2FingersEventArgs(gesture));
        }
    }
#endregion
}