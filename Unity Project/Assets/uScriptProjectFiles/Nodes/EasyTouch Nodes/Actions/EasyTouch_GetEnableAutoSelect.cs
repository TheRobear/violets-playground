﻿using UnityEngine;
using System.Collections;

[NodePath("EasyTouch/Actions")]

[NodeCopyright("Copyright 2015 Reclusive Ninja")]
[NodeToolTip("Returns if EnableAutoSelect is enabled/disabled.")]
[NodeAuthor("Reclusive Ninja", "http://reclusiveninja.com")]

[FriendlyName("EasyTouch Get Enable Auto Select", "Returns if EnableAutoSelect is enabled/disabled.")]
public class EasyTouch_GetEnableAutoSelect : uScriptLogic
{
#region uScript Functions ------------------------------------------------------------------------
    public void In(
        [FriendlyName("Is Auto Select Enabled?", "Returns if EnableAutoSelect is enabled/disabled.")]
        out bool b_AutoSelectEnabled)
    {
        b_AutoSelectEnabled = HedgehogTeam.EasyTouch.EasyTouch.GetEnableAutoSelect();
    }

//------------------------------------------------------------------------
    public bool Out
    {
        get { return true; }
    }
#endregion
}